func2d-glut - Demonstracja łączenia obliczeń w CUDA z wizualizacją w OpenGL
===========================================================================

Ten program ma (w zamierzeniach) demonstrować łączenie obliczeń na GPU
za pomocą CUDA C oraz tworzenia wizualizacji obliczeń za pomocą OpenGL.
Projekt składa się z kilku niezależnych i samodzielnych programów, z których
każdy dokonuje różnej wizualizacji.  Wszystkie z nich dokonują wizualizacji
funkcji statycznej $z=f(x,y)$, lub animacji funkcji $z=f(x,y;t)$.

Tworzenie grafiki (wizualizacja za pomocą OpenGL) odbywa się za pomocą
tzw. 'fixed-function pipeline', czyli bez użycia shaderów.

Do obsługi systemu okienkowe została użyta biblioteka GLUT (OpenGL Utility
Toolkit), jak wskazuje nazwa projektu.

Program używa biblioteki GLEW do zarządzania rozszerzeniami OpenGL.

Repozytorium projektu dostępne jest pod
<https://gitlab.com/programowanie-gpu/func2d-glut>


Dostępne programy
-----------------

Projekt ten zawiera różne funkcjonalności w różnych plikach źródłowych,
które są niezależne od siebie (tak więc można skopiować indywidualne
pliki, i mogą być one kompilowane niezależnie).  Każdy z głównych programów
jest bogato ukomentowany.

Dwa poniższe programy powstały w trakcie rozwoju projektu, i służą do
eksperymentów:

- **`pbo_demo.c`** - _przykładowy_ program tworzący obiekt bufora PBO
  (Pixel Buffer Object). Ten plik / program służy do testowania
  jakie pliki nagłówkowe trzeba dołączyć i jakie biblioteki; nie używa CUDA.

- **`func2d.cu`** - _przykładowy_ program do testowania tworzenia statycznego
  obrazu wizualizującego funkcję dwu zmiennych.  Służy do eksperymentów
  z generowaniem wykresów, profilowaniem OpenGL i CUDA, itp.

Poniższe (niezależne) programy stanowią główną część projektu.  Można
z nich korzystać do zrozumienia łączenia obliczeń w CUDA (lub obliczeń na
CPU) z wizualizacją z wykorzystaniem OpenGL.

- **`gpu_bitmap_static.cu`** - program ten wyświetla wykres funkcji dwu zmiennych
  $f(x,y)$ za pomocą mapy kolorów (odcienie szarości, z zaznaczeniem elementów
  wykraczających poza zakres wartości [f_min, f_max]).  Wyświetlanie odbywa
  się za pomocą glDrawPixels i obiektu PBO (Pixel Buffer Object).

  Makefile projektu generuje dwa programy z tego kodu źródłowego:
  - `cpu_bitmap_static`, wykonujący obliczenia na CPU
  - `gpu_bitmap_static`, wykonujący obliczenia na GPU w CUDA

  347 SLOC (linii kodu), 403 linii komentarzy, 108 linii odstępów,
  razem 858 linii.

- **`gpu_bitmap_anim.cu`** - program ten generuje animację funkcji
  dwu zmiennych zależnej od czasu $f(x,y;t)$.  Wartości funkcji przedstawiane
  są za pomocą mapy kolorów (odcienie szarości).  Wyświetlanie odbywa się
  za pomocą OpenGL (a dokładniej PBO (Pixmap Buffer Object) i glDrawPixels).

  Makefile projektu generuje dwa programy z tego kodu źródłowego:
  - `cpu_bitmap_anim`, wykonujący obliczenia na CPU
  - `gpu_bitmap_anim`, wykonujący obliczenia na GPU w CUDA

  338 SLOC (linii kodu), 448 linii komentarzy, 120 linii odstępów,
  razem 906 linii.

- **`gpu_plot3d_anim.cu`** -program ten generuje animację funkcji dwu
  zmiennych zależnej od czasu $f(x,y;t)$.  Wartości funkcji przedstawiane
  są jako wykres trójwymiarowy, w jednolitym kolorze lub z kolorowaniem
  za pomocą mapy kolorów.  Wyświetlanie odbywa się za pomocą glDrawElements
  oraz obiektu typu VBO (Vertex Buffer Object) z OpenGL.

  Obsługiwane klawisze wypisywane są po wciśnieciu klawisza '?'. Przesuwanie
  z wcisniętym lewym klawiszem myszy daje obracanie, a z prawym klawiszem
  myszy oddalanie / przybliżanie kamery.

  Makefile projektu generuje dwa programy z tego kodu źródłowego:
  - `cpu_plot3d_anim`, wykonujący obliczenia na CPU
  - `gpu_plot3d_anim`, wykonujący obliczenia na GPU w CUDA

  3122 SLOC (linii kodu), 1125 linii komentarzy, 367 linii odstępów,
  razem 1630 linii.


Historia powstania
------------------

Program powstał na zakończenie semestru letnim roku akademickiego 2013/2014
na przedmiocie do wyboru ["Programowanie GPU" (1000-I2PGPU)][1], który odbywa
się na Wydziale Matematyki i Informatyki Uniwersytetu Mikołaja Kopernika
w Toruniu, i został rozbudowany w roku akademickim 2014/15.  Repozytorium
GitLab zostało utworzone pod koniec semestru letniego roku akademickiego
2015/16.

[1]: https://usosweb.umk.pl/kontroler.php?_action=katalog2/przedmioty/pokazPrzedmiot&kod=1000-I2PGPU