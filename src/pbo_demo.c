#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// OpenGL Graphics includes
#include <GL/glew.h>
#ifdef _WIN32
#include <GL/wglew.h>
#endif
#if defined(__APPLE__) || defined(__MACOSX)
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif


GLuint bufferObj;
int width = 256, height = 128;


void freeGLBuffers(void);

void init(void)
{
	glClearColor(0.0, 0.0, 0.0, 1.0); // RGBA
	glClear(GL_COLOR_BUFFER_BIT); // color writing
	glFlush();
}

// OpenGL display function
void displayFunc(void)
{
	static int nr = 1;
	printf("- %i: [%s]\n", nr++, __FUNCTION__);

	glClearColor(0.0, 0.0, 0.0, 1.0); // RGBA
	glClear(GL_COLOR_BUFFER_BIT); // color writing
	//glDrawPixels(width, height, GL_RGBA,
	//             GL_UNSIGNED_BYTE, 0 /* data */);
	glFlush();

	glutSwapBuffers();
}

// OpenGL idle function
void idleFunc(void)
{
	// ...

	glutPostRedisplay();
}

// OpenGL keyboard function
void keyboardFunc(unsigned char key, int x, int y)
{
	switch (key) {
	case 'q':
	case 'Q':
		printf("received key '%c'\n", key);
		printf("Shutting down...\n");
		freeGLBuffers();
		exit(EXIT_SUCCESS);
		break;

	default:
		break;
	}
}

void initGL(int *argc, char *argv[])
{
	printf("Initializing GLUT...\n");
	glutInit(argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_ALPHA);
	glutInitWindowSize(width, height);
	//glutInitWindowPosition(0, 0);
	glutCreateWindow(argv[0]);

	printf(" Initializing GLEW...\n");
	GLenum err = glewInit();
	if (err != GLEW_OK) {
		/* Problem: glewInit failed, something is seriously wrong. */
		fprintf(stderr, "GLEW Error: %s\n", glewGetErrorString(err));
	}
	printf(" Status: Using GLEW %s\n", glewGetString(GLEW_VERSION));
	printf(" OpenGL vendor string: %s\n", glGetString(GL_VENDOR));
	printf(" OpenGL renderer string: %s\n", glGetString(GL_RENDERER));
	printf(" OpenGL version string: %s\n", glGetString(GL_VERSION));
	printf(" GLU version string: %s\n", gluGetString(GLU_VERSION));

	init();

	glutDisplayFunc(displayFunc);
	glutKeyboardFunc(keyboardFunc);
	printf("GLUT initialized (done)\n");
}

void initGLBuffers(int w, int h)
{
	printf("Initializing OpenGL buffers...\n");
	printf(" Creating PBO...\n");

	glGenBuffers(1, &bufferObj);
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, bufferObj);
	glBufferData(GL_PIXEL_UNPACK_BUFFER_ARB, width * height * 4,
	             NULL, GL_STATIC_DRAW_ARB);

	printf("PBO created (done)\n");
}

void freeGLBuffers(void)
{
	printf("Destroying PBO...\n");

	glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, 0);
	glDeleteBuffers(1, &bufferObj);

	printf("PBO deleted (done)\n");
}


// -----------------------------------------------------------------------
// -----------------------------------------------------------------------

int main(int argc, char *argv[])
{
	printf("[%s] - Starting...\n", argv[0]);

	initGL(&argc, argv);
	initGLBuffers(width, height);

	glutMainLoop();

	//cudaDeviceReset();

	printf("[%s] - Done\n", argv[0]);
	return 0;
}
