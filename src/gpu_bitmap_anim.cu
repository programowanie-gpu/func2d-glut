/*
 * Przykładowy program demonstrujący łączenie obliczeń w CUDA
 * z animacją (grafiką) OpenGL.
 *
 * Program ten generuje animację funkcji dwu zmiennych zależnej od
 * czasu f(x,y;t).  Wartości funkcji przedstawiane są za pomocą mapy
 * kolorów (odcienie szarości, z zaznaczeniem elementów wykraczających
 * poza zakres wartości [f_min, f_max]).
 *
 * Wyświetlanie odbywa się za pomocą OpenGL (a dokładniej PBO (Pixmap
 * Buffer Object) i glDrawPixels).  Do integracji z środowiskiem
 * graficznym używana jest biblioteka GLUT (OpenGL Utility Toolkit).
 * Obliczenia odbywają się normalnie na karcie graficznej za pomocą
 * CUDA; jeśli zdefiniowane jest makro preprocesora NO_CUDA to
 * obliczenia odbywają się sekwencyjnie na CPU, i kopiowane na kartę
 * graficzną do PBO do wyświetlenia.
 *
 * Program ten należy skompilować dołączając biblioteki (LDLIBS)
 * -lGL -lGLU -lglut -lGLEW -lm
 *
 * Ponieważ wykorzystywane są makra o zmiennej liczbie argumentów,
 * korzystając ze składni standardu C99 raczej niż z rozszerzeń GCC,
 * program należy kompilować z -std=c99 (CFLAGS).
 *
 * Kompilacja za pomocą gcc wymaga dodatkowo wymuszenia identyfikacji
 * pliku *.cu jako zawierającego źródła w języku C: -x c (CFLAGS).
 *
 * ..................................................................
 * Written in 2014 by Jakub Narębski <jnareb@mat.umk.pl>
 *
 * To the extent possible under law, the author(s) have dedicated all
 * copyright and related and neighboring rights to this software to
 * the public domain worldwide. This software is distributed without
 * any warranty.
 *
 * http://creativecommons.org/publicdomain/zero/1.0/
 */


/* ---------------------------------------------------------------------- */
/* Dołączanie plików nagłówkowych */

// tzw. feature test macros, potrzebne do dostępu do stałej M_PI z math.h
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#ifndef _BSD_SOURCE
#define _BSD_SOURCE
#endif

// biblioteki standardowe
#include <stdio.h>  // wypisywanie informacji, m.in. printf
#include <stdlib.h> // m.in alokowanie danych, m.in. malloc
#include <math.h>   // funkcje i stałe matematyczne, m.in. sinf
#include <string.h> // do obsługi opcji programu, m.in. strcmp
#include <float.h>  // stała FLT_EPSILON, do porównywania liczb
#include <time.h>   // pomiar czasu do animacji i FPS

#ifndef M_PI
#define M_PI 3.14159265358979323846  /* pi */
#endif /* !M_PI */

// biblioteki grafiki OpenGL
/*
 * Aby móc używać PBO: glBindBuffer, glBufferData, itp. potrzeba
 * załadować odpowiednie rozszerzeniami OpenGL.  Najprościej
 * wykorzystać wieloplatformową bibliotekę przeznaczoną do tego celu,
 * jako że bezpośrednie zarządzanie rozszerzeniami w OpenGL (glext.h)
 * jest zależne od środowiska (np. WGL lub GLX).
 *
 * Przykładami takich bibliotek są:
 *  - GLEW (The OpenGL Extension Wrangler Library)
 *  - GLee (GL Easy Extension library)
 * W tym programie wykorzystywana jest biblioteka GLEW.
 *
 * Jej plik nagłówkowy <GL/glew.h> automatycznie ładuje odpowiednie
 * pliki nagłówkowe OpenGL, tzn.:
 *   #include <GL/glut.h>
 *   #include <GL/glext.h>
 *
 * Patrz np. http://www.opengl.org/wiki/OpenGL_Loading_Library
 *
 *
 * Przykładowe programy z "CUDA by Example" ręcznie 'ładują'
 * glBindBuffer itp. za pomocą wglGetProcAddress() lub
 * glXGetProcAddress() - patrz gl_helper.h oraz gpu_anim.h
 * w katalogu common/ archiwum cuda_by_example.zip
 */
#include <GL/glew.h>
#ifdef _WIN32
#include <GL/wglew.h>
#endif
/*
 * Rysowanie za pomocą OpenGL wymaga zazwyczaj napisania kodu
 * zależnego od platformy do utworzenia okna, gdzie będzie odbywać się
 * renderowanie.  Aby zapewnić wieloplatformowość, najłatwiej
 * skorzystać z odpowiedniego toolkitu.
 *
 * Ten program wykorzystuje prostą bibliotekę GLUT (The OpenGL Utility
 * Toolkit), która została utworzona w celu ułatwienie procesu uczenia
 * się OpenGL-a (a dokładniej jej forku FreeGLUT).
 *
 * http://www.opengl.org/wiki/Related_toolkits_and_APIs#Context.2FWindow_Toolkits
 */
#if defined(__APPLE__) || defined(__MACOSX)
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif


#ifndef NO_CUDA
/*
 * Używamy CUDA Runtime API do obliczeń, oraz interoperacyjności
 * z OpenGL do wyświetlania, mapując PBO (Pixel Buffer Object) do
 * przestrzeni adresowej CUDA.
 *
 * http://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html#graphics-interoperability
 * http://docs.nvidia.com/cuda/cuda-runtime-api/group__CUDART__OPENGL.html
 * http://docs.nvidia.com/cuda/cuda-runtime-api/group__CUDART__INTEROP.html
 */
# warning "Using CUDA (GPU)"
// pliki nagłówkowe wysokopoziomowego CUDA Runtime API
// i plik nagłówkowy interoperacyjności CUDA z OpenGL
#include <cuda_runtime.h>
#include <cuda_gl_interop.h>
/*
 * Do obliczeń CUDA korzystamy z dwu-wymiarowych bloków wątków N x N,
 * dopasowując liczbę bloków do ilości danych (rozmiaru bitmapy).
 */
#define BLOCK_DIM 16

#else  /* NO_CUDA */
# warning "Using CPU only"
// definiujemy __host__ i __device__ jako no-op
#define __host__
#define __device__
#endif /* NO_CUDA */


/* ---------------------------------------------------------------------- */
/* Makrodefinicje preprocesora */

/*
 * Makro eprintf(fmt, args...) o zmiennej liczbie argumentów (składnia
 * standardu C99) jest wykorzystywane do wypisywania informacji
 * pomocniczych do debugowania programu (do standardowego strumienia
 * błędów stderr).  Nie wypisuje nic przy kompilacji programu ze
 * zdefiniowaną stałą preprocesora NDEBUG (CPPFLAGS += -DNEDBUG)
 */
#ifndef NDEBUG
#define eprintf(...) fprintf(stderr, __VA_ARGS__)
#else
#define eprintf(...) ((void)0)
#endif

/*
 * Makro ARRAY_SIZE(arr) zwraca liczbę elementów w tablicy arr.
 * Wyrażenie to jest stałą czasu kompilacji, zatem może na przykład
 * być używane przy definiowaniu nowych tablic.
 *
 * UWAGA: tą definicję preprocesora należy używać tylko na tablicach!
 */
#ifndef ARRAY_SIZE
#define ARRAY_SIZE(arr) (sizeof(arr)/sizeof((arr)[0]))
#endif


/* ---------------------------------------------------------------------- */
/* Definicje typów */

/*
 * Definicja typu wyliczeniowego, używanego jako indeks lub offset
 * (przesunięcie) w tablicy kolorów RGBA.  Wykorzystywana jest tylko
 * liczbowa reprezentacja wartości wyliczeniowych (jako stałe), w celu
 * lepszej czytelności programu.
 */
typedef enum rgba_coloridx {
	cR = 0,
	cG,
	cB,
	cA
} rgba_coloridx_t;


/* ---------------------------------------------------------------------- */
/* Definicje zmiennych globalnych */

// PBO (Pixel Buffer Object) i bitmap/pixmap
GLuint bufferObj; // uchwyt do bufora PBO dla OpenGL
#ifndef NO_CUDA
cudaGraphicsResource *resource; // uchwyt PBO dla CUDA
#else  /* NO_CUDA */
unsigned char *cpu_pixmap = NULL; // bitmapa na CPU
#endif /* NO_CUDA */

// parametry okna i wykresu
int width = 512, height = 256; // wymiary okna
float dx = 0.025, dy = 0.025;  // skala wykresu (odległości między pikselami)

// animacja
/*
 * Możliwe sposoby pomiaru czasu:
 *  - clock_getres(CLOCK_MONOTONIC, ...)  - sekundy i nanosekundy
 *  - gettimeofday(..., NULL)             - sekundy i mikrosekundy
 *  - clock()                             - CLOCKS_PER_SEC, czas CPU
 *  - glutGet(GLUT_ELAPSED_TIME)          - milisekundy od startu
 *
 * Ten program wykorzystuje glutGet(GLUT_ELAPSED_TIME), jak oryginalny
 * program demonstracyjny 'glxgears'.
 */
double t = 0.0;              // czas animacji w sekundach, od startu
unsigned long int iter = 0;  // liczba iteracji
// pomiar FPS (Frames Per Seconds)
int fps_start = -1;               // początek okresu zliczania, milisekundy
unsigned long int fps_frames = 0; // ilość ramek od początku okresu zliczania


// makra zamieniające indeks piksela na współrzędną
#define TO_X(ix) (dx*((ix) - width/2))
#define TO_Y(iy) (dy*((iy) - height/2))

// makro zwracające indeks elementu o współrzędnych (indeksach) 'i' i 'j'
// w "spłaszczonej" tablicy dwuwymiarowej 'Ni' x 'Nj', gdzie elementy są
// ułożone wierszami (tzn. 0-wy wiersz, 1-szy wiersz, ...)
#define IDX2D(Ni,Nj,i,j) ((j)*(Ni) + (i))
// makro zwracające adres piksela RGBA o indeksach 'ix' i 'iy'
// w bitmapie 'pixmap' typu unsigned char; każdy piksel ma 4 składowe
#define PIXEL_PTR(pixmap,ix,iy) ((pixmap) + IDX2D(width,height,ix,iy)*4)

/* ====================================================================== */
/* Definicje funkcji */


/* ---------------------------------------------------------------------- */
/* Tworzenie wykresu */

// funkcja dwu zmiennych do wykreślenia
// plot_GPU / plot_CPU zakłada, że wartości funkcji mieszczą się w [-2,2]
__host__ __device__
float func(float x, float y, float t)
{
	//return sinf(x*M_PI + t*M_PI) + cosf(y*M_PI + 0.5*t*M_PI);
	//return sinf(x*M_PI + t*M_PI) + (2.0*(y - floorf(y)) - 1.0);
	//return (2.0*(x - floorf(x)) - 1.0) + (2.0*(y - floorf(y)) - 1.0);
	return 2*expf(-(x*x/4 + y*y)/3.0)*sinf(4*sqrtf(x*x/4+y*y)*M_PI - 4*t*M_PI);
}


/*
 * Zamiana wartości 'val' w zakresie 'min'..'max' na piksel,
 * tutaj za pomocą 8-bitowej palety odcieni szarości (Y,Y,Y).
 * Wartości poza zakresem są zaznaczane za pomocą specjalnych
 * kolorów: czerwony (R,0,0) dla val > max, niebieski (0,0,B) dla val < min.
 *
 * https://en.wikipedia.org/wiki/List_of_monochrome_and_RGB_palettes#8-bit_Grayscale
 */
__host__ __device__
void plotPixel(unsigned char pixel[4], float val,
               float min, float max)
{
	// tzw. sanity check i ew. naprawienie min i max
	if (max < min) {
		float tmp = min;
		min = max;
		max = tmp;
	}
	if (fabs(max - min) < FLT_EPSILON)
		max = min + 1.0;

	// obsłużenie przypadku gdy wartości są poza zakresem
	if (val < min) {
		// alternatywnym rozwiązaniem jest przycięcie: val = min;
		// niebieski (chłodny) dla wartości poniżej minimum
		pixel[cR] = 0; pixel[cG] = 0; pixel[cB] = 255; pixel[cA] = 255;
		return;
	} else if (val > max) {
		// alternatywnym rozwiązaniem jest przycięcie: val = max;
		// czerwony (ciepły) dla wartości powyżej minimum
		pixel[cR] = 255; pixel[cG] = 0; pixel[cB] = 0; pixel[cA] = 255;
		return;
	}

	// przeskalowanie wartości do zakresu 0.0..1.0
	val = (val - min)/(max - min);

	// paleta odcieni szarości (R,G,B) = (Y,Y,Y)
	// zamiana wartości zmiennoprzecinkowej 0..1.0 na 8-bit 0..255
	pixel[cR] =
		pixel[cG] =
		pixel[cB] = (char)floor(255.0*val);
	pixel[cA] = 255;
}


/* ...................................................................... */
/* Tworzenie wykresu 2D, tzn. wypełnianie pixmap/bitmap */
#ifndef NO_CUDA
__global__
void plot_GPU(int width, int height, float dx, float dy, float t,
              unsigned long iter, unsigned char *img)
{
	// indeks wątku w dwuwymiarowej siatce
	int ix = blockIdx.x*blockDim.x + threadIdx.x;
	int iy = blockIdx.y*blockDim.y + threadIdx.y;

	// zabezpieczenie przed pisaniem poza tablicą, ważne jeśli
	// wymiary obrazu nie są całkowitą wielokrotnością rozmiaru bloku
	// (właściwie niepotrzebne jeśli zachowujemy poniższe pętle while)
	if (ix >= width || iy >= height)
		return;

	// pętle while zapewniają całkowite objęcie tablicy (bitmapy),
	// nawet jeśli maksymalna liczba wątków dla danej architektury GPU
	// jest mniejsza od liczby pikseli w obrazie (rozmiaru tablicy)
	// (nie jest właściwie potrzebne w tym programie)
	while (ix < width) {
		while (iy < height) {
			plotPixel(PIXEL_PTR(img, ix, iy),
			          func(TO_X(ix), TO_Y(iy), t),
			          -2.0, 2.0);

			iy += blockDim.y*gridDim.y;
		}
		ix += blockDim.x*gridDim.x;
	}
}

#else /* NO_CUDA */
void plot_CPU(int width, int height, float dx, float dy, float t,
              unsigned long iter, unsigned char *img)
{
	int ix, iy;


	for (ix = 0; ix < width; ix++) {
		for (iy = 0; iy < height; iy++) {
			plotPixel(PIXEL_PTR(img, ix, iy),
			          func(TO_X(ix), TO_Y(iy), t),
			          -2.0, 2.0);
		}
	}
}
#endif /* NO_CUDA */


/* ---------------------------------------------------------------------- */
/* Wyświetlanie wykresu 2D, tzn. zapisywanie do PBO (Pixel Buffer Object) */

#ifndef NO_CUDA
void draw_GPU(void)
{
	unsigned char *gpu_bitmap;
	size_t size;


	/*
	 * Mapowanie 1-go zasobu graficznego (niezależne od środowiska)
	 * dla dostępu (zapisu) przez CUDA w strumieniu NULL (połączenie
	 * wszystkich strumieni).  Ten zasób graficzny został wcześniej
	 * 'zarejestrowany' z PBO (Pixel Buffer Object) w OpenGL.
	 */
	cudaGraphicsMapResources(1, &resource, NULL);

	/*
	 * Pobranie (mapowanie) wskaźnika do pamięci urządzenia GPU
	 * w przestrzeni adresowej CUDA, przez który będzie odbywał się
	 * dostęp do mapowanego zasobu graficznego (bitmap RGBA 32-bit)
	 */
	cudaGraphicsResourceGetMappedPointer((void**)&gpu_bitmap, &size, resource);

	/*
	 * Ustawienie ilości bloków w siatce oraz ilości wątków w bloku
	 * tak aby ich punktowy iloczyn był większy lub równy rozmiarom obrazu:
	 *
	 *     grid »*« block >= dim3(width, height, 1)
	 *
	 * Ilość wątków w bloku powinna być odpowiednio duża by zapełnić
	 * multiprocesor i umożliwić ukrywanie opóźnień przez przeplatanie
	 * obliczeń.  Powinna być wielokrotnością rozmiaru warp czyli 32.
	 *
	 *   wątków w bloku: 16*16 = 256 (rekomendowane zaczęcie od 128-256)
	 */
	dim3 grid((width  + BLOCK_DIM - 1)/BLOCK_DIM,
	          (height + BLOCK_DIM - 1)/BLOCK_DIM); // bloków w siatce
	dim3 block(BLOCK_DIM, BLOCK_DIM);              // wątków w bloku
	/*
	 * Wykonanie jądra CUDA, zapisującego do wskaźnika urządzenia GPU
	 * mapowanego do zasobu graficznego OpenGL: Pixel Buffer Object
	 */
	plot_GPU<<<grid,block>>>(width, height, dx, dy, t, iter, gpu_bitmap);

	/*
	 * Odmapowanie zasobów graficznych (obiektu bufora bitmapy),
	 * zaznaczając że skończyliśmy do niego zapisywać.
	 */
	cudaGraphicsUnmapResources(1, &resource, NULL);
}

#else /* NO_CUDA */
void draw_CPU(void)
{
	/*
	 * Wykonanie obliczeń (sekwencyjnych) na CPU, zapisując do
	 * do wcześniej zaalokowanej pamięci gospodarza (CPU).
	 */
	plot_CPU(width, height, dx, dy, t, iter, cpu_pixmap);

	/*
	 * Skopiowanie danych z pamięci gospodarza CPU do obiektu bufora,
	 * tzn. PBO (Pixel Buffer Object), który jest obiektem w pamięci
	 * GPU dostępnym przez uchwyt OpenGL.  glBufferSubData() jest
	 * jakby odpowiednikiem memcpy().
	 *
	 * Alternatywnym rozwiązaniem, używanym przez common/cpu_bitmap.h
	 * z "CUDA by Example" jest przekazanie wskaźnika do pamięci CPU
	 * przy malowaniu za pomocą glDrawPixels().
	 */
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, bufferObj);
	glBufferSubData(GL_PIXEL_UNPACK_BUFFER_ARB,
	                0 /* offset */, width * height * 4,
	                cpu_pixmap);
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, 0);
}
#endif /* NO_CUDA */

/* ---------------------------------------------------------------------- */
/* wyliczanie FPS */

/*
 * Napisana wzorując się na kodzie programu demonstracyjnego 'glxgears'
 * http://cgit.freedesktop.org/mesa/demos/tree/src/demos/gears.c
 * w domenie publicznej (public domain).
 *
 * Wywoływane co każdą ramkę w displayFunc(), odpowiedzialne za
 * aktualizację zmiennej fps_frames.  Funkcja ta co około 5 sekund
 * oblicza i wypisuje ilość ramek na sekundę na podstawie ilości ramek
 * fps_frames i czasu od ostatniego wypisania (lub początku programu),
 * oraz resetuje okres zliczania ramek.
 */
void calculate_and_print_fps()
{
	int t_curr_ms = glutGet(GLUT_ELAPSED_TIME);
	double dt, fps;


	// zliczamy ramki (frames)
	fps_frames++;
	// sprawdzenie poprawności i ewentualny restart
	if (fps_start < 0)
		fps_start = t_curr_ms;

	// wypisujemy FPS co 5 sekund
	if (t_curr_ms - fps_start >= 5000) {
		dt = (t_curr_ms - fps_start) / 1000.0;
		fps = fps_frames / dt;

		printf("%lu frames in %6.3f seconds = %6.3f FPS / %6.3f ms per frame\n",
		       fps_frames, dt, fps, 1000.0*dt / fps_frames);
		fflush(stdout);
		eprintf(" t = %g sec, iter = %lu\n", t, iter);
		fflush(stderr);

		// restart - nowy okres czasu
		fps_start = t_curr_ms;
		fps_frames = 0;
	}
}


/* ---------------------------------------------------------------------- */
/* OpenGL */

/* ...................................................................... */
/* alokowanie i zwalnianie zasobów OpenGL i CUDA / CPU */

// alokowanie buforów OpenGL i rejestrowanie ich dla CUDA,
// lub alokacja pamięci CPU na bitmapę
void initGLBuffers(int width, int height)
{
	eprintf("Initializing OpenGL buffers [%s]...\n", __FUNCTION__);
	eprintf(" Creating PBO...\n");

	// tworzymy 1 uchwyt do obiektu bufora (którym będzie PBO)
	glGenBuffers(1, &bufferObj);
	/*
	 * Wiążemy bufor typu GL_PIXEL_UNPACK_BUFFER, używany do
	 * asynchronicznego wysyłania (upload) do właśnie utworzonego
	 * uchwytu do obiektu bufora (do pamięci GPU, kontrolowanej przez
	 * OpenGL).
	 *
	 * Polecenie glDrawPixels() jest operacją odpakowania ("unpack")
	 * bufora.  Kiedy taki bufor jest aktywny, operacja ta czyta
	 * (odpakowuje) dane pikseli z PBO i kopiuje do framebufora
	 * OpenGL, używając asynchronicznego transferu za pomocą DMA
	 * (Direct Memory Access).
	 */
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER, bufferObj);
	/*
	 * Tworzenie (odpowiednik malloc()) i inicjalizacja (ewentualnie
	 * połączona z kopiowaniem danych z pamięci CPU, ale nie w tym
	 * przypadku) obszaru przechowywania danych obiektu bufora.
	 *
	 * Alokacja pamięci GPU (zarządzanej przez OpenGL) dla bufora
	 * GL_PIXEL_UNPACK_BUFFER powiązanego do odpowiedniego uchwytu
	 * obiektu bufora.  NULL oznacza brak kopiowania danych z CPU.
	 *
	 * GL_DYNAMIC_DRAW oznacza, że OpenGL powinien zakładać że dane
	 * w buforze są wielokrotnie modyfikowane i używane wiele razy
	 * (DYNAMIC), i są zapisywane (modyfikowane) przez aplikację
	 * i używane przez OpenGL jako źródło do rysowania (DRAW).
	 */
	glBufferData(GL_PIXEL_UNPACK_BUFFER, width * height * 4,
	             NULL, GL_DYNAMIC_DRAW);


#ifndef NO_CUDA
	/*
	 * Rejestrowanie zasobu OpenGL: obiektu bufora.  Dla CUDA ten
	 * zasób graficzny jest widoczny jako wskaźnik urządzenia GPU,
	 * a zatem może być odczytywany i zapisywany przez jądra, lub
	 * przez cudaMemcpy().  Dalsze mapowanie zasobów na wskaźnik
	 * pamięci CUDA GPU odbywa się za pomocą interfejsu niezależnego
	 * od biblioteki graficznej (OpenGL lub różne wersje Direct3D).
	 *
	 * Użycie flagi cudaGraphicsRegisterFlagsWriteDiscard specyfikuje,
	 * że CUDA nie będzie czytać z tego zasobu, a zapis odbywa się
	 * po całej przestrzeni zasobu, nie zachowując żadnych poprzednich
	 * danych.  Wpływa to na odpowiednią optymalizację dostępu.
	 */
	eprintf(" Registering PBO [%u]...\n", (unsigned int)bufferObj);
	cudaGraphicsGLRegisterBuffer(&resource, bufferObj,
	                             cudaGraphicsMapFlagsWriteDiscard);

	eprintf("PBO created and registered [OpenGL:%u/GPU:%p] (done).\n",
	        bufferObj, resource);
#else /* NO_CUDA */
	eprintf(" Allocating CPU bitmap/pixmap...\n");
	// Alokowanie pamięci gospodarza (CPU) na bitmap/pixmap
	cpu_pixmap = malloc(width * height * 4);

	eprintf("PBO created and registered [OpenGL:%u/CPU:%p] (done).\n",
	        bufferObj, cpu_pixmap);
#endif /* NO_CUDA */

	// odwiązanie (odbindowanie) wszystkich obiektów buforów
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
}

// zwalnianie zasobów OpenGL i CUDA / CPU
void freeGLBuffers(void)
{
	eprintf("Destroying PBO [%u]...\n", (unsigned int)bufferObj);
#ifndef NO_CUDA
	// odrejestrowanie zasobu graficznego,
	// tak że nie będzie dostępny dla CUDA
	eprintf(" unregistering resource %p\n", resource);
	cudaGraphicsUnregisterResource(resource);
#else
	// zwolnienie pamięci bitmapy na CPU
	eprintf(" freeing CPU pixmap %p\n", cpu_pixmap);
	free(cpu_pixmap);
#endif

	eprintf(" deleting buffer %u (PBO)\n", bufferObj);
	// odwiąż (odbinduj) wszystkie obiekty buforów,
	// aby można było zwolnić ich pamięć
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
	// usuń 1 obiekt bufora o podanym uchwycie
	glDeleteBuffers(1, &bufferObj);

	eprintf("PBO unregistered and deleted (done).\n");
}


/* ...................................................................... */
/* pomocnicze funkcje grafiku OpenGL */

// wyczyszczenie okna
void clear(void)
{
	// ustawienie koloru czyszczenia bufora
	// RGBA, każda składowa w zakresie od 0.0 do 1.0
	glClearColor(0.0, 0.0, 0.0, 1.0); // czarny
	// wyczyszczenie bufora zapisu kolorów (wyświetlania)
	// za pomocą wcześniej ustawionego koloru
	glClear(GL_COLOR_BUFFER_BIT);
	// wymuszenie opróżnienia buforów / wykonania poleceń;
	// zapewnia że operacje zostaną wykonane w skończonym
	// czasie, tzn. zakolejkowane w OpenGL, ale nie blokuje
	// (być może niepotrzebne przy podwójnym buforowaniu)
	glFlush();
}



/* ---------------------------------------------------------------------- */
/* Funkcje wywołań zwrotnych (callback) obsługi zdarzeń w GLUT */

// callback bezczynności bieżącego okna w GLUT
/*
 * Celem tego callback w GLUT jest obsługa wykonywania zadań
 * przetwarzania w tle, lub ciągła animacja.  Callback ten jest
 * wywoływany gdy nie są otrzymywane żadne inne zdarzenia.
 *
 * Zalecane jest by ilość obliczeń w tym wywołaniu zwrotnym (callback)
 * nie była zbyt dużą, by nie wpływać negatywnie na responsywność
 * i interaktywność programu.
 *
 * Dlatego też ta funkcja, podążając za przykładem 'glxgears',
 * a w przeciwieństwie do 'gpu_anim.h' z programów w materiałach
 * pomocniczych "CUDA by Example", dokonuje wyłącznie wyliczania czasu
 * i wywołuje funkcję wyświetlania za pomocą glutPostRedisplay()
 * (a właściwie kolejkuje wyświetlanie w pętli zdarzeń programu).
 */
void idleFunc(void)
{
	static int t_prev_ms = -1; // czas rozpoczęcia animacji, milisekundy
	int t_curr_ms; // bieżący czas, milisekundy
	double dt; // czas od ostatniej iteracji, w sekundach

	// glutGet(GLUT_ELAPSED_TIME) zwraca liczbę milisekund od glutInit()
	// lub pierwszego wywołania glutGet(GLUT_ELAPSED_TIME);
	t_curr_ms = glutGet(GLUT_ELAPSED_TIME);
	if (t_prev_ms < 0)
		t_prev_ms = t_curr_ms;

	dt = (t_curr_ms - t_prev_ms)/1000.0;
	t_prev_ms = t_curr_ms;

	// próba ominięcia problemów z "zawijaniem" czasu
	t += dt; // czas w sekundach do animacji
	iter++;  // liczba iteracji od początku animacji

	// wywołuje displayFunc
	glutPostRedisplay();
}


// callback wyświetlania bieżącego okna w GLUT
void displayFunc(void)
{
	// wyczyszczenie okna (zapis do bufora kolorów)
	glClearColor(0.0, 0.0, 0.0, 1.0); // czarny
	glClear(GL_COLOR_BUFFER_BIT);

#ifndef NO_CUDA
	// użyj CUDA do rysowania na mapowanym zasobie OpenGL
	draw_GPU();
#else  /* NO_CUDA */
	// użyj CPU do rysowania w pamięci GPU, skopiuj do OpenGL
	draw_CPU();
#endif /* NO_CUDA */

	/*
	 * Odczyt surowych danych pikseli z pamięci obiektu bufora PBO
	 * i zapis do framebuffer do wyświetlenia.  Każdy piksel składa
	 * się z czterech komponentów (GL_RGBA): czerwony (Red), zielony
	 * (Green), niebieski (Blue) i kanał alfa widoczności (Alpha).
	 * Każdy komponent (kolor) reprezentowany jest za pomocą 8-bitowej
	 * wartości (bajtu) bez znaku (GL_UNSIGNED_BYTE).
	 *
	 * Ponieważ niezerowy obiekt bufora powiązany jest do celu
	 * GL_PIXEL_UNPACK_BUFFER, to glDrawPixels rysuje z tego bufora,
	 * a ostatni parametr tej funkcji jest przesunięciem (offset)
	 * w bajtach odczytu z obszaru danych tego bufora.
	 */
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER, bufferObj);
	glDrawPixels(width, height, GL_RGBA,
	             GL_UNSIGNED_BYTE, 0 /* offset */);
	// odwiąż (odbinduj) wszystkie obiekty buforów,
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);

	/*
	 * Wykonuje zamianę (swap) dla warstwy używanej przez bieżące
	 * okno.  Bufor tylnej warstwy (back buffer), na którym były
	 * wykonywane operacje graficzne jak drukowanie bitmapy, zostaje
	 * promowany do bufora przedniej warstwy (front buffer) i
	 * wyświetlony.  Zawartość bufora tylnej warstwy staje się
	 * niezdefiniowana.  Uaktualnienie odbywa się zazwyczaj podczas
	 * odświeżania zawartości monitora (retrace).
	 *
	 * Następne operacje OpenGL są kolejkowane, ale nie są wykonywane
	 * póki operacja zamiany buforów się nie zakończy; niejawne glFlush().
	 *
	 * Jeśli bieżąca warstwa (layer) nie jest podwójnie buforowana,
	 * funkcja ta nie daje żadnego efektu.
	 */
	glutSwapBuffers();

	// zliczanie FPS, i ewentualne drukowanie
	calculate_and_print_fps();
}


// callback klawiatury dla bieżącego okna w GLUT
void keyboardFunc(unsigned char key, int x, int y)
{
	// stan klawiszy modyfikujących: Shift, Ctrl, Alt
	int modifier = glutGetModifiers();


	eprintf("- received key '%1$c' (%1$d/0x%1$02x) "
	        "with modifier %2$d at (%3$d,%4$d)\n",
	        key, modifier, x, y);
	switch (key) {
	// wyjście z programu
	case 27: /* ESC */
	case 'q':
	case 'Q':
		printf("Shutting down...\n");
		freeGLBuffers();
		exit(EXIT_SUCCESS);
		break;

	default:
		break;
	}
}

// callback zmiany rozmiaru (kształtu) bieżącego okna dla GLUT,
// używany w naszym programie do zapewnienia stałych wymiarów
void reshapeFunc(int _width, int _height)
{
	// nie wykonuj nic, jeśli rozmiar się nie zmienił
	if (width == _width && height == _height)
		return;

	// jeśli rozmiar się zmienił, ignoruj go, zmień rozmiar na oryginalny
	// uwaga: wywołuje ponownie callback zmiany kształtu
	glutReshapeWindow(width, height);
	eprintf("Resized back to %dx%d from %dx%d\n",
	        width, height, _width, _height);
}


/* ---------------------------------------------------------------------- */
/* Inicjalizacja CUDA i GLUT */

#ifndef NO_CUDA
// inicjalizacja CUDA (jeśli potrzebne)
void initCUDA(void)
{
	eprintf("Initializing CUDA device %d for OpenGL...\n", 0);
	// jawnie użyj urządzenia 0 do obliczeń CUDA i grafiki OpenGL;
	// listę urządzeń CUDA powiązanych z bieżącym kontekstem OpenGL
	// można uzyskać za pomocą cudaGLGetDevices()
	// UWAGA: niepotrzebne od CUDA 5.0
	cudaGLSetGLDevice(0);
	eprintf("CUDA initialized (done)\n");
}
#endif /* !NO_CUDA */


// inicjalizacja interfejsu graficznego w GLUT
void initGLUT(int *argc, char *argv[])
{
	GLenum err;


	eprintf("Initializing GLUT...\n");
	/*
	 * Inicjalizacja biblioteki GLUT, interpretując specyficzne dla
	 * GLUT i systemu okienkowego parametry wykonania, ekstrahując
	 * i usuwając te parametry (dla X Window System np. -gldebug
	 * oraz -display <DISPLAY>).
	 */
	glutInit(argc, argv);

	/*
	 * Ustawienie początkowego trybu wyświetlania, rozmiarów okna oraz
	 * ewentualnie jego położenia.  Początkowy tryb wyświetlania to
	 * model kolorów RGBA (GLUT_RGBA) z podwójnym buforowaniem
	 * (GLUT_DOUBLE), bez komponentu alfa w buforze kolorów
	 * (GLUT_ALPHA).
	 */
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowSize(width, height);
	//glutInitWindowPosition(0, 0);

	// utworzenie okna głównego (top-level) o zadanej nazwie (tytule)
	glutCreateWindow(argv[0]);
	eprintf(" window size %d x %d\n",
	        glutGet(GLUT_WINDOW_WIDTH),
	        glutGet(GLUT_WINDOW_HEIGHT));

	/*
	 * Pliki nagłówkowe bibliotek OpenGL domyślnie udostępniają tylko
	 * podstawowe funkcje OpenGL, na przykład tylko funkcje standardu
	 * OpenGL 1.1.  Funkcje spoza tego zakresu, na przykład
	 * glGenBuffers, glBindBuffer, glReadBuffer itp. ze standardu
	 * OpenGL 1.5, potrzebne do używania PBO (Pixel Buffer Object)
	 * wymagają 'załadowania', tzn. pobrania ich adresów.
	 *
	 * Najprostszym rozwiązaniem jest użycie biblioteki do zarządzania
	 * rozszerzeniami OpenGL jak GLEW lub GLee.
	 */
	// inicjalizacja biblioteki GLEW i sprawdzenie jej poprawności
	eprintf(" Initializing GLEW...\n");
	err = glewInit();
	if (err != GLEW_OK) {
		/* Problem: glewInit nie powiodło się, pojawił się znaczący błąd */
		fprintf(stderr, "GLEW Error: %s\n", glewGetErrorString(err));
		exit(EXIT_FAILURE);
	}
	// wyświetlenie pewnych informacji o środowisku i bibliotekach,
	// podobnie do glewinfo czy glxinfo (dla GLX z X Window System)
	eprintf(" Status: Using GLEW %s\n", glewGetString(GLEW_VERSION));
	eprintf(" OpenGL vendor string: %s\n", glGetString(GL_VENDOR));
	eprintf(" OpenGL renderer string: %s\n", glGetString(GL_RENDERER));
	eprintf(" OpenGL version string: %s\n", glGetString(GL_VERSION));
	eprintf(" GLU version string: %s\n", gluGetString(GLU_VERSION));
	// sprawdzenie rozszerzeń i wersji, czy PBO dostępne (GLEW >= 1.3.0)
	if (!GL_VERSION_1_5) {
		fprintf(stderr, "OpenGL 1.5 (needed for glBindBuffer) not supported\n");
		//exit(EXIT_FAILURE);
	}

	// wyczyszczenie okna
	clear();

	// zarejestrowanie funkcji zwrotnych (callback) zdarzeń GLUT
	glutIdleFunc(idleFunc);         // animacja
	glutDisplayFunc(displayFunc);   // wyświetlanie
	glutKeyboardFunc(keyboardFunc); // obsługa klawiszy
	glutReshapeFunc(reshapeFunc);   // zmiana rozmiaru

	eprintf("GLUT initialized (done)\n");
}



/* ###################################################################### */
/* ====================================================================== */
/* ---------------------------------------------------------------------- */
/* Program główny */

int main(int argc, char *argv[])
{
	/*
	 * Obsługa [własnych] parametrów wywołania programu.
	 *
	 * UWAGA: przy większej ilości parametrów warto skorzystać z
	 * odpowiedniej biblioteki, np. funkcji getopt i getopt_long ze
	 * standardowej biblioteki C (libc), lub argp tamże, lub parseopt
	 * z git i perf, lub biblioteki popt lub argtable, lub tclap.
	 */
	if (argc > 1 &&
	    (!strcmp(argv[1], "-?") ||
	     !strcmp(argv[1], "-h") ||
	     !strcmp(argv[1], "--help"))) {
		printf("Składnia:  %1$s (-?|-h|--help)\n"
		       "           %1$s [opcje GLUT]\n", argv[0]);
		printf("\n");
		printf("Obsługiwane klawisze:\n");
		printf(" q, ESC\t wyjście z programu (quit)\n");
#ifndef NDEBUG
		printf("\n");
		printf("Program skompilowany z wyjściem debuggowania\n");
#endif /* !NDEBUG */

		exit(EXIT_SUCCESS);
	}

	/*
	 * Główna część programu
	 */
	eprintf("[%s] - Starting...\n", argv[0]);

	// inicjalizacja GLUT, CUDA oraz buforów OpenGL
	initGLUT(&argc, argv);
#if !defined(NO_CUDA) && CUDART_VERSION < 5000
	// począwszy od wersji CUDA 5.0 niepotrzebna jest jawna inicjalizacja CUDA
	// wiążąca urządzenie CUDA z kontekstem OpenGL w celu uzyskania maksymalnej
	// wydajności interoperacyjności między CUDA i OpenGL
	initCUDA();
#endif
	initGLBuffers(width, height);

	// pętla przetwarzania zdarzeń GLUT, nie wraca
	glutMainLoop();

#ifndef NO_CUDA
	// zrestartowanie urządzenia CUDA, wymusza synchronizację
	cudaDeviceReset();
#endif

	eprintf("[%s] - Done\n", argv[0]);
	return EXIT_SUCCESS;
}

/*
 * Local Variables:
 * mode: c
 * encoding: utf-8-unix
 * compile-command: "gcc -Wall -std=c99 -x c -DNO_CUDA \
 *   -o cpu_bitmap_static gpu_bitmap_static.cu \
 *   -lGL -lGLU -lglut -lGLEW -lm"
 * tab-width: 4
 * c-basic-offset: 4
 * eval: (whitespace-mode 1)
 * eval: (which-function-mode 1)
 * End:
 */

/* end of file gpu_bitmap_anim.cu */
