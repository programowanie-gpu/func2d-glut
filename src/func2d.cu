#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#ifndef _BSD_SOURCE
#define _BSD_SOURCE
#endif

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <errno.h>

#ifndef M_PI
#warning "defining M_PI"
#define M_PI 3.14159265358979323846  /* pi */
#endif

#ifndef ARRAY_SIZE
#define ARRAY_SIZE(arr) (sizeof(arr)/sizeof((arr)[0]))
#endif

// OpenGL Graphics includes
#include <GL/glew.h>
#ifdef _WIN32
#include <GL/wglew.h>
#endif
#if defined(__APPLE__) || defined(__MACOSX)
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#if defined NO_PBO && defined MAP_PBO
# error "NO_PBO and MAP_PBO are mutually exclusive"
#endif


#ifdef NO_PBO
# warning "No PBO (Pixel Buffer Object)"
# ifndef NO_CUDA
#  define NO_CUDA
#  warning "NO_PBO implies NO_CUDA"
# endif /* !NO_CUDA */
#endif /* NO_PBO */

#ifdef MAP_PBO
# warning "Mapped PBO (Pixel Buffer Object)"
# ifndef NO_CUDA
#  define NO_CUDA
#  warning "MAP_PBO implies NO_CUDA"
# endif /* !NO_CUDA */
#define MAP_BUFFER       1
#define MAP_BUFFER_RANGE 2
# if MAP_PBO != MAP_BUFFER && MAP_PBO != MAP_BUFFER_RANGE
#  warning "MAP_PBO must be MAP_BUFFER (default) or MAP_BUFFER_RANGE"
#  undef  MAP_PBO
#  define MAP_PBO MAP_BUFFER
# endif /* MAP_PBO choices */
# if   MAP_PBO == MAP_BUFFER
#  warning "MAP_PBO uses glMapBuffer"
# elif MAP_PBO == MAP_BUFFER_RANGE
#  warning "MAP_PBO uses glMapBufferRange"
# else
#  error   "Unknown MAP_PBO value"
# endif
#endif /* MAP_PBO */

#ifndef NO_CUDA
# warning "Using CUDA"
// CUDA includes
#include <cuda_runtime.h>
#include <cuda_gl_interop.h>
// two-dimensional block of threads N x N
#define BLOCK_DIM 16
#else  /* NO_CUDA */
# warning "CPU only"
// define __host__ and __device__ as no-op
#define __host__
#define __device__
typedef struct __attribute__((aligned(4)))
{
	unsigned char x, y, z, w;
} uchar4;
#endif /* NO_CUDA */


#ifndef NDEBUG
#define eprintf(...) fprintf(stderr, __VA_ARGS__)
#else
#define eprintf(...) ((void)0)
#endif

#if !defined(NDEBUG) && defined(DEBUG) && DEBUG > 1
#define dprintf(...) fprintf(stderr, __VA_ARGS__)
#else
#define dprintf(...) ((void)0)
#endif


#ifndef NO_PBO
GLuint bufferObj;
#endif /* !NO_PBO */
GLuint timeQuery;
#ifndef NO_CUDA
cudaGraphicsResource *resource;
#elif !defined MAP_PBO  /* NO_CUDA && !MAP_CUDA */
uchar4 *cpu_pixmap = NULL;
#endif /* NO_CUDA */
int width = 512, height = 256;
float dx = 0.025, dy = 0.025;
int use_gamma = 0; /* false */

#define PIXL(pixmap,ix,iy) ((pixmap) + (iy)*width + (ix))
#define to_x(ix) (dx*(ix))
#define to_y(iy) (dy*(iy))




#define OPENGL_TO_MS(x) ((x)/1000000)
struct timespec tv_get_diff(struct timespec start, struct timespec end)
{
	struct timespec temp;

	if ((end.tv_nsec-start.tv_nsec)<0) {
		temp.tv_sec  = end.tv_sec-start.tv_sec-1;
		temp.tv_nsec = 1000000000+end.tv_nsec-start.tv_nsec;
	} else {
		temp.tv_sec  = end.tv_sec-start.tv_sec;
		temp.tv_nsec = end.tv_nsec-start.tv_nsec;
	}
	return temp;
}

float tv_tick_ms(struct timespec tv)
{
	return 1000.0*tv.tv_sec + tv.tv_nsec/1000000.0;
}

/* ---------------------------------------------------------------------- */
/* CUDA */

__host__ __device__
float func(float x, float y)
{
	//return sinf(x*M_PI) + cosf(y*M_PI);
	return sinf(x*M_PI) + (2.0*(y - floorf(y)) - 1.0);
	//return (2.0*(x - floorf(x)) - 1.0) + (2.0*(y - floorf(y)) - 1.0);
}

__host__ __device__
float gamma_compression(float value /* 0..1 */)
{
	if (value <= 0.0031308)
		return 12.92*value;
	else
		return 1.055*powf(value, 1/2.4) - 0.055;
}

__host__ __device__
float gamma_expansion(float value /* 0..1 */)
{
	if (value <= 0.04045)
		return value/12.92;
	else
		return powf((value + 0.055)/1.055, 2.4);
}

__host__ __device__
void plotPixel(uchar4 *pixel, float val, float min, float max, int use_gamma)
{
	if (max < min) {
		float tmp = min;
		min = max;
		max = tmp;
	}

	if (val < min) {
		// alternative solution: val = min;
		// blue for below minimum
		pixel->x = 0; pixel->y = 0; pixel->z = 255; pixel->w = 255;
		return;
	}
	if (val > max) {
		// alternative solution: val = max;
		// red for above minimum
		pixel->x = 255; pixel->y = 0; pixel->z = 0; pixel->w = 255;
		return;
	}

	// rescale to 0.0..1.0
	val = (val - min)/(max - min);
	// gamma compression of linear luminance
	// http://en.wikipedia.org/wiki/Grayscale#Converting_color_to_grayscale
	if (use_gamma) {
		val = gamma_compression(val);
	}

	pixel->x =
		pixel->y =
		pixel->z = (char)floor(255.0*val);
	pixel->w = 255;
}

#ifndef NO_CUDA
__global__
void plot_GPU(int width, int height, float dx, float dy, int use_gamma,
              uchar4 *img)
{
	int ix = blockIdx.x*blockDim.x + threadIdx.x;
	int iy = blockIdx.y*blockDim.y + threadIdx.y;

	if (ix >= width || iy >= height)
		return;

	while (ix < width) {
		while (iy < height) {
			plotPixel(PIXL(img, ix, iy),
			          func(to_x(ix), to_y(iy)),
			          -2.0, 2.0, use_gamma);

			iy += blockDim.y*gridDim.y;
		}
		ix += blockDim.x*gridDim.x;
	}

#if !defined(NDEBUG) && defined(DEBUG) && DEBUG > 2
	if (ix % 32 == 0 && iy % 32 == 0) {
		int x = to_x(ix);
		int y = to_y(iy);
		uchar4 *pixel = PIXL(img, ix, iy);

		printf("  [%3d,%3d] = [%4.1f,%4.1f]: % 9.6f, (%3d,%3d,%3d,%3d)\n",
		       ix, iy, x, y, func(x, y),
		       pixel->x,
		       pixel->y,
		       pixel->z,
		       pixel->w);
	}
#endif /* extra debugging */
}
#else /* NO_CUDA */
void plot_CPU(int width, int height, float dx, float dy, int use_gamma,
              uchar4 *img)
{
	for (int ix = 0; ix < width; ix++)
		for (int iy = 0; iy < height; iy++)
			plotPixel(PIXL(img, ix, iy),
			          func(to_x(ix), to_y(iy)),
			          -2.0, 2.0, use_gamma);
}
#endif /* NO_CUDA */

#ifndef NO_CUDA
void draw_GPU(void)
{
	uchar4 *devPtr;
	size_t size;

	dprintf("Draw image with CUDA...\n");
	// map 1 graphics resource for access by CUDA in stream NULL
	// i.e. map buffer object for writing from CUDA
	cudaGraphicsMapResources(1, &resource, NULL);
	dprintf(" mapped %d resource(s)\n", 1);
	// get device pointer through which to access mapped graphics resource
	cudaGraphicsResourceGetMappedPointer((void**)&devPtr, &size, resource);
	dprintf(" got pointer %p, data size %lu\n", devPtr, (unsigned long)size);

	// run kernel writing to device pointer mapped to graphics resource
	dim3 grid((width  + BLOCK_DIM - 1)/BLOCK_DIM,
	          (height + BLOCK_DIM - 1)/BLOCK_DIM);
	dim3 block(BLOCK_DIM, BLOCK_DIM);
	plot_GPU<<<grid,block>>>(width, height, dx, dy, use_gamma, devPtr);
	dprintf(" plot_GPU<<<(%d,%d,%d),(%d,%d,%d)>>>(%i, %i, %g, %g, %s, %p)\n",
	        grid.x,grid.y,grid.z, block.x,block.y,block.z,
	        width, height, dx, dy, use_gamma ? "true" : "false", devPtr);

	// unmap buffer object
	cudaGraphicsUnmapResources(1, &resource, NULL);

	// swap buffers
	//glutSwapBuffers();
	// mark the current window as needing to be redisplayed
	// calls OpenGL display function (displayFunc)
	//glutPostRedisplay();
}
#else /* NO_CUDA */
void draw_CPU(void)
{
#ifdef MAP_PBO
	uchar4 *cpu_pixmap;
	GLboolean res;
#endif
	//size_t size = width * height * sizeof(uchar4);
	//uchar4 *pixmap;
	/*
	 * alt:
	 * void *glMapBufferRange(GLenum target,
	 *                        GLintptr offset,
	 *                        GLsizeiptr length,
	 *                        GLbitfield access);
	 */

	dprintf("Draw image on CPU... ");
#if   defined NO_PBO
	dprintf("using glDrawPixels(..., cpu_pixmap)\n");
#elif defined MAP_PBO
# if MAP_PBO == MAP_BUFFER
	dprintf("using glMapBuffer / glUnmapBuffer\n");
# else /* MAP_PBO == MAP_BUFFER_RANGE */
	dprintf("using glMapBufferRange / glUnmapBuffer\n");
# endif
#else
	dprintf("using glBufferSubData\n");
#endif

#ifdef MAP_PBO
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, bufferObj);
# if MAP_PBO == MAP_BUFFER
	cpu_pixmap =
		(uchar4 *)glMapBuffer(GL_PIXEL_UNPACK_BUFFER_ARB,
		                      GL_WRITE_ONLY);
# else /* MAP_PBO == MAP_BUFFER_RANGE */
	cpu_pixmap =
		(uchar4 *)glMapBufferRange(GL_PIXEL_UNPACK_BUFFER_ARB,
		                           0, width * height * 4,
		                           GL_MAP_WRITE_BIT |
		                           GL_MAP_INVALIDATE_BUFFER_BIT |
		                           GL_MAP_UNSYNCHRONIZED_BIT);
# endif
	dprintf(" mapped [%d] PBO to %p\n", bufferObj, cpu_pixmap);
#endif /* MAP_PBO */


	plot_CPU(width, height, dx, dy, use_gamma, cpu_pixmap);
	dprintf(" plot_CPU(%i, %i, %g, %g, %s, %p)\n",
	        width, height, dx, dy, use_gamma ? "true" : "false", cpu_pixmap);

#if !defined NO_PBO && !defined MAP_PBO
	/*
	 * void glBufferSubData(GLenum target,
	 *                      GLintptr offset,
	 *                      GLsizeiptr size,
	 *                      const GLvoid * data);
	 */
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, bufferObj);
	glBufferSubData(GL_PIXEL_UNPACK_BUFFER_ARB,
	                0, width * height * 4,
	                cpu_pixmap);
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, 0);
	dprintf(" copied [%p] CPU data to [%u] OpenGL PBO\n",
	        cpu_pixmap, bufferObj);
#elif defined MAP_PBO
	res = glUnmapBuffer(GL_PIXEL_UNPACK_BUFFER_ARB);
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, 0);
	dprintf(" unmapped PBO %s\n",
	        res == GL_TRUE ? "successfully" : "unsuccessfully");
#endif /* !NO_PBO && !MAP_PBO */

	// swap buffers
	//glutSwapBuffers();
	// mark the current window as needing to be redisplayed
	// calls OpenGL display function (displayFunc)
	//glutPostRedisplay();
}
#endif /* NO_CUDA */

/* ...................................................................... */
/* image */
void fprintf_bitmap_ppm_text(FILE *outfile, uchar4 *pixmap,
                             int width, int height)
{
	int ix, iy;
	uchar4 *pixel;


	// header
	fprintf(outfile, "P3\n");
	// comments
	fprintf(outfile, "# Portable PixMap ASCII (text) format\n");
	fprintf(outfile, "# width %d height %d\n", width, height);
	fprintf(outfile, "# RGB triplets with %d for max color\n", 255);
	fprintf(outfile, "#\n");
	fprintf(outfile, "# func2d\n");
	fprintf(outfile, "%d %d\n", width, height);
	fprintf(outfile, "%d\n", 255);
	for (iy = 0; iy < height; iy++) {
		fprintf(outfile, "# row %d\n", iy);
		for (ix = 0; ix < width; ix++) {
			pixel = PIXL(pixmap, ix, iy);
			fprintf(outfile, "%03d %03d %03d\n", pixel->x, pixel->y, pixel->z);
		}
	}
}

void write_bitmap_ppm_text(const char *filename)
{
	FILE *outfile = NULL;
	int errnum = 0;
	uchar4 *pixmap = NULL;
#ifdef MAP_PBO
	GLboolean res;
#endif /* MAP_PBO */
#ifndef NO_CUDA
	uchar4 *gpu_pixmap = NULL;
	size_t size;


	eprintf("Writing GPU image to file\n");
	cudaGraphicsMapResources(1, &resource, NULL);
	eprintf(" mapped %d resource(s)\n", 1);
	cudaGraphicsResourceGetMappedPointer((void**)&gpu_pixmap, &size, resource);
	eprintf(" got pointer %p, data size %lu\n",
	        gpu_pixmap, (unsigned long)size);
	cudaMallocHost((void **)&pixmap, size);
	eprintf(" allocated %lu memory on host %p\n", size, pixmap);
	cudaMemcpy(pixmap, gpu_pixmap, size, cudaMemcpyDeviceToHost);
	eprintf(" copied %lu data from %p GPU to %p CPU\n",
	        (unsigned long)size, gpu_pixmap, pixmap);
#else  /* NO_CUDA */


	eprintf("Writing CPU image to file\n");
# ifndef MAP_PBO
	pixmap = cpu_pixmap;
# else
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, bufferObj);
#  if MAP_PBO == MAP_BUFFER
	pixmap =
		(uchar4 *)glMapBuffer(GL_PIXEL_UNPACK_BUFFER_ARB,
		                      GL_READ_ONLY);
	eprintf(" mapped PBO buffer [%d] for reading to %p\n",
	        bufferObj, pixmap);
#  else /* MAP_PBO == MAP_BUFFER_RANGE */
	pixmap =
		(uchar4 *)glMapBufferRange(GL_PIXEL_UNPACK_BUFFER_ARB,
		                           0, width * height * 4,
		                           GL_MAP_READ_BIT);
	eprintf(" mapped %lu data in PBO buffer [%d] for reading to %p\n",
	        (unsigned long)width * height * 4, bufferObj, pixmap);
#  endif
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, 0);
# endif /* MAP_PBO */
#endif /* NO_CUDA */
	eprintf(" using pixmap %p (on CPU, to write to file)\n",
	        pixmap);

	eprintf(" opening file '%s' for writing\n", filename);
	outfile = fopen(filename, "w");
	if (!outfile) {
		errnum = errno;
		fprintf(stderr, "Nie udało się utworzyć pliku '%s' do zapisu:\n %s\n",
		        filename, strerror(errnum));
		return;
	}

	fprintf_bitmap_ppm_text(outfile, pixmap, width, height);

	fclose(outfile);
#ifndef NO_CUDA
	cudaFreeHost(pixmap);
	eprintf(" freed host memory\n");
#elif defined MAP_PBO
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, bufferObj);
	res = glUnmapBuffer(GL_PIXEL_UNPACK_BUFFER_ARB);
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, 0);
	eprintf(" unmapped [%d] PBO %s\n", bufferObj,
	        res == GL_TRUE ? "successfully" : "unsuccessfully");
#endif
	eprintf("(done writing)\n");
}

/* ...................................................................... */
/* OpenGL */
void freeGLthings(void);

void clear(void)
{
	glClearColor(0.0, 0.0, 0.0, 1.0); // RGBA
	glClear(GL_COLOR_BUFFER_BIT); // color writing
	glFlush();
}

// OpenGL display function
void displayFunc(void)
{
#ifndef NDEBUG
	static int ncalls = 1;
	GLuint timeElapsedGL = 0;
#ifndef NO_CUDA
	cudaEvent_t cuda_ev[4];
	float timeElapsedCUDA = 0.0;

	for (int i = 0; i < ARRAY_SIZE(cuda_ev); i++)
		cudaEventCreate(&cuda_ev[i]);
#else  /* NO_CUDA */
	clockid_t clk = CLOCK_MONOTONIC;
	struct timespec tv_res, tv[4], tv_diff;
	clock_getres(clk, &tv_res);
#endif /* NO_CUDA */
#endif /* !NDEBUG */

	eprintf("- %i: [%s]\n", ncalls++, __FUNCTION__);

#ifndef NDEBUG
	glBeginQuery(GL_TIME_ELAPSED, timeQuery);
#ifndef NO_CUDA
	cudaEventRecord(cuda_ev[0], NULL);
#else  /* NP_CUDA */
	clock_gettime(clk, &tv[0]);
#endif /* NO_CUDA */
#endif /* !NDEBUG */
	// clear window (color buffer, written to)
	glClearColor(0.0, 0.0, 0.0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT);

#ifndef NO_CUDA
#ifndef NDEBUG
	cudaEventRecord(cuda_ev[1], NULL);
#endif /* !NDEBUG */
	// run CUDA to draw on mapped OpenGL resource
	draw_GPU();
#ifndef NDEBUG
	cudaEventRecord(cuda_ev[2], NULL);
#endif /* !NDEBUG */

#else  /* NO_CUDA */
#ifndef NDEBUG
	clock_gettime(clk, &tv[1]);
#endif /* !NDEBUG */
	draw_CPU();
#ifndef NDEBUG
	clock_gettime(clk, &tv[2]);
#endif /* !NDEBUG */

#endif /* NO_CUDA */

#ifndef NO_PBO
	// read pixel data from memory and write to framebuffer
	// each pixel is four-component group: red, green, blue, alpha
	// one color / depth component is unsigned 8-bit byte
	// all RGBA components are uchar4 (4 * 8-bit byte)
	// pixel data is read from buffer object's store with offset 0
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, bufferObj);
	glDrawPixels(width, height, GL_RGBA,
	             GL_UNSIGNED_BYTE, 0);
#else /* !NO_PBO */
	// draw pixels from CPU memory
	glDrawPixels(width, height, GL_RGBA,
	             GL_UNSIGNED_BYTE, cpu_pixmap);
#endif /* !NO_PBO */

	// swap front and back buffers, displaying what was created
	// it also does glFlush();
	glutSwapBuffers();

#ifndef NDEBUG
	glEndQuery(GL_TIME_ELAPSED);
	glGetQueryObjectuiv(timeQuery, GL_QUERY_RESULT, &timeElapsedGL);
#ifndef NO_CUDA
	cudaEventRecord(cuda_ev[3], NULL);
#else  /* NO_CUDA */
	clock_gettime(clk, &tv[3]);
#endif /* NO_CUDA */

#ifndef NO_CUDA
	cudaEventSynchronize(cuda_ev[3]);

	printf("# 1:OpenGL[ms]  2:CUDA[ms]  3:CUDA_kernel[ms]\n");
	printf("%9f ", OPENGL_TO_MS((float)timeElapsedGL));
	cudaEventElapsedTime(&timeElapsedCUDA, cuda_ev[0], cuda_ev[3]);
	printf("%9f ",  timeElapsedCUDA);
	cudaEventElapsedTime(&timeElapsedCUDA, cuda_ev[1], cuda_ev[2]);
	printf("%9f\n", timeElapsedCUDA);
#else  /* NO_CUDA */
	printf("# CLOCK_MONOTONIC resolution: %ld sec %ld nsec\n",
	       (long)tv_res.tv_sec, (long)tv_res.tv_nsec);

	printf("# 1:OpenGL[ms]  2:CPU[ms]  3:CPU_kernel[ms]\n");
	printf("%9f ",  OPENGL_TO_MS((float)timeElapsedGL));
	tv_diff = tv_get_diff(tv[0], tv[3]);
	printf("%9f ",  tv_tick_ms(tv_diff));
	tv_diff = tv_get_diff(tv[1], tv[2]);
	printf("%9f\n", tv_tick_ms(tv_diff));
#endif /* NO_CUDA */
#endif /* !NDEBUG */
}

// OpenGL idle function
void idleFunc(void)
{
	// ...

	glutPostRedisplay();
}

// OpenGL keyboard function
void keyboardFunc(unsigned char key, int x, int y)
{
	eprintf("received key '%c' at (%d,%d)\n", key, x, y);

	switch (key) {
	case 'g':
	case 'G':
		use_gamma = !use_gamma;
		eprintf(" use_gamma = %d\n", use_gamma);
		glutPostRedisplay(); // force redraw
		break;

	case 'r':
	case 'R':
		eprintf("replot\n");
		glutPostRedisplay(); // force redraw
		break;

	case 's':
	case 'S':
		eprintf("save to PPM file\n");
		write_bitmap_ppm_text("func2d.ppm");
		break;

	case 'q':
	case 'Q':
		printf("Shutting down...\n");
		freeGLthings();
		exit(EXIT_SUCCESS);
		break;

	default:
		break;
	}
}

// OpenGL reshape function
void reshapeFunc(int _width, int _height)
{
	if (width == _width && height == _height)
		return;

	// ignore resizing, reset size to original
	glutReshapeWindow(width, height);
	eprintf("Resized back to %dx%d from %dx%d\n",
	        width, height, _width, _height);
}

void initGL(int *argc, char *argv[])
{
	eprintf("Initializing GLUT...\n");
	glutInit(argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowSize(width, height);
	//glutInitWindowPosition(0, 0);
	glutCreateWindow(argv[0]);
	eprintf(" window size %d x %d\n",
	        glutGet(GLUT_WINDOW_WIDTH),
	        glutGet(GLUT_WINDOW_HEIGHT));
	//eprintf(" Status: Using GLUT %d\n", glutGet(GLUT_VERSION));

	// GLEW or GLee (library to manage OpenGL extensions) is needed
	// to be able to use PBO - Pixel Buffer Objects
	// or we could use the trick from "CUDA by Example"
	eprintf(" Initializing GLEW...\n");
	GLenum err = glewInit();
	if (err != GLEW_OK) {
		/* Problem: glewInit failed, something is seriously wrong. */
		fprintf(stderr, "GLEW Error: %s\n", glewGetErrorString(err));
	}
	eprintf(" Status: Using GLEW %s\n", glewGetString(GLEW_VERSION));
	eprintf(" OpenGL vendor string: %s\n", glGetString(GL_VENDOR));
	eprintf(" OpenGL renderer string: %s\n", glGetString(GL_RENDERER));
	eprintf(" OpenGL version string: %s\n", glGetString(GL_VERSION));
	eprintf(" GLU version string: %s\n", gluGetString(GLU_VERSION));
	// check extensions (GLEW >= 1.3.0)
#ifndef NDEBUG
	if (!GL_VERSION_1_5) {
		printf("OpenGL 1.5 (needed for glBindBuffer) not supported.\n");
	}
#endif /* !NDEBUG */

	clear();

	glutDisplayFunc(displayFunc);
	glutKeyboardFunc(keyboardFunc);
	glutReshapeFunc(reshapeFunc);
	eprintf("GLUT initialized (done)\n");
}

#ifndef NO_CUDA
void initCUDA(void)
{
	eprintf("Initializing CUDA device %d for OpenGL...\n", 0);
	// explicitely set device 0
	cudaGLSetGLDevice(0);
	eprintf("CUDA initialized (done)\n");
}
#endif /* !NO_CUDA */

void initGLthings(int w, int h)
{
#ifndef NO_PBO
	GLint data;
#endif /* !NO_PBO */

	eprintf("Initializing OpenGL buffers...\n");

#ifndef NO_PBO
	eprintf(" Creating PBO...\n");

	// request 1 buffer object (generate 1 handle)
	glGenBuffers(1, &bufferObj);
	// bind buffer to handle -> handle 'bufferObj' refers to memory
	// GL_PIXEL_UNPACK_BUFFER_ARB is used for async upload
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, bufferObj);
	// create and initialize a buffer object's data store
	// allocate GPU memory for the buffer bound to ..._UNPACK_BUFFER_ARB
	// do not copy data from main (CPU) memory, pass NULL
	// GL_STATIC_DRAW_ARB means that we never change the data pointed at
	// STATIC = modified once, used many times, DRAW = data modified by app
	glBufferData(GL_PIXEL_UNPACK_BUFFER_ARB, width * height * 4,
	             NULL, GL_STATIC_DRAW_ARB);
#ifndef NDEBUG
	if (glIsBuffer(bufferObj)) {
		printf(" - handle %u is buffer object\n", (unsigned int)bufferObj);
		glGetIntegerv(GL_PIXEL_UNPACK_BUFFER_BINDING, &data);
		printf(" - GL_PIXEL_UNPACK_BUFFER_BINDING:     %i\n", (int)data);
		glGetIntegerv(GL_PIXEL_UNPACK_BUFFER_BINDING_ARB, &data);
		printf(" - GL_PIXEL_UNPACK_BUFFER_BINDING_ARB: %i\n", (int)data);
	}
#endif /* !NDEBUG */
#endif /* !NO_PBO */

	// 1 query object
	glGenQueries(1, &timeQuery);
	eprintf(" Created query object [%d]\n", timeQuery);

#ifndef NO_CUDA
	eprintf(" Registering PBO [%u]...\n", (unsigned int)bufferObj);
	// specify that CUDA will not read from this resource,
	// and write over entire contents of the resource
	cudaGraphicsGLRegisterBuffer(&resource, bufferObj,
	                             cudaGraphicsMapFlagsWriteDiscard);
	//                           cudaGraphicsMapFlagsNone);

	eprintf("PBO created and registered [OpenGL:%u/GPU:%p] (done).\n",
	        bufferObj, resource);
#else /* NO_CUDA */
#ifndef MAP_PBO
	eprintf(" Allocating CPU bitmap/pixmap...\n");
	cpu_pixmap = (uchar4 *)malloc(width * height * sizeof(uchar4));
#endif /* !MAP_PBO */

#ifdef MAP_PBO
	eprintf("PBO created [OpenGL:%u] (done).\n",
	        bufferObj);
#elif !defined NO_PBO
	eprintf("PBO created and CPU bitmap allocated [OpenGL:%u/CPU:%p] (done).\n",
	        bufferObj, cpu_pixmap);
#else /* NO_PBO */
	eprintf("CPU bitmap allocated [CPU:%p] (done).\n",
	        cpu_pixmap);
#endif /* NO_PBO / MAP_PBO */
#endif /* NO_CUDA */
}

void freeGLthings(void)
{
	eprintf("Freeing graphics-related resources [%s]\n", __FUNCTION__);
#ifndef NO_CUDA
	eprintf(" unregistering graphics resource in CUDA [%p]\n", resource);
	cudaGraphicsUnregisterResource(resource);
#elif !defined MAP_PBO
	eprintf(" freeing CPU pixmap [%p]\n", cpu_pixmap);
	free(cpu_pixmap);
#endif

#ifndef NO_PBO
	eprintf(" deleting PBO [%u]...\n", (unsigned int)bufferObj);
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, 0); // unbind all buffer objects
	glDeleteBuffers(1, &bufferObj); // delete 1 buffer object
#endif /* !NO_PBO */
	eprintf(" deleting time query [%u]...\n", (unsigned int)timeQuery);
	glDeleteQueries(1, &timeQuery); // delete 1 time query

	eprintf("Unregistered, freed and deleted (done).\n");
}


// -----------------------------------------------------------------------
// -----------------------------------------------------------------------

int main(int argc, char *argv[])
{
	eprintf("[%s] - Starting...\n", argv[0]);

#ifndef NO_CUDA
	initCUDA();
#endif
	initGL(&argc, argv);
	initGLthings(width, height);

	glutMainLoop();

	//cudaDeviceReset();

	eprintf("[%s] - Done\n", argv[0]);
	return EXIT_SUCCESS;
}

/*
 * Local Variables:
 * mode: c
 * encoding: utf-8-unix
 * tab-width: 4
 * c-basic-offset: 4
 * eval: (whitespace-mode 1)
 * End:
 */

/* end of file func2d.cu */
