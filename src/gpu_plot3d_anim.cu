/*
 * Przykładowy program demonstrujący łączenie obliczeń w CUDA
 * z animacją (grafiką 3d) w OpenGL.
 *
 * Program ten generuje animację funkcji dwu zmiennych zależnej od
 * czasu f(x,y;t).  Wartości funkcji przedstawiane są jako wykres
 * trójwymiarowy, w jednolitym kolorze lub z kolorowaniem za pomocą mapy
 * kolorów (odcienie szarości, z zaznaczeniem elementów wykraczających
 * poza zakres wartości [f_min, f_max]).
 *
 * Wyświetlanie odbywa się za pomocą OpenGL (a dokładniej VBO (Vertex
 * Buffer Object) i glDrawElements).  Do integracji z środowiskiem
 * graficznym używana jest biblioteka GLUT (OpenGL Utility Toolkit).
 * Obliczenia odbywają się normalnie na karcie graficznej za pomocą
 * CUDA; jeśli zdefiniowane jest makro preprocesora NO_CUDA to
 * obliczenia odbywają się sekwencyjnie na CPU, i kopiowane na kartę
 * graficzną do PBO do wyświetlenia.
 *
 * Program ten należy skompilować dołączając biblioteki (LDLIBS)
 * -lGL -lGLU -lglut -lGLEW -lm
 *
 * Ponieważ wykorzystywane są makra o zmiennej liczbie argumentów,
 * korzystając ze składni standardu C99 raczej niż z rozszerzeń GCC,
 * program należy kompilować z -std=c99 (CFLAGS).
 *
 * Kompilacja za pomocą gcc wymaga dodatkowo wymuszenia identyfikacji
 * pliku *.cu jako zawierającego źródła w języku C: -x c (CFLAGS).
 *
 * ..................................................................
 * Written in 2015 by Jakub Narębski <jnareb@mat.umk.pl>
 *
 * To the extent possible under law, the author(s) have dedicated all
 * copyright and related and neighboring rights to this software to
 * the public domain worldwide. This software is distributed without
 * any warranty.
 *
 * http://creativecommons.org/publicdomain/zero/1.0/
 */

/* ---------------------------------------------------------------------- */
/* Dołączanie plików nagłówkowych */

// tzw. feature test macros, potrzebne do dostępu do stałej M_PI z math.h
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#ifndef _BSD_SOURCE
#define _BSD_SOURCE
#endif

// biblioteki standardowe
#include <stdio.h>  // wypisywanie informacji, m.in. printf
#include <stdlib.h> // m.in alokowanie danych, m.in. malloc
#include <math.h>   // funkcje i stałe matematyczne, m.in. sinf
#include <string.h> // do obsługi opcji programu, m.in. strcmp
#include <float.h>  // stała FLT_EPSILON, do porównywania liczb
#include <time.h>   // pomiar czasu do animacji i FPS, benchmark
#include <unistd.h> // prędkość animacji, do usleep
#include <errno.h>  // zmienna errno - przyczyna błędu


#ifndef M_PI
#define M_PI 3.14159265358979323846  /* pi */
#endif /* !M_PI */

// biblioteki grafiki OpenGL
/*
 * Aby móc używać PBO: glBindBuffer, glBufferData, itp. potrzeba
 * załadować odpowiednie rozszerzeniami OpenGL.  Najprościej
 * wykorzystać wieloplatformową bibliotekę przeznaczoną do tego celu,
 * jako że bezpośrednie zarządzanie rozszerzeniami w OpenGL (glext.h)
 * jest zależne od środowiska (np. WGL lub GLX).
 *
 * Przykładami takich bibliotek są:
 *  - GLEW (The OpenGL Extension Wrangler Library)
 *  - GLee (GL Easy Extension library) - uwaga: wygląda na nieaktywny
 *  - glsdk (Unofficial OpenGL SDK)
 *  - glbinding (C++ binding for the OpenGL API, from gl.xml specs)
 *  - Epoxy / libepoxy - nie wymaga jawnej inicjalizacji
 * W tym programie wykorzystywana jest biblioteka GLEW.
 *
 * Jej plik nagłówkowy <GL/glew.h> automatycznie ładuje odpowiednie
 * pliki nagłówkowe OpenGL, tzn.:
 *   #include <GL/glut.h>
 *   #include <GL/glext.h>
 *
 * Patrz np. http://www.opengl.org/wiki/OpenGL_Loading_Library
 *
 *
 * Przykładowe programy z "CUDA Application Design and Development"
 * także używają biblioteki GLEW; patrz programy w katalogu
 * chapter09/ archiwum MK_CUDA_5_Examples.zip
 */
#include <GL/glew.h>
#ifdef _WIN32
#include <GL/wglew.h>
#endif
/*
 * Rysowanie za pomocą OpenGL wymaga zazwyczaj napisania kodu
 * zależnego od platformy do utworzenia okna, gdzie będzie odbywać się
 * renderowanie.  Aby zapewnić wieloplatformowość, najłatwiej
 * skorzystać z odpowiedniego toolkitu.
 *
 * Ten program wykorzystuje prostą bibliotekę GLUT (The OpenGL Utility
 * Toolkit), która została utworzona w celu ułatwienie procesu uczenia
 * się OpenGL-a (a dokładniej jej forku FreeGLUT).
 *
 * http://www.opengl.org/wiki/Related_toolkits_and_APIs#Context.2FWindow_Toolkits
 */
#if defined(__APPLE__) || defined(__MACOSX)
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#define USE_PRIMITIVE_RESTART_NV
#ifdef USE_PRIMITIVE_RESTART_NV
# warning "Using NVIDIA extension for primitive restart"
#endif

#ifndef NO_CUDA
/*
 * Używamy CUDA Runtime API do obliczeń, oraz interoperacyjności
 * z OpenGL do wyświetlania, mapując VBO (Vertex Buffer Object) do
 * przestrzeni adresowej CUDA.
 *
 * http://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html#graphics-interoperability
 * http://docs.nvidia.com/cuda/cuda-runtime-api/group__CUDART__OPENGL.html
 * http://docs.nvidia.com/cuda/cuda-runtime-api/group__CUDART__INTEROP.html
 */
# warning "Using CUDA (GPU)"
// pliki nagłówkowe wysokopoziomowego CUDA Runtime API
// i plik nagłówkowy interoperacyjności CUDA z OpenGL
#include <cuda_runtime.h>
#include <cuda_gl_interop.h>
/*
 * Do obliczeń CUDA korzystamy z dwu-wymiarowych bloków wątków N x N,
 * dopasowując liczbę bloków do ilości danych (rozmiaru bitmapy).
 */
#define BLOCK_DIM 16

#else  /* NO_CUDA */
# warning "Using CPU only"
// plik nagłówkowy potrzebny by używać typów wektorowych CUDA, jak uint4 czy float3
// ale ten plik nagłówkowy pochodzi z CUDA SDK, więc jak nie ma CUDA to go nie będzie
//#include <vector_types.h>
// zamiast tego możemy 'ręcznie' zdefiniować odpowiednie typy: uchar4, float3, float4
//#if !defined(__VECTOR_TYPES_H__)
typedef struct __attribute__((aligned(4)))
{
	unsigned char x, y, z, w;
} uchar4;
typedef struct
{
	float x, y, z;
} float3;
typedef struct __attribute__((aligned(16)))
{
	float x, y, z, w;
} float4;

inline float4 make_float4(float x, float y, float z, float w)
{
	float4 res = { .x = x, .y = y, .z = z, .w = w };
	return res;
}

inline uchar4 make_uint4(unsigned char x,
                         unsigned char y,
                         unsigned char z,
                         unsigned char w)
{
	uchar4 res = { .x = x, .y = y, .z = z, .w = w };
	return res;
}
//#endif /* !__VECTOR_TYPES_H__ */

// jeśli nie używamy CUDA (i nie ma jej plików nagłówkowych) to
// definiujemy __host__ i __device__ jako no-op
#define __host__
#define __device__
#endif /* NO_CUDA */


/* ---------------------------------------------------------------------- */
/* Makrodefinicje preprocesora */

/*
 * Makro eprintf(fmt, args...) o zmiennej liczbie argumentów (składnia
 * standardu C99) jest wykorzystywane do wypisywania informacji
 * pomocniczych do debugowania programu (do standardowego strumienia
 * błędów stderr).  Nie wypisuje nic przy kompilacji programu ze
 * zdefiniowaną stałą preprocesora NDEBUG (CPPFLAGS += -DNDEBUG)
 */
#ifndef NDEBUG
#define eprintf(...) fprintf(stderr, __VA_ARGS__)
#else
#define eprintf(...) ((void)0)
#endif

/*
 * Makro ARRAY_SIZE(arr) zwraca liczbę elementów w tablicy arr.
 * Wyrażenie to jest stałą czasu kompilacji, zatem może na przykład
 * być używane przy definiowaniu nowych tablic.
 *
 * UWAGA: tą definicję preprocesora należy używać tylko na tablicach!
 */
#ifndef ARRAY_SIZE
#define ARRAY_SIZE(arr) (sizeof(arr)/sizeof((arr)[0]))
#endif

// patrz także http://stackoverflow.com/a/14568783/46058
#ifndef hypot3f
#define hypot3f(x,y,z) hypotf(hypot((x),(y)),(z))
#endif

#ifndef fmin3f
#define fmin3f(a,b,c) fminf((a),fminf((b),(c)))
#define fmax3f(a,b,c) fmaxf((a),fmaxf((b),(c)))
#endif

/* ---------------------------------------------------------------------- */
/* Definicje typów */

/*
 * Definicja typu wyliczeniowego, używanego jako indeks lub offset
 * (przesunięcie) w tablicy kolorów RGBA i RGB.  Wykorzystywana jest tylko
 * liczbowa reprezentacja wartości wyliczeniowych (jako stałe), w celu
 * lepszej czytelności programu.
 */
typedef enum rgba_coloridx {
	cR = 0,
	cG,
	cB,
	cA
} rgba_coloridx_t;

typedef enum rgb_coloridx {
	colR = 0,
	colG,
	colB
} rgb_coloridx_t;

/*
 * Definicja struktury grupującej uchwyt obiektu bufora w OpenGL
 * i dane z nim związane, z zasobem w przestrzeni adresowej CUDA
 */
typedef struct mappedBuffer {
  GLuint vbo;      // uchwyt obiektu bufora OpenGL
  GLuint typeSize; // rozmiar jednego elementu bufora
#ifndef NO_CUDA
  struct cudaGraphicsResource *cudaResource;
#endif /* !NO_CUDA */
} mappedBuffer_t;

/*
 * Definicja struktury grupującej parametry związane z tablicą
 * indeksów wierzchołków do rysowania elementów (glDrawElements),
 * dla pojedynczego typu elementu.
 */
typedef struct indices_info {
	GLenum mode;      // typ elementów do narysowania (np. trójkąty)
	GLuint *data;     // tablica indeksów wierzchołków elementów
	GLuint ibo;       // uchwyt obiektu bufora z tablicą indeksów powyżej
	size_t allocated; // ilość zaalokowanych elementów / rozmiar tablicy
	// elementy określonego rodzaju mogą zajmować część tablicy
	size_t nelems;    // ilość elementów do narysowania
	size_t offset;    // indeks (offset) pierwszego z elementów
	// używanie tzw. 'primitive restart'
	GLuint restart_index; // specjalna wartość indeksu ozn. koniec elementu
	// UWAGA: restart_index == 0 oznacza nieużywanie primitive restart
} indices_info_t;

/*
 * Definicja struktury grupującej informacje związane z przełączaniem
 * sposobu rysowania między jednym a drugim zestawem indices_info_t.
 */
typedef struct draw_mode_info {
	const char *name;
	void (*createIndices)(indices_info_t *surf);
	indices_info_t draw_mode;
} draw_mode_info_t;

typedef struct current_draw_mode_info {
	indices_info_t *current_mode_ptr; // skrót, powinien być zgodny z indeksem
	int current_mode_index, number_of_modes;
} current_draw_mode_info_t;

/*
 * Definicja struktury grupującej informacje związane z siatką (mesh)
 * punktów, w tym przypadku do generowania wykresu funkcji dwu
 * zmiennych (być może zależnej od czasu) za pomocą wykresu
 * trójwymiarowego (wysokości):
 *
 *  z = f(x, y)
 */
typedef struct mesh_info {
	unsigned int width, height; // wymiary siatki - ilość elementów w danym kierunku
	float dx, dy;               // skala siatki (rozmiar okna siatki)
} mesh_info_t;

/*
 * Definicja struktury grupującej informacje o oknie aplikacji
 * (w systemie okienkowym takim jak X Window System).
 */
#define MAX_WINDOW_TITLE_LENGTH 256
typedef struct window_info {
	int width, height;
	char title[MAX_WINDOW_TITLE_LENGTH];

	// poniższe pola bardziej charakteryzują aplikację niż okno
	int /*@ bool @*/ use_idle;  // czy używać idleFunc do animacji
	// alternatywą jest natychmiastowe ponowne wyświetlanie
	// w displayFunc() za pomocą glutPostRedisplay()
} window_info_t;

/*
 * Definicja struktury opisującej parametry wykresu funkcji dwu
 * zmiennych z = f(x,y), a dokładniej sposób generowania danych oraz
 * jej wyświetlania
 */
typedef struct splot_info {
	// zakresy wartości zmiennych niezależnych dla których będzie
	// rysowana funkcja: (x,y) \in [xmin, xmax] (x) [ymin, ymax]
	float xmin, xmax;
	float ymin, ymax;

	// skalowanie wartości wykresu, tzn. wartości 'z'
	float val_scale;

	// oczekiwane minimalne i maksymalne wartości funkcji na zadanym
	// zakresie; potrzebne do skalowania mapy kolorów colorPoint*()
	float val_min, val_max;
} splot_info_t;

/*
 * AA Box - Axis Aligned Box
 *
 * Parametry prostopadłościanu o krawędziach równoległych do osi
 * układu współrzędnych.
 */
typedef struct aa_box {
	float xmin, ymin, zmin;
	float xmax, ymax, zmax;
} aa_box_t;

/*
 * Definicja struktury do obliczania statystyk w locie;
 * powinna była być w osobnym module (osobnym pliku)
 */
typedef struct stat_float {
	float avg;  // średnia z dotychczasowych danych, 1/N \sum x_i
	float avg2; // średnia z kwadratów danych, potrzebna do odchylenia standardowego
	float max, min; // najmniejsza i największa z widzanych wartości
	unsigned long int count; // ilość widzianych danych
} stat_float_t;

/* ---------------------------------------------------------------------- */
/* Deklaracje niektórych funkcji, to co byłoby w pliku nagłówkowym projektu */

void createSurfaceTriangleFanIndices(indices_info_t *surf);
void createSurfaceTriangleFanLargeIndices(indices_info_t *surf);
void createSurfaceTrianglesIndices(indices_info_t *surf);
void createSurfaceTriangleStrip(indices_info_t *surf);

void createGridLineStrip(indices_info_t *grid);
void createGridLines(indices_info_t *grid);

/* ---------------------------------------------------------------------- */
/* Definicje zmiennych globalnych */

// specjalna wartość indeksu w rysowaniu indeksowanym oznaczająca koniec elementu
// rysowanie za pomocą glDrawElements lub podobnych, z użyciem "primitive restart"
//const GLuint restart_index = 0xffffffff;

// VBO (Voxel Buffer Object) dla voxeli i kolorów, oraz tablica indeksów
mappedBuffer_t vertexVBO = {(GLuint)0, sizeof(float4)}; // wierzchołki elementów
mappedBuffer_t colorVBO  = {(GLuint)0, sizeof(uchar4)}; // kolory wierzchołków

draw_mode_info_t surf_draw_modes[] = {
	{"triangle fans",       &createSurfaceTriangleFanIndices,      {} },
	{"triangles",           &createSurfaceTriangleFanLargeIndices, {} },
	{"triangle fans large", &createSurfaceTrianglesIndices,        {} },
	{"triangle strip",      &createSurfaceTriangleStrip,           {} },
};
current_draw_mode_info_t current_surf_mode = {
	&(surf_draw_modes[0].draw_mode),
	0, ARRAY_SIZE(surf_draw_modes),
};

draw_mode_info_t grid_draw_modes[] = {
	{"line strip", &createGridLineStrip, {}},
	{"lines",      &createGridLines, {}},
};
// TODO: użyć makr preprocesora by uniknąć duplikacji kodu
current_draw_mode_info_t current_grid_mode = {
	&(grid_draw_modes[0].draw_mode),
	0, ARRAY_SIZE(grid_draw_modes),
};
int /*@ bool @*/ display_grid = 0 /*@ false @*/;

// parametry okna, widoku, rysowania i wykresu
window_info_t window = {
	512, 256, // wymiary okna: szerokość i wysokość
#ifndef NO_CUDA
	"gpu_plot3d_anim",  // tytuł okna, obliczenia na GPU (CUDA)
#else
	"cpu_plot3d_anim",  // tytuł okna, obliczenia na CPU
#endif
	1 /*@ true @*/, // czy używać idleFunc do animacji
};

mesh_info_t mesh = {
#ifdef NO_CUDA
	// w C99 można używać nazwanych inicjalizatorów struktur,
	// czego nie ma w standardzie C++11 (a nvcc kompiluje do C i C++)
	.width = 256, .height = 256, // wymiary siatki
	.dx = 0.025,  .dy = 0.025,  // skala wykresu (rozmiar oczka siatki)
#else
	256,   256,   // wymiary siatki: width, height
	0.025, 0.025, // rozmiar oczka siatki: dx, dy
#endif
};

splot_info_t plot = {
	// zakres wartości x i y
	-128*0.025, 128*0.025, // xmin, xmax - na podstawie rozmiarów siatki
	-128*0.025, 128*0.025, // ymin, ymax - na podstawie rozmiarów siatki
	// skalowanie wartości wykresu
	1.0, // domyślnie skala taka sama dla osi Oz jak dla osi Ox i Oy
	// oczekiwane zakresy wartości funkcji, do mapy kolorów
	-2.0, 2.0, // oryginalnie sin(kx + wt) + cos(ky + wt); patrz func()
};

float val_scale = 1.0; // skalowanie wartości wykresu, tzn. wartości 'z'
// zakresy wartości wykresu, zakładając że wartości funkcji \in [-2, 2]
// inicjalizator nie jest stałą, a C99 nie ma constexpr z C++11
float xmin, ymin, zmin = -2.0;
float xmax, ymax, zmax =  2.0;
// promień pudełka obejmującego wykres (przed przeskalowaniem)
//float bounding_radius = hypot3f(xmax,ymax,zmax);

// parametry transformacji, w kolejności wykonania / aplikowania
// obszar widoku w OpenGL przed transformacjami to x=-1..1, y=-1..1
float scale; // wspólne skalowanie, przynajmniej początkowo
float scale_x, scale_y, scale_z; // skalowanie wykresu, przed przesunięciem
float translate_z = -3.0; // odległość kamery perspektywicznej od centrum wykresu
float rotate_x = 0.0, rotate_y = 0.0, rotate_z = 0.0; // obrót widoku

// parametry OpenGL (możliwości użycia pewnych mechanizmów)
// zakładamy że są niedostępne, o ile tego nie sprawdzimy
// TODO: lepsza struktura danych, kiedy zbierze się więcej flag
int gl_has_query_timers = 0;

/*
 * profilowanie OpenGL, za pomocą wbudowanych funkcji; patrz:
 * - https://www.opengl.org/wiki/Query_Object
 * - http://www.lighthouse3d.com/tutorials/opengl-timer-query/
 * - https://github.com/progschj/OpenGL-Examples/blob/master/10queries_conditional_render.cpp
 */
// ilość buforów zapytań, aby pytanie nie powodowało martwych punktów
#define QUERY_BUFFERS 5  // za OpenGL-Examples
// liczba potrzebnych zapytań (pomiarów czasu) na jedną klatkę
#define QUERY_COUNT   1  // dla renderGL (jeden pomiar odstępu czasu)
// tablica składująca bufory zapytań (do pomiaru czasu)
GLuint queryID[QUERY_BUFFERS][QUERY_COUNT]; // bufor cykliczny
int current_query_id = 0, next_query_id = 0; // położenie w buforze
stat_float_t gl_time_stats[QUERY_COUNT] = { 0 };

// pomiar czasu obliczeń na CPU lub GPU (CUDA)
#ifdef NO_CUDA
// na CPU używamy clock_gettime()
clockid_t clk_id = CLOCK_MONOTONIC; // używamy monotonicznego globalnego czasu
struct timespec   ts_res, ts_beg, ts_end; // rozdzielczość, początek, koniec
unsigned long int res_ns; // rozdzielczość w nanosekundach (z ts_res)

#else  /* !NO_CUDA */
#define TIMERS_COUNT 1   // dla draw_GPU
// na GPU używamy cudaEventRecord() i cudaEventElapsedTime()
// podobnie jak w przypadku OpenGL, używamy buforów zdarzeń (asynchroniczność)
cudaEvent_t timer_events[QUERY_BUFFERS][2*TIMERS_COUNT]; // 2 == początek i koniec
int current_timer_id = 0, next_timer_id = 0;

#endif /* !NO_CUDA */


typedef enum {
	// rysowanie punktów siatki
	drPoints = 0,
	// rysowanie pionowych linii siatki (wierszy)
	// za pomocą glDrawArrays()
	drLines,
	// rysowanie powierzchni za pomocą wachlarzy trójkątnych
	// przy użyciu glDrawElements() i primitive restart
	drPlotSurface,

	// liczba sposobów rysowania
	drN,
} draw_mode_t;
int drawMode = drPlotSurface;

typedef enum {
	palGrayscale = 0, // odcienie szarości
	palYellowToRed,   // od żółci do czerwieni
	palSunset,        // liniowa paleta biały--czerwony
	palBWR,           // od niebieskiego, przez biały, do czerwonego
	palPlusMin,       // odchylenia okolicy środka, niebiesko-czerwony
	palShortRainbow,  // kolory tęczy, wersja krótka
	palLongRainbow,   // kolory tęczy, wersja długa
	palInterpolatedRainbow, // liniowa paleta kolorów tęczy, interpolowana
	palFloatToColor,  // paleta float_to_color z "CUDA by Example"

	// TODO:
	// - http://stackoverflow.com/q/5137831/46058
	// - http://krazydad.com/tutorials/makecolors.php
	// - http://www.sandia.gov/~kmorel/documents/ColorMaps/
	// - http://matplotlib.org/users/colormaps.html
	// - https://mycarta.wordpress.com/2012/05/29/the-rainbow-is-dead-long-live-the-rainbow-series-outline/
	//
	// patrz także:
	// - https://github.com/kbinani/glsl-colormap/

	// liczba palet kolorów
	palN,
} color_palette_t;
int colorPalette = palGrayscale;

// animacja
/*
 * Możliwe sposoby pomiaru czasu:
 *  - clock_getres(CLOCK_MONOTONIC, ...)  - sekundy i nanosekundy
 *  - gettimeofday(..., NULL)             - sekundy i mikrosekundy
 *  - clock()                             - CLOCKS_PER_SEC, czas CPU (niska rozdzielczość)
 *  - glutGet(GLUT_ELAPSED_TIME)          - milisekundy od startu
 *
 * Ten program wykorzystuje glutGet(GLUT_ELAPSED_TIME), jak oryginalny
 * program demonstracyjny 'glxgears'.
 */
double t = 0.0;              // czas animacji w sekundach, od startu
unsigned long int iter = 0;  // liczba iteracji
int pause_animation = 0;     // zatrzymanie iteracji
// pomiar FPS (Frames Per Seconds)
int fps_start = -1;               // początek okresu zliczania, milisekundy
unsigned long int fps_frames = 0; // ilość ramek od początku okresu zliczania
// opóźnienie między krokami w milisekundach, jeśli nie używamy idleFunc
int sleep_time = 0, sleep_inc = 100;

// makra zamieniające indeks piksela na współrzędną
// zakłada istnienie zmiennych `dx, `dy, `width` i `height` w otoczeniu "wywołania"
#define TO_X(ix) (dx*((ix) - width/2))
#define TO_Y(iy) (dy*((iy) - height/2))

// makro zwracające indeks elementu o współrzędnych (indeksach) 'i' i 'j'
// w "spłaszczonej" tablicy dwuwymiarowej 'Ni' x 'Nj', gdzie elementy są
// ułożone wierszami (tzn. 0-wy wiersz, 1-szy wiersz, ...)
#define IDX2D(Ni,Nj,i,j) ((j)*(Ni) + (i))
// makro zwracające adres voxela RGBA o indeksach 'ix' i 'iy'
// zakłada istnienie zmiennych `width` i `height` w otoczeniu "wywołania"
// w tablicy wierzchołków 'arr' odpowiedniego typu wektorowego (uint4, float4, ...)
#define VOXEL_PTR(arr,ix,iy)  ((arr) + IDX2D(width,height,ix,iy))
// w tablicy wierzchołków 'arr' odpowiedniego typu, każdy voxel / wierzchołek ma 4 składowe
#define VOXEL4_PTR(arr,ix,iy) ((arr) + IDX2D(width,height,ix,iy)*4)
// w tablicy wierzchołków 'arr' odpowiedniego typu, każdy voxel / wierzchołek ma 3 składowe
#define VOXEL3_PTR(arr,ix,iy) ((arr) + IDX2D(width,height,ix,iy)*3)


/* ====================================================================== */
/* Definicje funkcji */

/* ---------------------------------------------------------------------- */
/* Statystyki "w locie" */
void add_stat(stat_float_t *stats, float val)
{
	/* minimalna i maksymalna widziana wartość */
	if (stats->count == 0) {
		stats->min = stats->max = val;
	} else if (val < stats->min) {
		stats->min = val;
	} else if (stats->max < val) {
		stats->max = val;
	}

	/* średnia ruchoma */
	if (stats->count == 0) {
		stats->avg  = val;
		stats->avg2 = val*val;

		stats->count++;
		return;
	}

#if 0
	stats->avg =
		stats->avg*(stats->count/(stats->count + 1.0)) +
		val/(stats->count + 1.0);

	stats->avg2 =
		stats->avg2*(stats->count/(stats->count + 1.0)) +
		(val*val)/(stats->count + 1.0);
#endif
	// wzór z https://en.wikipedia.org/wiki/Moving_average#Cumulative_moving_average
	stats->avg  += (val     - stats->avg )/(stats->count + 1);
	stats->avg2 += (val*val - stats->avg2)/(stats->count + 1);

	// patrz także https://en.wikipedia.org/wiki/Standard_deviation#Rapid_calculation_methods
	// dla alternatywnego sposobu wyliczania odchylenia standardowego

	stats->count++;
}

float get_stat_avg(stat_float_t *stats)
{
	return stats->avg;
}

// standardowe odchylenie populacji
float get_stat_stddev(stat_float_t *stats)
{
	return sqrt(stats->avg2 - stats->avg*stats->avg);
}

// standardowe odchylenie próbki
float get_stat_stddev_sample(stat_float_t *stats)
{
	return sqrt(stats->count/(stats->count - 1.0))*get_stat_stddev(stats);
}

/* ---------------------------------------------------------------------- */
/* Tworzenie wykresu */

// funkcja dwu zmiennych do wykreślenia
// plot_GPU / plot_CPU zakłada, że wartości funkcji mieszczą się w [-2,2]
__host__ __device__
float func(float x, float y, float t)
{
	//return sinf(x*M_PI + t*M_PI) + cosf(y*M_PI + 0.5*t*M_PI);
	//return sinf(x*M_PI + t*M_PI) + (2.0*(y - floorf(y)) - 1.0);
	//return (2.0*(x - floorf(x)) - 1.0) + (2.0*(y - floorf(y)) - 1.0);
	//return 2*expf(-(x*x/4 + y*y)/3.0)*sinf(4*sqrtf(x*x/4+y*y)*M_PI - 4*t*M_PI);
	return 2*expf(-(x*x + y*y)/2.0)*sinf(2*sqrtf(x*x+y*y)*M_PI - 4*t*M_PI);
}


#ifndef NDEBUG
#define SANITY_CHECK_MINMAX(min,max)			\
do {											\
	if ((max) < (min)) {						\
		float tmp = (min);						\
		(min) = (max);							\
		(max) = tmp;							\
	}											\
	if (fabs((max) - (min)) < FLT_EPSILON)		\
		(max) = (min) + 1.0;					\
} while (0) /* no trailing ';' */
#else  /* NDEBUG */
#define SANITY_CHECK_MINMAX(min,max) ((void)0)
#endif /* NDEBUG */

/*
 * Zamiana wartości 'val' w zakresie 'min'..'max' na piksel,
 * tutaj za pomocą 8-bitowej palety odcieni szarości (Y,Y,Y).
 * Wartości poza zakresem są zaznaczane za pomocą specjalnych
 * kolorów: czerwony (R,0,0) dla val > max, niebieski (0,0,B) dla val < min.
 *
 * https://en.wikipedia.org/wiki/List_of_monochrome_and_RGB_palettes#8-bit_Grayscale
 */
__host__ __device__
void colorPointGrayscale(unsigned char rgba[4],
                         float val, float min, float max)
{
	// tzw. sanity check i ew. naprawienie min i max
	SANITY_CHECK_MINMAX(min,max);

	// obsłużenie przypadku gdy wartości są poza zakresem
	if (val < min) {
		// alternatywnym rozwiązaniem jest przycięcie: val = min;
		// niebieski (chłodny) dla wartości poniżej minimum
		rgba[cR] = 0; rgba[cG] = 0; rgba[cB] = 255; rgba[cA] = 255;
		return;
	} else if (val > max) {
		// alternatywnym rozwiązaniem jest przycięcie: val = max;
		// czerwony (ciepły) dla wartości powyżej minimum
		rgba[cR] = 255; rgba[cG] = 0; rgba[cB] = 0; rgba[cA] = 255;
		return;
	}

	// przeskalowanie wartości do zakresu 0.0..1.0
	val = (val - min)/(max - min);

	// paleta odcieni szarości (R,G,B) = (Y,Y,Y)
	// zamiana wartości zmiennoprzecinkowej 0..1.0 na 8-bit 0..255
	rgba[cR] = rgba[cG] = rgba[cB] =
		(unsigned char)floor(255.0*val);
	rgba[cA] = 255;
}

__host__ __device__
void colorPointYellowToRed(unsigned char rgba[4],
						   float val, float min, float max)
{
	// tzw. sanity check i ew. naprawienie min i max
	SANITY_CHECK_MINMAX(min,max);

	// obsłużenie przypadku gdy wartości są poza zakresem
	if (val < min) {
		// alternatywnym rozwiązaniem jest przycięcie: val = min;
		// zielony dla wartości poniżej minimum (żółty)
		rgba[cR] = 0; rgba[cG] = 255; rgba[cB] = 0; rgba[cA] = 255;
		return;
	} else if (val > max) {
		// alternatywnym rozwiązaniem jest przycięcie: val = max;
		// ciemny czerwony dla wartości powyżej minimum (czerwony)
		rgba[cR] = 128; rgba[cG] = 0; rgba[cB] = 0; rgba[cA] = 255;
		return;
	}

	// przeskalowanie wartości do zakresu 0.0..1.0
	val = (val - min)/(max - min);

	// paleta od żółtego do czerwieni (R,G,B) = (255,255-Y,0)
	// zamiana wartości zmiennoprzecinkowej 0..1.0 na 8-bit 0..255
	rgba[cR] = 255;
	rgba[cG] = (unsigned char)floor(255.0*(1 - val));
	rgba[cB] = 0;

	rgba[cA] = 255;
}

__host__ __device__
void colorPointBlueWhiteRed(unsigned char rgba[4],
                            float val, float min, float max)
{
	// tzw. sanity check i ew. naprawienie min i max
	SANITY_CHECK_MINMAX(min,max);

	// wartość pośrodku zakresu wartości
	float mid = (min + max)/2.0;

	// obsłużenie przypadku gdy wartości są poza zakresem
	if (val < min) {
		// alternatywnym rozwiązaniem jest przycięcie: val = min;
		// ciemny niebieski (chłodny) dla wartości poniżej minimum
		rgba[cR] = 0; rgba[cG] = 0; rgba[cB] = 128; rgba[cA] = 255;
		return;
	} else if (val > max) {
		// alternatywnym rozwiązaniem jest przycięcie: val = max;
		// ciemny czerwony (ciepły) dla wartości powyżej minimum
		rgba[cR] = 128; rgba[cG] = 0; rgba[cB] = 0; rgba[cA] = 255;
		return;
	}

	// przeskalowanie wartości do zakresu -1.0..1.0
	val = (val - mid)/(2.0*(max - min));

	if (val < 0) {
		// paleta odcieni dla wielkości poniżej środka (R,G,B) = (1-|Y|,1-|Y|,1.0)
		// zamiana wartości zmiennoprzecinkowej -1.0..0 na 8-bit 0..255 niebieski
		rgba[cB] = 255;
		rgba[cR] = rgba[cG] =
			255 + (char)floor(255.0*val);
	} else if (fabs(val) <= FLT_EPSILON) {
		// pośrodku palety mamy kolor biały
		rgba[cR] = rgba[cG] = rgba[cB] = 255;
	} else {
		// paleta odcieni dla wielkości poniżej środka (R,G,B) = (1.0,1-|Y|,1-|Y|)
		// zamiana wartości zmiennoprzecinkowej 0..1.0 na 8-bit 0..255 czerwony
		rgba[cR] = 255;
		rgba[cG] = rgba[cB] =
			255 - (char)floor(255.0*val);
	}
	rgba[cA] = 255;
}

// https://www.particleincell.com/2014/colormap/
__host__ __device__
void colorPointShortRainbow(unsigned char rgba[4],
                            float val, float min, float max)
{
	// tzw. sanity check i ew. naprawienie min i max
	SANITY_CHECK_MINMAX(min,max);

	// obsłużenie przypadku gdy wartości są poza zakresem
	if (val < min || val > max) {
		// alternatywnym rozwiązaniem jest przycięcie: val = val < min ? min : max;
		// prawie czarny dla wartości poza zakresem
		rgba[cR] = 1; rgba[cG] = 1; rgba[cB] = 1; rgba[cA] = 255;
		return;
	}

	// przeskalowanie wartości do zakresu 0.0..1.0
	val = (val - min)/(max - min);

	float a = (1 - val)/0.25; // odwrócenie i grupowanie (4 grupy)
	unsigned char X = floor(a); // część całkowita, czyli grupa
	// Y to część ułamkowa (w grupie), przeskalowana do 0..255
	unsigned char Y = floor(255*(a - X));
	switch (X) {
	case 0:
		rgba[cR] = 255;
		rgba[cG] = Y;
		rgba[cB] = 0;
		break;

	case 1:
		rgba[cR] = 255-Y;
		rgba[cG] = 255;
		rgba[cB] = 0;
		break;

	case 2:
		rgba[cR] = 0;
		rgba[cG] = 255;
		rgba[cB] = Y;
		break;

	case 3:
		rgba[cR] = 0;
		rgba[cG] = 255-Y;
		rgba[cB] = 255;
		break;

	case 4:
	default:
		rgba[cR] = 0;
		rgba[cG] = 0;
		rgba[cB] = 255;
		break;
	}

	rgba[cA] = 255;
}

// https://www.particleincell.com/2014/colormap/
__host__ __device__
void colorPointLongRainbow(unsigned char rgba[4],
                            float val, float min, float max)
{
	// tzw. sanity check i ew. naprawienie min i max
	SANITY_CHECK_MINMAX(min,max);

	// obsłużenie przypadku gdy wartości są poza zakresem
	if (val < min || val > max) {
		// alternatywnym rozwiązaniem jest przycięcie: val = val < min ? min : max;
		// prawie czarny dla wartości poza zakresem
		rgba[cR] = 1; rgba[cG] = 1; rgba[cB] = 1; rgba[cA] = 255;
		return;
	}

	// przeskalowanie wartości do zakresu 0.0..1.0
	val = (val - min)/(max - min);

	float a = (1 - val)/0.2; // odwrócenie i grupowanie (5 grup)
	unsigned char X = floor(a); // część całkowita, czyli grupa
	// Y to część ułamkowa (w grupie), przeskalowana do 0..255
	unsigned char Y = floor(255*(a - X));
	switch (X) {
	case 0:
		rgba[cR] = 255;
		rgba[cG] = Y;
		rgba[cB] = 0;
		break;

	case 1:
		rgba[cR] = 255-Y;
		rgba[cG] = 255;
		rgba[cB] = 0;
		break;

	case 2:
		rgba[cR] = 0;
		rgba[cG] = 255;
		rgba[cB] = Y;
		break;

	case 3:
		rgba[cR] = 0;
		rgba[cG] = 255-Y;
		rgba[cB] = 255;
		break;

	case 4:
		rgba[cR] = Y;
		rgba[cG] = 0;
		rgba[cB] = 255;
		break;

	case 5:
	default:
		rgba[cR] = 255;
		rgba[cG] = 0;
		rgba[cB] = 255;
		break;
	}

	rgba[cA] = 255;
}

// http://www.sron.nl/~pault/
/*
 * Skala kolorów przeznaczona do badania odchyleń w okolicach zera,
 * a dokładniej w okolicach środka wartości (zera dla symetrycznego
 * zakresu możliwych wartości).  Kolory od niebieskiego do czerwonego.
 *
 * Gradienty najlepszej widoczności autorstwa Paul Tol,
 * na podstawie kodu dla gnuplot i matplotlib (Python)
 * napisanego przez Tim van Werkhoven.
 */
__host__ __device__
void colorPointPlusMin(unsigned char rgba[4],
                       float val, float min, float max)
{
	// tzw. sanity check i ew. naprawienie min i max
	SANITY_CHECK_MINMAX(min,max);

	// obsłużenie przypadku gdy wartości są poza zakresem
	if (val < min || val > max) {
		// alternatywnym rozwiązaniem jest przycięcie: val = val < min ? min : max;
		// prawie czarny dla wartości poza zakresem
		rgba[cR] = 1; rgba[cG] = 1; rgba[cB] = 1; rgba[cA] = 255;
		return;
	}

	// przeskalowanie wartości do zakresu 0.0..1.0
	float x = (val - min)/(max - min);
	// potęgi przeskalowanej wartości, do interpolacji kolorów
	float x2 = x*x;
	float x3 = x*x2;
	float x4 = x2*x2;
	float x5 = x2*x3;

	float rcol = 0.237 - 2.13*x + 26.92*x2 - 65.5*x3 + 63.5*x4 - 22.36*x5;
	float gcol = (0.572 + 1.524*x - 1.811*x2)/(1 - 0.291*x + 0.1574*x2);
	gcol *= gcol; // gcol = gcol^2
	float bcol = 1.0/(1.579 - 4.03*x + 12.92*x2 - 31.4*x3 + 48.6*x4 - 23.36*x5);

	rgba[cR] = (unsigned char)floor(255.0*rcol);
	rgba[cG] = (unsigned char)floor(255.0*gcol);
	rgba[cB] = (unsigned char)floor(255.0*bcol);
	rgba[cA] = 255;
}

// http://www.sron.nl/~pault/
/*
 * Liniowa skala kolorów od białego do czerwonego,
 * odcienie zachodu Słońca.
 *
 * Gradienty najlepszej widoczności autorstwa Paul Tol,
 * na podstawie kodu dla gnuplot i matplotlib (Python)
 * napisanego przez Tim van Werkhoven.
 */
__host__ __device__
void colorPointSunset(unsigned char rgba[4],
                      float val, float min, float max)
{
	// tzw. sanity check i ew. naprawienie min i max
	SANITY_CHECK_MINMAX(min,max);

	// obsłużenie przypadku gdy wartości są poza zakresem
	if (val < min) {
		// alternatywnym rozwiązaniem jest przycięcie: val = min;
		// [prawie] czarny, jak w implementacji dla IDL
		rgba[cR] = 1; rgba[cG] = 1; rgba[cB] = 1; rgba[cA] = 255;
		return;
	} else if (val > max) {
		// alternatywnym rozwiązaniem jest przycięcie: val = max;
		// biały, jak w implementacji dla IDL
		rgba[cR] = 255; rgba[cG] = 255; rgba[cB] = 255; rgba[cA] = 255;
		return;
	}


	// przeskalowanie wartości do zakresu 0.0..1.0
	float x = (val - min)/(max - min);

	float rcol = (1 - 0.392*(1 + erff((x - 0.869)/0.255)));
	float gcol = (1.021 - 0.456*(1 + erff((x - 0.527)/0.376)));
	float bcol = (1 - 0.493*(1 + erff((x - 0.272)/0.309)));

	rgba[cR] = (unsigned char)floor(255.0*rcol);
	rgba[cG] = (unsigned char)floor(255.0*gcol);
	rgba[cB] = (unsigned char)floor(255.0*bcol);
	rgba[cA] = 255;
}

// http://www.sron.nl/~pault/
/*
 * Liniowa skala kolorów tęczy (interpolowana)
 *
 * Gradienty najlepszej widoczności autorstwa Paul Tol,
 * na podstawie kodu dla gnuplot i matplotlib (Python)
 * napisanego przez Tim van Werkhoven.
 */
__host__ __device__
void colorPointInterpolatedRainbow(unsigned char rgba[4],
                                   float val, float min, float max)
{
	// tzw. sanity check i ew. naprawienie min i max
	SANITY_CHECK_MINMAX(min,max);

	// obsłużenie przypadku gdy wartości są poza zakresem
	if (val < min) {
		// alternatywnym rozwiązaniem jest przycięcie: val = min;
		// [prawie] czarny, jak w implementacji dla IDL
		rgba[cR] = 1; rgba[cG] = 1; rgba[cB] = 1; rgba[cA] = 255;
		return;
	} else if (val > max) {
		// alternatywnym rozwiązaniem jest przycięcie: val = max;
		// biały, jak w implementacji dla IDL
		rgba[cR] = 255; rgba[cG] = 255; rgba[cB] = 255; rgba[cA] = 255;
		return;
	}


	// przeskalowanie wartości do zakresu 0.0..1.0
	float x = (val - min)/(max - min);
	// potęgi przeskalowanej wartości, do interpolacji kolorów
	float x2 = x*x;
	float x3 = x*x2;
	float x4 = x2*x2;
	float x5 = x2*x3;
	float x6 = x3*x3;

	float rcol = (0.472-0.567*x+4.05*x2)/(1.+8.72*x-19.17*x2+14.1*x3);
	float gcol = 0.108932-1.22635*x+27.284*x2-98.577*x3+163.3*x4-131.395*x5+40.634*x6;
	float bcol = 1./(1.97+3.54*x-68.5*x2+243*x3-297*x4+125*x5);

	rgba[cR] = (unsigned char)floor(255.0*rcol);
	rgba[cG] = (unsigned char)floor(255.0*gcol);
	rgba[cB] = (unsigned char)floor(255.0*bcol);
	rgba[cA] = 255;
}

/*
 * Paleta kolorów używana w przykładowych programach
 * z "CUDA by Example", w szczególności rozdziału
 * '7. Texture Memory' oraz '8. Graphics Interoperability'.
 *
 * Implementowana w common/book.h jako jądro float_to_color
 * oraz funkcja value (tutaj: hsl_helper).
 */
__host__ __device__
static unsigned char hsl_helper(float n1, float n2, int hue)
{
	if (hue > 360)      hue -= 360;
	else if (hue < 0)   hue += 360;

	if (hue < 60)
		return (unsigned char)(255 * (n1 + (n2-n1)*hue/60));
	if (hue < 180)
		return (unsigned char)(255 * n2);
	if (hue < 240)
		return (unsigned char)(255 * (n1 + (n2-n1)*(240-hue)/60));
	return (unsigned char)(255 * n1);
}

__host__ __device__
void colorPointFloatToColor(unsigned char rgba[4],
                            float val, float min, float max)
{
	// tzw. sanity check i ew. naprawienie min i max
	SANITY_CHECK_MINMAX(min,max);

	// obsłużenie przypadku gdy wartości są poza zakresem
	if (val < min) {
		val = min;
	} else if (val > max) {
		val = max;
	}


	// przeskalowanie wartości do zakresu 0.0..1.0
	float x = (val - min)/(max - min);

	// najprawdopodbniej wartości HSL
	// * H - odcień światła (ang. Hue), kąt na kole barw 0°..360°
	// * S - nasycenie koloru (ang. Saturation), promień podstawy 0..1
	// * L - średnie światło białe (ang. Lightness), 0..1 lub 0%..100%
	float l = x;
	float s = 1;
	int   h = (180 + (int)(360.0f * l)) % 360;

	float m1, m2;

	if (l <= 0.5f)
		m2 = l * (1 + s);
	else
		m2 = l + s - l * s;
	m1 = 2 * l - m2;


	rgba[cR] = hsl_helper(m1, m2, h + 120);
	rgba[cG] = hsl_helper(m1, m2, h);
	rgba[cB] = hsl_helper(m1, m2, h - 120);
	rgba[cA] = 255;
}


/*
 * Ustawianie parametrów voxeli powierzchni do wykreślenia:
 *  - współrzędnych wierzchołka (współrzędne homogeniczne {x,y,z,w=1.0}
 *  - kolorów wierzchołków (przestrzeń kolorów RGBA {red,green,blue,alpha}
 *
 *  Kolorowanie jest dokonywane za pomocą odpowiedniej palety kolorów,
 *  domyślnie `colorPointGrayscale`
 */
__host__ __device__
void plotVoxel(float4 *pos, uchar4 *colorPos, // wskaźniki do wierzchołków
               float x, float y, float val,   // współrzędne (x,y) i wartość funkcji
               float val_min, float val_max,  // ograniczenia wartości dla kolorowania
               int colorPalette)              // numer palety kolorów
{
	// zapisz wartość (położenie) wierzchołka
#ifndef NO_CUDA
	//(*pos) = make_float4(x, y, zscale*val, 1.0f);
	(*pos) = make_float4(x, y, val, 1.0f);
#else /* NO_CUDA */
	// z jakiegoś powodu make_float4 nie chce działać
	pos->x = x;
	pos->y = y;
	//pos->z = zscale*val;
	pos->z = val;
	pos->w = 1.0f;
#endif /* NO_CUDA */

	// zapisz kolor wierzchołka
	switch (colorPalette) {
	case palYellowToRed:
		colorPointYellowToRed((unsigned char *)colorPos, val, -2.0, 2.0);
		break;
	case palBWR:
		colorPointBlueWhiteRed((unsigned char *)colorPos, val, -2.0, 2.0);
		break;
	case palShortRainbow:
		colorPointShortRainbow((unsigned char *)colorPos, val, -2.0, 2.0);
		break;
	case palLongRainbow:
		colorPointLongRainbow((unsigned char *)colorPos, val, -2.0, 2.0);
		break;
	case palInterpolatedRainbow:
		colorPointInterpolatedRainbow((unsigned char *)colorPos, val, -2.0, 2.0);
		break;
	case palFloatToColor:
		colorPointFloatToColor((unsigned char *)colorPos, val, -2.0, 2.0);
		break;
	case palPlusMin:
		colorPointPlusMin((unsigned char *)colorPos, val, -2.0, 2.0);
		break;
	case palSunset:
		colorPointSunset((unsigned char *)colorPos, val, -2.0, 2.0);
		break;
	case palGrayscale:
	default:
		colorPointGrayscale((unsigned char *)colorPos, val, -2.0, 2.0);
		break;
	} /* end switch colorPalette */
}


/* ...................................................................... */
/* Tworzenie wykresu funkcji, tzn. ustalanie kształtu i koloru powierzchni */
#ifndef NO_CUDA
__global__
void plot_GPU(int width, int height, float dx, float dy,
              float t, unsigned long iter, int colorPalette,
              float4 *pos, uchar4 *colorPos)
{
	// indeks wątku w dwuwymiarowej siatce
	int ix = blockIdx.x*blockDim.x + threadIdx.x;
	int iy = blockIdx.y*blockDim.y + threadIdx.y;

	// zabezpieczenie przed pisaniem poza tablicą, ważne jeśli
	// wymiary obrazu nie są całkowitą wielokrotnością rozmiaru bloku
	// (właściwie niepotrzebne jeśli zachowujemy poniższe pętle while)
	if (ix >= width || iy >= height)
		return;

	// pętle while zapewniają całkowite objęcie tablicy (bitmapy),
	// nawet jeśli maksymalna liczba wątków dla danej architektury GPU
	// jest mniejsza od liczby pikseli w obrazie (rozmiaru tablicy)
	// (nie jest właściwie potrzebne w tym programie)
	while (ix < width) {
		while (iy < height) {
			plotVoxel(VOXEL_PTR(pos, ix, iy), VOXEL_PTR(colorPos, ix, iy),
			          TO_X(ix), TO_Y(iy), func(TO_X(ix), TO_Y(iy), t),
			          -2.0, 2.0, colorPalette);

			iy += blockDim.y*gridDim.y;
		}
		ix += blockDim.x*gridDim.x;
	}
}

#else /* NO_CUDA */
void plot_CPU(int width, int height, float dx, float dy,
              float t, unsigned long iter, int colorPalette,
              float4 *pos, uchar4 *colorPos)
{
	int ix, iy;

	for (ix = 0; ix < width; ix++) {
		for (iy = 0; iy < height; iy++) {
			plotVoxel(VOXEL_PTR(pos, ix, iy), VOXEL_PTR(colorPos, ix, iy),
			          TO_X(ix), TO_Y(iy), func(TO_X(ix), TO_Y(iy), t),
			          -2.0, 2.0, colorPalette);
		}
	}
}
#endif /* NO_CUDA */


/* ---------------------------------------------------------------------- */
/* Wyświetlanie wykresu 3D, tzn. zapisywanie do VBO (Vertex Buffer Object) */

#ifndef NO_CUDA
void draw_GPU(void)
{
	float4 *gpu_pos;
	uchar4 *gpu_col;
	size_t pos_size, col_size;


	/*
	 * Mapowanie 1-go zasobu graficznego (niezależne od środowiska)
	 * dla dostępu (zapisu) przez CUDA w strumieniu NULL (połączenie
	 * wszystkich strumieni).  Ten zasób graficzny został wcześniej
	 * 'zarejestrowany' z VBO (Vertex Buffer Object) w OpenGL.
	 */
	cudaGraphicsMapResources(1, &vertexVBO.cudaResource, NULL);
	cudaGraphicsMapResources(1, &colorVBO.cudaResource,  NULL);

	/*
	 * Pobranie (mapowanie) wskaźnika do pamięci urządzenia GPU
	 * w przestrzeni adresowej CUDA, przez który będzie odbywał się
	 * dostęp do mapowanego zasobu graficznego (bitmap RGBA 32-bit)
	 */
	cudaGraphicsResourceGetMappedPointer((void**)&gpu_pos, &pos_size, vertexVBO.cudaResource);
	cudaGraphicsResourceGetMappedPointer((void**)&gpu_col, &col_size, colorVBO.cudaResource);

	/*
	 * Ustawienie ilości bloków w siatce oraz ilości wątków w bloku
	 * tak aby ich punktowy iloczyn był większy lub równy rozmiarom obrazu:
	 *
	 *     grid »*« block >= dim3(width, height, 1)
	 *
	 * Ilość wątków w bloku powinna być odpowiednio duża by zapełnić
	 * multiprocesor i umożliwić ukrywanie opóźnień przez przeplatanie
	 * obliczeń.  Powinna być wielokrotnością rozmiaru warp czyli 32.
	 *
	 *   wątków w bloku: 16*16 = 256 (rekomendowane zaczęcie od 128-256)
	 */
	dim3 grid((mesh.width  + BLOCK_DIM - 1)/BLOCK_DIM,
	          (mesh.height + BLOCK_DIM - 1)/BLOCK_DIM); // bloków w siatce
	dim3 block(BLOCK_DIM, BLOCK_DIM);              // wątków w bloku
	/*
	 * Wykonanie jądra CUDA, zapisującego do wskaźnika urządzenia GPU
	 * mapowanego do zasobów graficznych OpenGL: Vertex Buffer Object
	 */
	plot_GPU<<<grid,block>>>(mesh.width, mesh.height, mesh.dx, mesh.dy, t, iter,
							 colorPalette, gpu_pos, gpu_col);

	/*
	 * Odmapowanie zasobów graficznych (obiektu bufora bitmapy),
	 * zaznaczając że skończyliśmy do niego zapisywać.
	 */
	cudaGraphicsUnmapResources(1, &vertexVBO.cudaResource, NULL);
	cudaGraphicsUnmapResources(1, &colorVBO.cudaResource,  NULL);
}

#else /* NO_CUDA */
void draw_CPU(void)
{
	float4 *cpu_pos;
	uchar4 *cpu_col;
	GLboolean res;

	/*
	 * Mapowanie obiektu bufora z OpenGL dla dostępu (zapisu) przez CPU
	 */
	glBindBuffer(GL_ARRAY_BUFFER, vertexVBO.vbo);
	cpu_pos =
		(float4 *)glMapBuffer(GL_ARRAY_BUFFER,
		                      GL_WRITE_ONLY);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindBuffer(GL_ARRAY_BUFFER, colorVBO.vbo);
	cpu_col =
		(uchar4 *)glMapBuffer(GL_ARRAY_BUFFER,
		                      GL_WRITE_ONLY);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	/*
	 * Wykonanie obliczeń (sekwencyjnych) na CPU, zapisując do
	 * wskaźnika pamięci CPU (gospodarza) mapowanej do zasobów graficznych OpenGL.
	 */
	plot_CPU(mesh.width, mesh.height, mesh.dx, mesh.dy, t, iter,
			 colorPalette, cpu_pos, cpu_col);

	/*
	 * Odmapowanie zasobów graficznych (obiektu bufora bitmapy),
	 * zaznaczając że skończyliśmy do niego zapisywać.
	 */
	glBindBuffer(GL_ARRAY_BUFFER, vertexVBO.vbo);
	res = glUnmapBuffer(GL_ARRAY_BUFFER);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	if (res == GL_FALSE)
		eprintf("error unmapping GL_ARRAY_BUFFER vertexVBO.vbo = %d\n", vertexVBO.vbo);

	glBindBuffer(GL_ARRAY_BUFFER, colorVBO.vbo);
	res = glUnmapBuffer(GL_ARRAY_BUFFER);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	if (res == GL_FALSE)
		eprintf("error unmapping GL_ARRAY_BUFFER colorVBO.vbo = %d\n", colorVBO.vbo);
}
#endif /* NO_CUDA */


/* ---------------------------------------------------------------------- */
/* rysowanie powierzchni wykresu */
// rysowanie punktów siatki
// TODO: glPointSize, glDisable(GL_PROGRAM_POINT_SIZE), GL_POINT_SIZE,
//       GL_POINT_SIZE_RANGE, GL_POINT_SIZE_GRANULARITY
void drawMeshPoints(void)
{
	/*
	 * Rysuj elementy z danych tablicowych (wcześniej "włączonych").
	 * Tutaj renderowanych jest mesh_width*mesh_height punktów -- tyle
	 * jest elementów w tablicy.
	 */
	glDrawArrays(GL_POINTS, 0, mesh.width * mesh.height);
}

// rysowanie linii poziomych siatki za pomocą wielu glDrawArrays
// TODO: glLineWidth(), GL_LINE_WIDTH_RANGE, GL_LINE_WIDTH_GRANULARITY
//       GL_LINE_STIPPLE, glLineStipple() -- przerywane w.g. wzorca
void drawMeshLinesY(void)
{
	/*
	 * Rysuj linie siatki tak jak są ułożone wierzchołki: wierszami.
	 * Oznacza to, że rysowanie są linie równoległe do kierunku Oy,
	 * ponieważ dane są ułożone wierszami, (x,y) obok (x+dx,y).
	 */
	for (int i = 0; i < mesh.width*mesh.height; i += mesh.width) {
		/*
		 * Rysowanie ciągłego segmentu linii, złożonego z kolejnych
		 * wierzchołków, `mesh.width` punktów od indeksu `i`.
		 * Korzysta z bieżącej aktywnej macierzy wierzchołków (tutaj
		 * VBO).
		 */
		glDrawArrays(GL_LINE_STRIP, i, mesh.width);
	} /* end for */
}

// Indeksowane rysowanie powierzchni za pomocą elementów tego samego typu,
// opisanych w strukturze typu indices_info_t (może używać "primitive restart")
// Używa tablicy indeksów, albo na GPU w buforze typu GL_ELEMENTS_ARRAY_BUFFER,
// albo na CPU w tablicy.  Rysowanie za pomocą glDrawElements().
void drawElements(indices_info_t *surf)
{
	// specjalna wartość 0 dla restart_index oznacza nie używanie go,
	// bo na przykład elementy mają stałą ilość wierzchołków, jak np.
	// GL_TRIANGLES czy GL_LINES
	if (surf->restart_index != 0) {
		/*
		 * Włącza możliwość restartu wielo-wierzchołkowego elementu
		 * (primitive) jak taśma czy wachlarz za pomocą specjalnej
		 * wartości indeksu, i specyfikuje tą wartość indeksu.
		 *
		 * I tak na przykład jeśli tablica indeksów zawiera następujące
		 * elementy: {0, 1, 2, 3, TAG, 2, 3, 4, 5, ...}, to zamiast
		 * rysować taśmy przez wszystkie elementy następuje restart
		 * i rozpoczęcie nowej taśmy po wartości TAG.  Pierwsza taśma
		 * składa się z wierzchołków o indeksach 0, 1, 2, 3, druga taśma
		 * rozpoczyna się od wierzchołków 2, 3, 4, 5.
		 */
		//surf->restart_index = restart_index;
#ifdef USE_PRIMITIVE_RESTART_NV
		// rozszerzenie NVIDII
		glPrimitiveRestartIndexNV(surf->restart_index);
		glEnableClientState(GL_PRIMITIVE_RESTART_NV);
#else
		// OpenGL od wersji 3.1
		glPrimitiveRestartIndex(surf->restart_index);
		glEnable(GL_PRIMITIVE_RESTART);
#endif
	} /* end if (surf->restart_index != 0) */

	/*
	 * Rysowanie elementów za pomocą rysowania indeksowanego.
	 *
	 * W tablicy indeksów lub w buforze VBO typu GL_ELEMENTS_ARRAY_BUFFER
	 * podajemy które po kolei wierzchołki w tablicy wierzchołków tworzą
	 * element do renderowania.  Specjalna wartość indeksu `RestartIndex`
	 * oznacza restartowanie rysowania, czyli rozpoczęcie nowego elementu.
	 *
	 * Rysowanie dokonywane jest za pomocą "wachlarzy" trójkątów.
	 * Każde sąsiednie dwa wierzchołki tworzą trójkąt z pierwszym
	 * wierzchołkiem w tablicy.
	 */
	if (surf->ibo != 0) {
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, surf->ibo);
		glDrawElements(surf->mode, surf->nelems, GL_UNSIGNED_INT,
					   NULL);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	} else {
		glDrawElements(surf->mode, surf->nelems, GL_UNSIGNED_INT,
					   surf->data + surf->offset);
	}

	// dobrze jest wyłączyć to co zostało włączone
	if (surf->restart_index != 0) {
#ifdef USE_PRIMITIVE_RESTART_NV
		glDisableClientState(GL_PRIMITIVE_RESTART_NV);
#else
		glDisable(GL_PRIMITIVE_RESTART);
#endif
	}
}


void renderGL(int drawMode)
{
	/*
	 * Zdefiniuj lokalizację (dowiązany bufor VBO) i format danych (4
	 * współrzędne "homogeniczne" {x,y,z,w}, każda typu float) tablicy
	 * współrzędnych wierzchołków używanych przy renderowaniu.
	 *
	 * Włącz możliwość użycia tablicy wierzchołków (vertex array) po
	 * stronie klienta (aplikacji) do renderowania za pomocą operacji
	 * glArrayElement, glDrawArrays, glDrawElements,
	 * glDrawRangeElements, glMultiDrawArrays, lub
	 * glMultiDrawElements.  Ponieważ używamy VBO więc dane są po
	 * stronie serwera tzn. w pamięci GPU, czyli tak naprawdę opcja
	 * dotyczy renderowania po stronie serwera (GPU).
	 */
	glBindBuffer(GL_ARRAY_BUFFER, vertexVBO.vbo);
	glVertexPointer(4, GL_FLOAT, 0, 0);
	glEnableClientState(GL_VERTEX_ARRAY);

	/*
	 * Zdefiniuj lokalizację i format danych tablicy kolorów, tutaj
	 * używane jako kolory wierzchołków.  Jako źródło tablicy używany
	 * jest bufor VBO.  Kolory są w formacie RGBA (4 składowe), z
	 * 8-bit / 1-bajt na składową koloru.
	 *
	 * Włącz możliwość użycia tablicy kolorów (color array) do
	 * renderowania za pomocą glArrayElement, glDrawArrays,
	 * glDrawElements, glDrawRangeElements glMultiDrawArrays, lub
	 * glMultiDrawElements.
	 */
	glBindBuffer(GL_ARRAY_BUFFER, colorVBO.vbo);
	glColorPointer(4, GL_UNSIGNED_BYTE, 0, 0);
	glEnableClientState(GL_COLOR_ARRAY);


	// właściwe rysowanie powierzchni
	switch (drawMode) {
	case drPoints:
		drawMeshPoints(); // punkty siatki
		break;

	case drLines:
		drawMeshLinesY(); // linie siatki
		break;

	case drPlotSurface:
	default:
		if (current_surf_mode.current_mode_ptr)
			drawElements(current_surf_mode.current_mode_ptr); // powierzchnia wykresu

		// https://en.wikibooks.org/wiki/OpenGL_Programming/Scientific_OpenGL_Tutorial_05
		/*
		 * Rysowanie trójwymiarowego wykresu jako powierzchni ma swoje
		 * zalety, ale trudniej na nim zauważyć subtelne krzywe (oraz
		 * artefakty związane ze zbyt dużym oczkiem siatki / zbyt małą
		 * ilością punktów).  W tym celu dajemy możliwość narysowania
		 * linie siatki nad powierzchnią, uzyskując najlepsze z dwu
		 * podejść.
		 */
		indices_info_t *grid_ptr = current_grid_mode.current_mode_ptr;
		// jeśli wiemy jak rysować siatkę (grid_ptr != NULL),
		// i rysowanie siatki jest włączone (display_grid == TRUE)
		if (grid_ptr && display_grid) {
			/*
			 * Linie siatki wykorzystują dokładnie te same punkty co
			 * powierzchnia.  Przy włączonym usuwaniu linii ukrytych (przez
			 * testowanie głębokości) spowodowało by, że to czy linie są widoczne
			 * czy nie zależy od błędów zaokrągleń (bez nich siatki nie byłoby
			 * widać, jako że pokrywa się z powierzchnią, i powierzchnia ją
			 * zasłania).
			 *
			 * Aby to naprawić, i wyświetlać poprawnie (w sposób widoczny) linie
			 * siatki, powinny być one rysowane bliżej do kamery niż powierzchnia,
			 * lub co równoważne powierzchnia powinna być rysowana nieco dalej.
			 * Można w tym celu skorzystać z funkcji OpenGL przeznaczonej
			 * specjalnie do tego celu (dostępna od OpenGL 2.0):
			 *
			 *   glPolygonOffset(GLfloat factor, GLfloat units)
			 *
			 * Parametr 'factor' (pierwszy parametr) określa czynnik skali używany
			 * do zmiennego przesunięcia głębokości dla każdego wielokąta, podczas
			 * gdy parametr 'units' (drugi parametr) jest używany do stworzenia
			 * przesunięcia o stałej wielkości.
			 *
			 * Ostateczne przesunięcie wynosi $factor × DZ + r × units$, gdzie
			 * $DZ$ jest miarą zmiany odległości od kamery (głębokości) relatywnej
			 * do powierzchni wielokąta na ekranie, a $r$ jest najmniejszą
			 * (zależną od implementacji) wielkością, która gwarantuje niezerowe
			 * przesunięcie dla danej implementacji.  Przesunięcie jest dodawane
			 * przed testowaniem ukrytych linii (przed testem głębokości).
			 */
			glPolygonOffset(1, 1); // przesunięcie linii siatki nad powierzchnię
			/*
			 * Wielokąty powierzchni są rysowane w trybie wypełnienia GL_FILL (a
			 * nie rysowania linii GL_LINE, lub punktów GL_POINT), zatem wybieramy
			 * przesunięcie dla wypełnionych wielokątów za pomocą włączenia opcji
			 * GL_POLYGON_OFFSET_FILL.
			 *
			 * Dzięki temu trójkąty (a nie linie - używane do rysowania siatki,
			 * lub punkty) będą rysowane z nieco większą głębią (oddaleniem od
			 * kamery). Powinno to uwidocznić linie siatki.
			 */
			glEnable(GL_POLYGON_OFFSET_FILL);

			/*
			 * Aby linie siatki były widoczne, trzeba im nadać inny kolor.
			 * Teoretycznie można by zmienić wartości bufora z kolorami (na GPU za
			 * pomocą CUDA, lub na CPU), ale prościej nadać liniom siatki
			 * jednolity kolor.  W tym celu wyłączamy tablicę kolorów, i ustalamy
			 * bieżący kolor rysowania.
			 */
			// użycie jednorodnego koloru dla linii siatki
			glDisableClientState(GL_COLOR_ARRAY);
			glColor3f(0.8f, 0.2f, 1.0f); // RGB
			/*
			 * Aby linie siatki były lepiej widoczne i ładniejsze, zwiększmy ich
			 * szerokość (domyślna szerokość to 1), oraz włączmy gładkie,
			 * tzn. antyaliasowane, rysowanie linii.
			 */
			// zwiększenie szerokości linii i antyaliasowane rysowanie
			//glLineWidth(2);
			glEnable(GL_LINE_SMOOTH);

			drawElements(grid_ptr); // linie siatki

			/*
			 * Zalecane jest aby po użyciu pewnych właściwości i zmianie
			 * parametrów przywrócić je (na ile możliwe) do stanu pierwotnego.
			 */
			glDisable(GL_LINE_SMOOTH);
			glLineWidth(1);
			glDisable(GL_POLYGON_OFFSET_FILL);
		}
		break;
	} /* end switch */


	/*
	 * Wyłącz możliwości użycia tablicy wierzchołków i tablicy kolorów
	 * jak już z nich skończyliśmy korzystać.
	 */
	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_COLOR_ARRAY); // podwójne wyłączenie to noop

	// odwiązanie (odbindowanie) wszystkich obiektów buforów
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

/* ---------------------------------------------------------------------- */
/* wyliczanie FPS */

/*
 * Napisana wzorując się na kodzie programu demonstracyjnego 'glxgears'
 * http://cgit.freedesktop.org/mesa/demos/tree/src/demos/gears.c
 * w domenie publicznej (public domain).
 *
 * Wywoływane co każdą ramkę w displayFunc(), odpowiedzialne za
 * aktualizację zmiennej fps_frames.  Funkcja ta co około 5 sekund
 * oblicza i wypisuje ilość ramek na sekundę na podstawie ilości ramek
 * fps_frames i czasu od ostatniego wypisania (lub początku programu),
 * oraz resetuje okres zliczania ramek.
 */
void calculate_and_print_fps()
{
	int t_curr_ms = glutGet(GLUT_ELAPSED_TIME);
	double dt, fps;


	// zliczamy ramki (frames)
	fps_frames++;
	// sprawdzenie poprawności i ewentualny restart
	if (fps_start < 0)
		fps_start = t_curr_ms;

	// wypisujemy FPS co 5 sekund, czyli co 5000 milisekund
	// lub co 500 ramek, które zachodzi pierwsze
	if (t_curr_ms - fps_start >= 5000 || fps_frames >= 500) {
		dt = (t_curr_ms - fps_start) / 1000.0; // w sekundach
		fps = fps_frames / dt;

		printf("%lu frames in %6.3f seconds = %6.3f FPS / %6.3f ms per frame\n",
		       fps_frames, dt, fps, 1000.0*dt / fps_frames);
		fflush(stdout);
		//eprintf(" t = %g sec, iter = %lu\n", t, iter);
		//fflush(stderr);

		char window_title_fps[256] = "";
		if (sleep_time > 0 && !window.use_idle) {
			snprintf(window_title_fps, 256, "%s: %3.1f fps sleep_time %3.1f ms ",
					 window.title, fps, sleep_time/1000.0);
		} else {
			snprintf(window_title_fps, 256, "%s: %3.1f fps ",
					 window.title, fps);
		}
		glutSetWindowTitle(window_title_fps);

		// restart - nowy okres czasu
		fps_start = t_curr_ms;
		fps_frames = 0;
	}
}

/*
 * Restartowanie licznika ramek (FPS) np. po jego zatrzymaniu
 */
void reset_fps(void)
{
	fps_frames = 0;
	fps_start = -1;
}

/*
 * Zwiększa czas animacji i liczbę iteracji.
 *
 * Obecnie czas nie zależy od prędkości wyśwtelania (ilości FPS), i
 * jest sterowany przez czas zegara (czas od początku uruchomienia
 * programu), tak jak w 'glxgears'
 */
void animation_step(void)
{
	static int t_prev_ms = -1; // czas rozpoczęcia animacji, milisekundy
	int t_curr_ms; // bieżący czas, milisekundy
	double dt; // czas od ostatniej iteracji, w sekundach


	// glutGet(GLUT_ELAPSED_TIME) zwraca liczbę milisekund od glutInit()
	// lub pierwszego wywołania glutGet(GLUT_ELAPSED_TIME);
	t_curr_ms = glutGet(GLUT_ELAPSED_TIME);
	if (t_prev_ms < 0)
		t_prev_ms = t_curr_ms;

	dt = (t_curr_ms - t_prev_ms)/1000.0;
	t_prev_ms = t_curr_ms;

	// próba ominięcia problemów z "zawijaniem" czasu
	t += dt; // czas w sekundach do animacji
	iter++;  // liczba iteracji od początku animacji
}

/* ---------------------------------------------------------------------- */
/* OpenGL */

/* ...................................................................... */
/* alokowanie i zwalnianie zasobów OpenGL i CUDA / CPU */

void createVBO(mappedBuffer_t *mbuf, const char *name)
{
	eprintf(" Creating VBO '%s'...\n", name);

	// tworzymy 1 uchwyt do obiektu bufora (którym będzie VBO)
	// VBO = vertex buffer object (grafika 3d)
	glGenBuffers(1, &(mbuf->vbo) );

	/*
	 * Wiążemy bufor typu GL_ARRAY_BUFFER, używany do asynchronicznego
	 * wysyłania (upload) do właśnie utworzonego uchwytu do obiektu
	 * bufora (do pamięci GPU, kontrolowanej przez OpenGL).
	 */
	glBindBuffer(GL_ARRAY_BUFFER, mbuf->vbo);

	/*
	 * Tworzenie (odpowiednik malloc()) i inicjalizacja (ewentualnie
	 * połączona z kopiowaniem danych z pamięci CPU, ale nie w tym
	 * przypadku) obszaru przechowywania danych obiektu bufora.
	 *
	 * Alokacja pamięci GPU (zarządzanej przez OpenGL) dla bufora
	 * GL_ARRAY_BUFFER powiązanego do odpowiedniego uchwytu
	 * obiektu bufora.  NULL oznacza brak kopiowania danych z CPU.
	 *
	 * GL_DYNAMIC_DRAW oznacza, że OpenGL powinien zakładać że dane
	 * w buforze są wielokrotnie modyfikowane i używane wiele razy
	 * (DYNAMIC), i są zapisywane (modyfikowane) przez aplikację
	 * i używane przez OpenGL jako źródło do rysowania (DRAW).
	 */
	unsigned int size = mesh.width * mesh.height * mbuf->typeSize;
	glBufferData(GL_ARRAY_BUFFER, size, 0, GL_DYNAMIC_DRAW);
	eprintf("  allocated %u data (%i x %i mesh of %u size)\n",
			size, mesh.width, mesh.height, mbuf->typeSize);

	// odwiązanie (odbindowanie) wszystkich obiektów buforów
	glBindBuffer(GL_ARRAY_BUFFER, 0);

#ifndef NO_CUDA
	/*
	 * Rejestrowanie zasobu OpenGL: obiektu bufora.  Dla CUDA ten
	 * zasób graficzny jest widoczny jako wskaźnik urządzenia GPU,
	 * a zatem może być odczytywany i zapisywany przez jądra, lub
	 * przez cudaMemcpy().  Dalsze mapowanie zasobów na wskaźnik
	 * pamięci CUDA GPU odbywa się za pomocą interfejsu niezależnego
	 * od biblioteki graficznej (OpenGL lub różne wersje Direct3D).
	 *
	 * Użycie flagi cudaGraphicsRegisterFlagsWriteDiscard specyfikuje,
	 * że CUDA nie będzie czytać z tego zasobu, a zapis odbywa się
	 * po całej przestrzeni zasobu, nie zachowując żadnych poprzednich
	 * danych.  Wpływa to na odpowiednią optymalizację dostępu.
	 *
	 * Użycie flagi cudaGraphicsMapFlagsNone zakłada, że dane mogą być
	 * zapisywane i odczytywane (domyślne).
	 */
	eprintf("  Registering VBO [%u]...\n", (unsigned int)mbuf->vbo);
	cudaGraphicsGLRegisterBuffer(&(mbuf->cudaResource), mbuf->vbo,
	                             cudaGraphicsMapFlagsWriteDiscard);
	eprintf("  VBO created and registered [OpenGL:%u/GPU:%p] (done).\n",
	        mbuf->vbo, mbuf->cudaResource);
#endif /* !NO_CUDA */
	eprintf(" created VBO [%u]\n", (unsigned int)mbuf->vbo);
}

void deleteVBO(mappedBuffer_t *mbuf)
{
	eprintf("[%s]\n", __FUNCTION__);
#ifndef NO_CUDA
	// odrejestrowanie zasobu graficznego,
	// tak że nie będzie dostępny dla CUDA
	if (mbuf->cudaResource) {
		cudaGraphicsUnregisterResource(mbuf->cudaResource);
		eprintf(" unregistered graphics resource %p\n", mbuf->cudaResource);
		mbuf->cudaResource = NULL; // zapezpieczenie przed podwójnym zwolniniem
	}
#endif /* !NO_CUDA */

	/*
	 * na wszelki wypadek, odwiąż (odbinduj) wszystkie obiekty buforów,
	 * aby można było zwolnić ich pamięć; inaczej pamięć będzie zwolniona
	 * dopiero przy odbindowaniu
	 */
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	if (mbuf->vbo != 0) {
		// usuń 1 obiekt bufora o podanym uchwycie
		glDeleteBuffers(1, &(mbuf->vbo));
		eprintf(" deleted array buffer %d (vbo)\n", mbuf->vbo);
		mbuf->vbo = (GLuint)0;
	}
}

// wspólna część: tworzenie bufora z indeksami i skopiowanie danych z CPU
void createIBO(indices_info_t *surf, const char *caller_name)
{
	// to jest wewnętrzna funkcja
	eprintf(" [%s] for [%s]\n", __FUNCTION__, caller_name);

	eprintf("  indices array: %zu = %zu indices (%zu each), %lu data size (%p)\n",
			surf->allocated, surf->nelems, sizeof(GLuint),
			(unsigned int)surf->allocated*sizeof(GLuint),
			surf->data);

	eprintf("  Creating IBO...\n");

	// tworzymy 1 uchwyt do obiektu bufora (którym będzie IBO)
	// IBO = index buffer object
	glGenBuffers(1, &(surf->ibo));

	/*
	 * Wiążemy bufor typu GL_ELEMENT_ARRAY_BUFFER (tablica indeksów),
	 * używany do asynchronicznego wysyłania (upload) do właśnie
	 * utworzonego uchwytu do obiektu bufora (do pamięci GPU,
	 * kontrolowanej przez OpenGL).
	 */
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, surf->ibo);

	/*
	 * Tworzenie (odpowiednik malloc()) i inicjalizacja połączona
	 * z kopiowaniem danych z pamięci CPU do obiektu bufora.
	 *
	 * GL_STATIC_DRAW oznacza, że OpenGL powinien zakładać że dane w
	 * buforze będa zapisywane raz i używane wiele razy (STATIC), i są
	 * zapisywane (modyfikowane) przez aplikację i używane przez
	 * OpenGL (DRAW).
	 *
	 * Tutaj dane opisują podział na trójkąty lub linie (teselację),
	 * który to nie zmienia się w czasie (jest statyczny).
	 */
	size_t type_size = sizeof(surf->data[0]);
	size_t size      = surf->nelems*type_size;
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, size, surf->data, GL_DYNAMIC_DRAW);
	eprintf("   allocated %zu data (%zi elements representing indices of %zu size)\n",
			size, surf->nelems, type_size);
	eprintf("   copied %zu data (out of %zu allocated)\n"
			"   from surf->data = %p on CPU to surf->ibo = %d buffer\n",
			size, surf->allocated*sizeof(GLuint), surf->data, surf->ibo);

	// odwiązanie (odbindowanie) wszystkich obiektów buforów
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	eprintf("  created IBO [%u]\n", (unsigned int)surf->ibo);
}

/* . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . */

void createSurfaceTriangleFanIndices(indices_info_t *surf)
{
	eprintf("[%s]\n", __FUNCTION__);
	/*
	 * Alokowanie i wypełnienie macierzy indeksów elementów do
	 * wyświetlenia: wachlarzy trójkątów.
	 *
	 * Każdy wachlarz składa się z 4 wierzchołków (2 trójkątów), plus
	 * restart elementu -- razem 5 wartości indeksów.  Wachlarze są
	 * rozpięte pomiędzy sąsiednimi wierzchołkami, tak więc jest ich
	 * jak odcinków o 1 mniej niż punktów.
	 */
	surf->mode = GL_TRIANGLE_FAN;
	surf->allocated = 5*(mesh.height-1)*(mesh.width-1);
	surf->data = (GLuint *)malloc(surf->allocated*sizeof(GLuint));
	if (surf->restart_index == 0)
		surf->restart_index = 0xffffffff;

	/*
	 *    (i-1,j-1)        (i-1,j  )
	 *              2----3
	 *              |\   |
	 *              | \  |
	 *              |  \ |
	 *              |   \|
	 *              1----0
	 *    (i  ,j-1)        (i  ,j  )
	 */
	int index = 0;
	for (int i = 1; i < mesh.height; i++) {
		for (int j = 1; j < mesh.width; j++) {
			surf->data[index++] = (i  )*mesh.width + j;
			surf->data[index++] = (i  )*mesh.width + j-1;
			surf->data[index++] = (i-1)*mesh.width + j-1;
			surf->data[index++] = (i-1)*mesh.width + j;

			surf->data[index++] = surf->restart_index;
		} /* end for j */
	} /* end for i */
	surf->nelems = index;

	createIBO(surf, __FUNCTION__);
}

void createSurfaceTriangleFanLargeIndices(indices_info_t *surf)
{
	eprintf("[%s]\n", __FUNCTION__);
	/*
	 * Alokowanie i wypełnienie macierzy indeksów elementów do
	 * wyświetlenia: dużych wachlarzy trójkątów.
	 *
	 * Każdy wachlarz obejmuje cztery oczka siatki, i składa się z 9
	 * wierzchołków (8 trójkątów), plus restart elementu (końca
	 * wachlarza trójkątów) -- razem 10 wartości indeksów.  Wachlarze
	 * są mają środki w nieparzystych punktach siatki, i obejmują 2
	 * punkty siatki (oczka siatki) w każdym kierunku, tak więc ich
	 * jest połowa rozmiarów siatki.
	 *
	 * UWAGA: w obecnej implementacji ostatnie punkty mogą być
	 * pominięte dla parzystego rozmiaru siatki (ostatni wachlarz się
	 * nie mieści); to może zostać poprawione w przyszłej wersji.
	 */
	surf->mode = GL_TRIANGLE_FAN;
	surf->allocated = 11*((mesh.height-1)/2)*((mesh.width-1)/2);
	surf->data = (GLuint *)malloc(surf->allocated*sizeof(GLuint));
	if (surf->restart_index == 0)
		surf->restart_index = 0xffffffff;

	/*
	 *    (i-1,j-1)  (i-1,j  )  (i-1,j+1)
	 *              2----3----4
	 *              |\   |	 /|
	 *              | \  |	/ |
	 *              |  \ | /  |
	 *              |   \|/	  |
	 *    (i  ,j-1) 1,9--0----5 (i  ,j+1)
	 *              |   /|\	  |
	 *              |  / | \  |
	 *              | /	 |	\ |
	 *              |/   |	 \|
	 *              8----7----6
	 *    (i+1,j-1)  (i+1,j  )  (i+1,j+1)
	 */
	int index = 0;
	for (int i = 1; i+1 < mesh.height; i += 2) {
		for (int j = 1; j+1 < mesh.width; j += 2) {
			// start = centrum
			surf->data[index++] = (i  )*mesh.width + j;
			// wierzchołki naookoło centrum
			surf->data[index++] = (i  )*mesh.width + j-1;
			surf->data[index++] = (i-1)*mesh.width + j-1;
			surf->data[index++] = (i-1)*mesh.width + j;
			surf->data[index++] = (i-1)*mesh.width + j+1;
			surf->data[index++] = (i  )*mesh.width + j+1;
			surf->data[index++] = (i+1)*mesh.width + j+1;
			surf->data[index++] = (i+1)*mesh.width + j;
			surf->data[index++] = (i+1)*mesh.width + j-1;
			surf->data[index++] = (i  )*mesh.width + j-1; // powielony pierwszy

			surf->data[index++] = surf->restart_index;
		} /* end for j */
	} /* end for i */
	surf->nelems = index;

	createIBO(surf, __FUNCTION__);
}

// oparte na https://en.wikibooks.org/wiki/OpenGL_Programming/Scientific_OpenGL_Tutorial_05
void createSurfaceTrianglesIndices(indices_info_t *surf)
{
	eprintf("[%s]\n", __FUNCTION__);
	/*
	 * Alokowanie i wypełnienie macierzy indeksów elementów do
	 * wyświetlenia: trójkąty.
	 *
	 * Każde oczko siatki podzielone jest na dwa tójkąty, każdy z nich
	 * składa się z trzech wierzchołków.  Ten sposób kafelkowania
	 * (tesselacji) powierzchni (siatki) nie używa "primitive
	 * restart", ponieważ każdy element (ang. primitive) składa się ze
	 * stałej ilości wierzchołków - każdy trójkąt to trzy wierzchołki.
	 */
	surf->mode = GL_TRIANGLES;
	surf->allocated = 6*(mesh.height-1)*(mesh.width-1);
	surf->data = (GLuint *)malloc(surf->allocated*sizeof(GLuint));
	surf->restart_index = 0;

	/*
	 *  (x  ,y  )        (x+1,y  )    (x  ,y  )        (x+1,y  )
	 *            0----1                        4
	 *             \   |   	   	   	   	   	   	|\
	 *              \  |						| \
	 *               \ |						|  \
	 *                \|						|	\
	 *                 2                        6----5
	 *  (x  ,y+1)        (x+1,y+1)    (x  ,y+1)        (x+1,y+1)
	 */
	int i = 0;
	for (int y = 0; y < mesh.height-1; y++) {
		for (int x = 0; x < mesh.width-1; x++) {
			surf->data[i++] =  y      * mesh.width + x;
			surf->data[i++] =  y      * mesh.width + x + 1;
			surf->data[i++] = (y + 1) * mesh.width + x + 1;

			surf->data[i++] =  y      * mesh.width + x;
			surf->data[i++] = (y + 1) * mesh.width + x + 1;
			surf->data[i++] = (y + 1) * mesh.width + x;
		} /* end for x */
	} /* end for y */
	surf->nelems = i;

	createIBO(surf, __FUNCTION__);
}

void createSurfaceTriangleStrip(indices_info_t *surf)
{
	eprintf("[%s]\n", __FUNCTION__);
	/*
	 * Alokowanie i wypełnienie macierzy indeksów elementów do
	 * wyświetlenia: taśma trójkątna.
	 *
	 *
	 */
	surf->mode = GL_TRIANGLE_STRIP;
	surf->allocated = 2*(mesh.height-1)*(mesh.width) + 1*(mesh.height-1);
	surf->data = (GLuint *)malloc(surf->allocated*sizeof(GLuint));
	if (surf->restart_index == 0)
		surf->restart_index = 0xffffffff;

	/*
	 * Jedna z możliwości utworzenia jednego paska taśmy trójkątnej,
	 * używana w tej funkcji:
	 *
	 *    (i-1,j  )  (i-1,j+1)  (i-1,j+2)              (i-1,j+N-1)
	 *              1----3----5-..           ..-B----D
	 *              |\   |\   |\                |\   |
	 *              | \  | \  | .            .  | \  |
	 *              |  \ |  \ |  .            . |  \ |
	 *              |   \|   \|                \|   \|
	 *              0----2----4-..           ..-A----C
	 *    (i  ,j  )  (i  ,j+1)  (i  ,j+2)              (i  ,j+N-1)
	 */
	int index = 0;
	for (int i = 1; i < mesh.height; i++) {
		for (int j = 0; j < mesh.width; j++) {
			surf->data[index++] = (i  )*mesh.width + j;
			surf->data[index++] = (i-1)*mesh.width + j;
		} /* end for j */
		surf->data[index++] = surf->restart_index;
	} /* end for i */
	surf->nelems = index;

	createIBO(surf, __FUNCTION__);
}


void createGridLineStrip(indices_info_t *grid)
{
	eprintf("[%s]\n", __FUNCTION__);
	/*
	 * Alokowanie i wypełnienie macierzy indeksów elementów do
	 * wyświetlenia: siatka renderowana za pomocą taśmy linii
	 * (łańcucha), tzn. GL_LINE_STRIP.
	 */
	grid->mode = GL_LINE_STRIP;
	grid->allocated = (mesh.height+1)*(mesh.width+0)
	                + (mesh.height+0)*(mesh.width+1);
	grid->data = (GLuint *)malloc(grid->allocated*sizeof(GLuint));
	if (grid->restart_index == 0)
		grid->restart_index = 0xffffffff;

	/*
	 *  (x  ,y  )  (x+1,y  )  (x+2,y  )        (x+N-1,y  )
	 *      0----------1----------2---- [...] ------N-1
	 *
	 *
	 *  (x  ,y+1)  (x+1,y+1)  (x+2,y+1)        (x+N-1,y+1)
	 *      0----------1----------2---- [...] ------N-1
	 *
	 * [...]
	 *
	 *   (x  ,y  ) 0    (x+1,y  ) 0     [...]  (x+N-1,y  ) 0
	 *             |              |                        |
	 *             |              |                        |
	 *             |              |                        |
	 *   (x  ,y+1) 1    (x+1,y+1) 1     [...]  (x+N-1,y+1) 1
	 *             |              |                        |
	 *             .              .                        .
	 *             :              :                        :
	 *             |              |                        |
	 *  (x  ,y+N-1)N-1 (x+1,y+N-1)N-1   [...]  (x+N-1,y+N) N-1
	 *
	 */
	int index = 0;
	// poziome linie, wzdłuż osi Ox
	for (int y = 0; y < mesh.height; y++) {
		for (int x = 0; x < mesh.width; x++) {
			grid->data[index++] =  y*mesh.width + x;
		} /* end for x */
		// koniec łańcucha
		grid->data[index++] = grid->restart_index;
	} /* end for y */
	// pionowe linie, wzdłuż osi Oy
	for (int x = 0; x < mesh.width; x++) {
		for (int y = 0; y < mesh.height; y++) {
			grid->data[index++] =  y*mesh.width + x;
		} /* end for y */
		// koniec łańcucha
		grid->data[index++] = grid->restart_index;
	} /* end for x */
	grid->nelems = index;

	createIBO(grid, __FUNCTION__);
}

// https://en.wikibooks.org/wiki/OpenGL_Programming/Scientific_OpenGL_Tutorial_04
// https://en.wikibooks.org/wiki/OpenGL_Programming/Scientific_OpenGL_Tutorial_05
// https://gitlab.com/wikibooks-opengl/modern-tutorials/blob/master/graph05/graph.cpp
void createGridLines(indices_info_t *grid)
{
	eprintf("[%s]\n", __FUNCTION__);
	/*
	 * Alokowanie i wypełnienie macierzy indeksów elementów do
	 * wyświetlenia: siatka renderowana za pomocą zestawu linii,
	 * tzn. GL_LINES.
	 */
	grid->mode = GL_LINES;
	grid->allocated = 2*(mesh.height-1)*(mesh.width+0)
	                + 2*(mesh.height+0)*(mesh.width-1);
	grid->data = (GLuint *)malloc(grid->allocated*sizeof(GLuint));
	grid->restart_index = 0;

	/*
	 *  (x  ,y  )  (x+1,y  )  (x+2,y  )   ...
	 *      0----------1          4-------...
	 *                                    ...
	 *                 2----------3       ...
	 *
	 * i podobnie dla linii pionowych
	 *
	 */
	int i = 0;
	// poziome linie, wzdłuż osi Ox
	for (int y = 0; y < mesh.height; y++) {
		for (int x = 0; x < mesh.width-1; x++) {
			grid->data[i++] = y * mesh.width + x;
			grid->data[i++] = y * mesh.width + x + 1;
		}
	}
	// pionowe linie, wzdłuż osi Oy
	for (int x = 0; x < mesh.width; x++) {
		for (int y = 0; y < mesh.height-1; y++) {
			grid->data[i++] =  y      * mesh.width + x;
			grid->data[i++] = (y + 1) * mesh.width + x;
		}
	}
	grid->nelems = i;

	createIBO(grid, __FUNCTION__);
}

/* . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . */

void deleteIndices(indices_info_t *surf)
{
	eprintf("[%s]\n", __FUNCTION__);
	// jeśli dane zostały zaalokowane, i nie zostały zwolnione, usuń je
	if (surf->data) {
		free(surf->data);
		surf->data = NULL; // zabezpiecza przed podwójnym zwolnieniem

		eprintf(" freed %zd elements of surface data\n", surf->nelems);
		surf->allocated = surf->nelems = 0; // puste
	}

	if (surf->ibo != 0) {
		/*
		 * na wszelki wypadek, odwiąż (odbinduj) wszystkie obiekty buforów,
		 * aby można było zwolnić ich pamięć; inaczej pamięć będzie zwolniona
		 * dopiero przy odbindowaniu
		 */
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		// usuń 1 obiekt bufora o podanym uchwycie
		glDeleteBuffers(1, &(surf->ibo));

		eprintf(" deleted element array buffer %d (ibo)\n", surf->ibo);
		surf->ibo = (GLuint)0; // zabezpiecza przed użyciem usuniętego bufora
	}
}

// alokowanie buforów OpenGL i rejestrowanie ich dla CUDA,
// oraz alokowanie i wypełnienie buforów indeksów (powierzchnia i siatka)
void initGLBuffers(void)
{
	eprintf("[%s]\n", __FUNCTION__);
	createVBO(&vertexVBO, "vertexVBO");
	createVBO(&colorVBO,  "colorVBO");

	for (int surf_idx = 0; surf_idx < ARRAY_SIZE(surf_draw_modes); surf_idx++) {
		eprintf(" initializing surface [%d] %s\n",
				surf_idx, surf_draw_modes[surf_idx].name);
		(*surf_draw_modes[surf_idx].createIndices)(&(surf_draw_modes[surf_idx].draw_mode));
	}
	current_surf_mode.current_mode_index = 0;
	current_surf_mode.current_mode_ptr =
		&(surf_draw_modes[ current_surf_mode.current_mode_index ].draw_mode);
	eprintf(" default surface is %d (%p)\n",
			current_surf_mode.current_mode_index,
			current_surf_mode.current_mode_ptr);

	// TODO: utworzyć nową funkcję by wyeliminować duplikację kodu
	for (int grid_idx = 0; grid_idx < ARRAY_SIZE(grid_draw_modes); grid_idx++) {
		eprintf(" initializing surface grid [%d] %s\n",
				grid_idx, grid_draw_modes[grid_idx].name);
		(*grid_draw_modes[grid_idx].createIndices)(&(grid_draw_modes[grid_idx].draw_mode));
	}
	current_grid_mode.current_mode_index = 0;
	current_grid_mode.current_mode_ptr =
		&(grid_draw_modes[ current_grid_mode.current_mode_index ].draw_mode);
	eprintf(" default surface grid is %d (%p)\n",
			current_grid_mode.current_mode_index,
			current_grid_mode.current_mode_ptr);
}

// zwalnianie zasobów OpenGL i CUDA / CPU
void freeGLBuffers(void)
{
	eprintf("[%s]\n", __FUNCTION__);

	for (int surf_idx = 0; surf_idx < ARRAY_SIZE(surf_draw_modes); surf_idx++) {
		eprintf(" freeing surface [%d] %s\n",
				surf_idx, surf_draw_modes[surf_idx].name);
		deleteIndices(&(surf_draw_modes[surf_idx].draw_mode));
	}
	current_surf_mode.current_mode_index = -1;
	current_surf_mode.current_mode_ptr = NULL;

	// TODO: utworzyć nową funkcję by wyeliminować duplikację kodu
	for (int grid_idx = 0; grid_idx < ARRAY_SIZE(grid_draw_modes); grid_idx++) {
		eprintf(" freeing surface grid [%d] %s\n",
				grid_idx, grid_draw_modes[grid_idx].name);
		deleteIndices(&(grid_draw_modes[grid_idx].draw_mode));
	}
	current_grid_mode.current_mode_index = -1;
	current_grid_mode.current_mode_ptr = NULL;

	deleteVBO(&vertexVBO);
	deleteVBO(&colorVBO);
}

/* .......................................................................... */
/* pomocnicze funkcje do analizy wydajności (profilowania) OpenGL, CUDA i CPU */
// TODO: być może przenieść zmienne tutaj, aby dane i operacje były razem

void initQueries(void)
{
	eprintf("[%s]\n", __FUNCTION__);

	/*
	 * Tworzy obiekty zapytań do pomiaru czasu QUERY_COUNT na klatkę,
	 * używając QUERY_BUFFERS buforów / zapytań, by nie było martwego
	 * czasu czekania na pomiar czasu - polecenia na GPU są wykonywane
	 * asynchronicznie.
	 */
	glGenQueries(QUERY_COUNT*QUERY_BUFFERS, queryID[0]);
	current_query_id = 0;
	eprintf(" created %d x %d = %d queries\n",
			QUERY_COUNT, QUERY_BUFFERS, QUERY_COUNT*QUERY_BUFFERS);
}

void freeQueries(void)
{
	eprintf("[%s]\n", __FUNCTION__);

	glDeleteQueries(QUERY_COUNT*QUERY_BUFFERS, queryID[0]);
	//current_query_id = 0;
}


void initTimers(void)
{
	eprintf("[%s]\n", __FUNCTION__);

#ifdef NO_CUDA
	clk_id = CLOCK_MONOTONIC; // może być nie wspierany
	if (clock_getres(clk_id, &ts_res) != 0) {
		int saved_errno = errno;
		eprintf(" error getting resolution for CLOCK_MONOTONIC\n");
		eprintf(" %d: %s\n", saved_errno, strerror(saved_errno));

		clk_id = CLOCK_REALTIME; // każda implementacja go wspiera
		clock_getres(clk_id, &ts_res);
	}
	res_ns = ts_res.tv_nsec + ts_res.tv_sec*1e9;
	eprintf(" clock resolution is %g ms = %ld ns\n", res_ns*1e-6, res_ns);
#else  /* !NO_CUDA */
	for (int i = 0; i < QUERY_BUFFERS; i++) {
		for (int j = 0; j < TIMERS_COUNT; j++) {
			cudaEventCreate(&timer_events[i][j+0]);
			cudaEventCreate(&timer_events[i][j+1]);
			eprintf(" [%d][%d]: created events: %p and %p\n",
					i, j, timer_events[i][0], timer_events[i][1]);
		}
	}
#endif /* !NO_CUDA */
}

void freeTimers(void)
{
	eprintf("[%s]\n", __FUNCTION__);

#ifndef NO_CUDA
	for (int i = 0; i < QUERY_BUFFERS; i++) {
		eprintf(" [%d]: deleting timer events: %p and %p\n",
				i, timer_events[i][0], timer_events[i][1]);

		cudaEventDestroy(timer_events[i][0]);
		cudaEventDestroy(timer_events[i][1]);
	}
#endif /* !NO_CUDA */
}

void printEstimatedTimeOpenGL(void)
{
	if (!gl_has_query_timers)
		return;

	// wyświetl pomiar czasu z QUERY_BUFFERS klatek przed obecną;
	// ponieważ bufor jest cykliczny, jest to jednocześnie następna klatka
	GLint query = queryID[next_query_id][0];
	int surf_type = current_surf_mode.current_mode_index;
	int grid_type = current_grid_mode.current_mode_index;

	if (display_grid) {
		printf("> renderGL (%d x %d): drawMode = %d; surf_type = %d; grid_type = %d;\n",
			   mesh.width, mesh.height, drawMode, surf_type, grid_type);
	} else {
		printf("> renderGL (%d x %d): drawMode = %d; surf_type = %d;\n",
			   mesh.width, mesh.height, drawMode, surf_type);
	}
	if (glIsQuery(query) == GL_TRUE) { // zainicjalizowane
		GLint    done = 0;
		GLuint64 result; // odstęp czasu w nanosekundach
		// na wszelki wypadek zaczekamy na dostępność zapytania
		// (które ze względu na to że pochodzi z paru klatek wstecz
		// powinno i tak być dostępne) używając "spinlock"
		while (!done) {
			glGetQueryObjectiv(query, GL_QUERY_RESULT_AVAILABLE,
							   &done);
			if (!done) {
				printf(".");
				fflush(stdout);
			}
		}

		glGetQueryObjectui64v(query, GL_QUERY_RESULT, &result);
		printf("> renderGL done in %g ms/frame, using query %d/%d\n",
			   result*1.e-6, next_query_id, QUERY_BUFFERS);
	} /* if (glIsQuery(query) == GL_TRUE) */
	else {
		printf("> renderGL - waiting for timer: %d-%d/%d queries\n",
			   current_query_id, next_query_id, QUERY_BUFFERS);
	} /* if (glIsQuery(query) != GL_TRUE) */
}

#ifdef NO_CUDA
void printEstimatedTimeCPU(void)
{
	unsigned long int diff_ns;

	diff_ns = ts_end.tv_nsec - ts_beg.tv_nsec
		+ 1e9*(ts_end.tv_sec - ts_beg.tv_sec);

	printf("> draw_CPU done in %g ± %g ms/frame (clock_gettime)\n",
		   diff_ns*1e-6, res_ns*1e-6);

}
#else  /* !NO_CUDA */
void printEstimatedTimeGPU(void)
{
	/*
	 * Przy odpytywaniu zdarzeń CUDA o pomiar czasu między nimi mogą zajść
	 * następujące przypadki:
	 *
	 * 1. Zdarzenia nie zostały jeszcze zarejestrowane, tzn. nie zostały
	 *    wstawione do strumienia poleceń dla GPU; to może zajść dla kilku
	 *    pierwszych (dokładniej QUERY_BUFFER) "rozgrzewających" klatek,
	 *    kiedy bufor cykliczny nie jest zapełniony zaszłymi zdarzeniami.
	 *
	 *    cudaEventElapsedTime() zwraca wtedy cudaErrorInvalidResourceHandle
	 *
	 * 2. Zdarzenia zostały zarejestrowane (zakolejkowane do wykonania), ale
	 *    jeszcze nie zaszły. To nie powinno się często zdarzać, gdyż
	 *    specjalnie używany jest bufor cykliczny, i mierzymy czas z
	 *    QUERY_BUFFER klatek wstecz. W tym przypadku wymuszamy
	 *    synchronizację, tzn. czekamy na zakończenie obliczeń / końcowe
	 *    zdarzenie.
	 *
	 *    cudaEventElapsedTime() zwraca wtedy cudaErrorNotReady
	 *
	 * 3. Możemy zmierzyć czas między zdarzeniami: zdarzenia zostały
	 *    zarejestrowane (wstawione do kolejki) i zaszły.
	 *
	 *    cudaEventElapsedTime() zwraca wtedy cudaSuccess
	 *
	 */
	float diff_ms = 0.0; // czas (odstęp czasu) w milisekundach
	cudaEvent_t          // między tymi zdarzeniami
		beg = timer_events[next_timer_id][0],
		end = timer_events[next_timer_id][1];

	cudaError_t res = cudaEventElapsedTime(&diff_ms, beg, end);

	if (res == cudaErrorInvalidResourceHandle)
		return;

	if (res == cudaErrorNotReady) {
		cudaEventSynchronize(end);

		res = cudaEventElapsedTime(&diff_ms, beg, end);
	} /* if ( zdarzenie nie zaszło ) */

	// TODO: sprawdzić res, czy nie zaszły nieoczekiwane błędy
	printf("> draw_GPU done in %g ms/frame, using buffer %d/%d\n",
		   diff_ms, next_timer_id, QUERY_BUFFERS);
}
#endif /* !NO_CUDA */

void printTiming(void)
{
	printEstimatedTimeOpenGL();
#ifdef NO_CUDA
	printEstimatedTimeCPU();
#else
	printEstimatedTimeGPU();
#endif
}

/* ...................................................................... */
/* pomocnicze funkcje grafiku OpenGL */

// wyczyszczenie okna
void clear(void)
{
	// ustawienie koloru czyszczenia bufora
	// RGBA, każda składowa w zakresie od 0.0 do 1.0
	glClearColor(0.0, 0.0, 0.0, 1.0); // czarny
	// wyczyszczenie bufora zapisu kolorów (wyświetlania)
	// za pomocą wcześniej ustawionego koloru
	glClear(GL_COLOR_BUFFER_BIT);
	// wymuszenie opróżnienia buforów / wykonania poleceń;
	// zapewnia że operacje zostaną wykonane w skończonym
	// czasie, tzn. zakolejkowane w OpenGL, ale nie blokuje
	// (być może niepotrzebne przy podwójnym buforowaniu)
	glFlush();
}

// deklaracja zapowiadawcza (forward declaration)
void setPerspectiveProjection(int width, int height);

/* ---------------------------------------------------------------------- */
/* Funkcje wywołań zwrotnych (callback) obsługi zdarzeń w GLUT */

// callback bezczynności bieżącego okna w GLUT
/*
 * Celem tego callback w GLUT jest obsługa wykonywania zadań
 * przetwarzania w tle, lub ciągła animacja.  Callback ten jest
 * wywoływany gdy nie są otrzymywane żadne inne zdarzenia.
 *
 * Zalecane jest by ilość obliczeń w tym wywołaniu zwrotnym (callback)
 * nie była zbyt dużą, by nie wpływać negatywnie na responsywność
 * i interaktywność programu.
 *
 * Dlatego też ta funkcja, podążając za przykładem 'glxgears',
 * a w przeciwieństwie do 'gpu_anim.h' z programów w materiałach
 * pomocniczych "CUDA by Example", dokonuje wyłącznie wyliczania czasu
 * i wywołuje funkcję wyświetlania za pomocą glutPostRedisplay()
 * (a właściwie kolejkuje wyświetlanie w pętli zdarzeń programu).
 */
void idleFunc(void)
{
	if (pause_animation) {
		glutPostRedisplay();
		return;
	}

	// odpowiednie zwiekszenie t (bieżączego czasu) oraz iter (liczby iteracji)
	animation_step();

	// wywołuje displayFunc
	glutPostRedisplay();
}


// callback wyświetlania bieżącego okna w GLUT
void displayFunc(void)
{
	// wyczyszczenie okna (zapis do bufora kolorów)
	glClearColor(0.0, 0.0, 0.0, 1.0); // czarny
	 // czyścimy także bufor głębi
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// ustalenie macierzy widoku
	// pozwala obracać widokiem i go przesuwać
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glScalef(scale_x, scale_y, scale_z);
	// TODO: bez przesunięcia przy rzutowaniu ortograficznym;
	//       to przesunięcie to jest oddalenie kamery od wykresu
	glTranslatef(0.0f, 0.0f, translate_z);
	// TODO: obracanie za pomocą klawiatury, wokół 3 osi
	glRotatef(rotate_x, 1.0f, 0.0f, 0.0f);
	glRotatef(rotate_y, 0.0f, 1.0f, 0.0f);
	//glRotatef(rotate_z, 0.0f, 0.0f, 1.0f); // nieużywane

#ifndef NO_CUDA
	// pomiar czasu, asynchroniczny względem CPU - stąd bufor cykliczny
	current_timer_id = next_timer_id;
	cudaEventRecord(timer_events[current_timer_id][0]); // początek
	// użyj CUDA do rysowania na mapowanych zasobach OpenGL
	draw_GPU();
	cudaEventRecord(timer_events[current_timer_id][1]); // koniec
	next_timer_id = (current_timer_id + 1) % QUERY_BUFFERS;
#else  /* NO_CUDA */
	clock_gettime(clk_id, &ts_beg);
	// użyj CPU do rysowania w pamięci CPU, mapowanej do zasobów OpenGL
	draw_CPU();
	clock_gettime(clk_id, &ts_end);
#endif /* NO_CUDA */


	// start timer query
	if (gl_has_query_timers) {
		current_query_id = next_query_id;
		glBeginQuery(GL_TIME_ELAPSED,
					 queryID[current_query_id][0]);
	}
	// rysowanie za pomocą OpenGL
	renderGL(drawMode);
	// end timer query
    if (gl_has_query_timers) {
		glEndQuery(GL_TIME_ELAPSED);
		next_query_id = (current_query_id + 1) % QUERY_BUFFERS;
	}

	/*
	 * Wykonuje zamianę (swap) dla warstwy używanej przez bieżące
	 * okno.  Bufor tylnej warstwy (back buffer), na którym były
	 * wykonywane operacje graficzne jak drukowanie bitmapy, zostaje
	 * promowany do bufora przedniej warstwy (front buffer) i
	 * wyświetlony.  Zawartość bufora tylnej warstwy staje się
	 * niezdefiniowana.  Uaktualnienie odbywa się zazwyczaj podczas
	 * odświeżania zawartości monitora (retrace).
	 *
	 * Następne operacje OpenGL są kolejkowane, ale nie są wykonywane
	 * póki operacja zamiany buforów się nie zakończy; niejawne glFlush().
	 *
	 * Jeśli bieżąca warstwa (layer) nie jest podwójnie buforowana,
	 * funkcja ta nie daje żadnego efektu.
	 */
	glutSwapBuffers();
	if (!window.use_idle) {
		// wymuszamy natychmiast ponowne wyświetlanie
		glutPostRedisplay();

		// zwolnienie renderowania jeśli GPU byłoby zbyt szybkie
		if (sleep_time)
			usleep(sleep_time);
		// awansowanie animacji
		animation_step();
	}
	// jeśli use_idle jest zdefiniowane, to nie ma potrzeby ponownego wykonania
	// displayFunc() za pomocą glutPostRedisplay(), bo robimy to w idleFunc()

	// zliczanie FPS, i ewentualne drukowanie
	if (!pause_animation) {
		calculate_and_print_fps();
	} /* if (pause_animation) */
}


// wyświetlanie informacji pomocniczych o klawiszach itp.
void print_help(void)
{
	printf("Obsługiwane klawisze:\n");
	printf(" ?  \t pomoc dotycząca obsługiwanych klawiszy itp.\n");
	printf(" q, ESC\t wyjście z programu (quit)\n");
	printf(" d, D\t przełączanie trybu rysowania (draw)\n");
	printf(" p, P\t przełączanie trybu rysowania powierzchni (plot)\n");
	printf(" g, G\t przełączanie trybu rysowania siatki (grid)\n");
	printf(" c, C\t przełączanie palety kolorów (colormap)\n");
	printf(" h   \t przełączenie testu głębii, tzn. ukrywania zasłanianych linii\n");
	printf("    H\t przełączenie usuwania trójkątów spodu powierzchni\n");
	printf(" SPC\t zatrzymanie/wznowienie animacji\n");
	printf("\n");
	printf("Obsługiwanie myszy:\n");
	printf(" przesuwanie z lewym przyciskiem myszy  - obracanie wykresu\n");
	printf(" przesuwanie z prawym przyciskiem myszy - oddalanie/przybliżanie kamery\n");
#ifndef NDEBUG
	printf("\n");
	printf("Program skompilowany z wyjściem debugowania\n");
#endif /* !NDEBUG */
}

// callback klawiatury dla bieżącego okna w GLUT
void keyboardFunc(unsigned char key, int x, int y)
{
	// stan klawiszy modyfikujących: Shift, Ctrl, Alt
	int modifier = glutGetModifiers();


	eprintf("- received key '%1$c' (%1$d/0x%1$02x) "
	        "with modifier %2$d at (%3$d,%4$d)\n",
	        key, modifier, x, y);
	switch (key) {
	// przełączenie ukrywania linii
	case 'h':
		if (glIsEnabled(GL_DEPTH_TEST)) {
			glDisable(GL_DEPTH_TEST);
			eprintf(" disabled depth test\n");
		} else {
			glEnable(GL_DEPTH_TEST);
			eprintf(" enabled depth test\n");
		}
		break;

	// przełączanie usuwania spodu / wierzchu powierzchni (na podstawie skręcenia trójkątów)
	case 'H':
		if (glIsEnabled(GL_CULL_FACE)) {
			glDisable(GL_CULL_FACE);
			eprintf(" disabled cull face\n");
		} else {
			glEnable(GL_CULL_FACE);
			eprintf(" enabled cull face\n");
		}
		break;

	// przełączenie trybu rysowania
	case 'd':
	case 'D':
		// drN jest ilością różnych typów rysowania,
		// numerowanych od zera
		drawMode = ((drawMode + 1) % drN);
		eprintf(" switched to draw mode [%d]\n", drawMode);
		if (drawMode == drPlotSurface) {
			glEnable(GL_DEPTH_TEST);
			eprintf(" - enabled depth test for surface plot\n");
		} else {
			glDisable(GL_DEPTH_TEST);
			eprintf(" - disabled depth test for points or lines plot\n");
		}
		break;

	// przełączenie sposobu rysowania powierzchni
	case 'p':
	case 'P':
		current_surf_mode.current_mode_index =
			(current_surf_mode.current_mode_index+1) % current_surf_mode.number_of_modes;
		current_surf_mode.current_mode_ptr =
			&(surf_draw_modes[current_surf_mode.current_mode_index].draw_mode);
		eprintf(" drawing surface using %s\n",
				surf_draw_modes[current_surf_mode.current_mode_index].name);

		if (current_surf_mode.current_mode_index == 0) {
			display_grid = !display_grid;
		}
		if (display_grid) {
			eprintf(" and drawing grid on top of surface\n");
		}
		break;

	// przełączenie sposobu rysowania siatki
	case 'g':
	case 'G':
		current_grid_mode.current_mode_index =
			(current_grid_mode.current_mode_index+1) % current_grid_mode.number_of_modes;
		current_grid_mode.current_mode_ptr =
			&(grid_draw_modes[current_grid_mode.current_mode_index].draw_mode);
		eprintf(" %s surface grid using [%d/%d] %s\n",
				display_grid ? "drawing" : "will draw",
				current_grid_mode.current_mode_index,
				current_grid_mode.number_of_modes,
				grid_draw_modes[current_grid_mode.current_mode_index].name);
		break;

	// przełączanie palety kolorów
	case 'c':
	case 'C':
		// palN jest ilością różnych palet kolorów,
		colorPalette = ((colorPalette + (key == 'C' ? -1 : 1)) % palN);
#ifndef NDEBUG
		eprintf(" switched to color palette [%d]: ", colorPalette);
		switch (colorPalette) {
		case palYellowToRed:
			eprintf("yellow to red");
			break;
		case palBWR:
			eprintf("BWR - blue/white/red");
			break;
		case palShortRainbow:
			eprintf("short rainbow (4 groups)");
			break;
		case palLongRainbow:
			eprintf("long rainbow (5 groups)");
			break;
		case palInterpolatedRainbow:
			eprintf("linear interpolated rainbow");
			break;
		case palFloatToColor:
			eprintf("float_to_color from \"CUDA by Example\" - hue in HSL");
			break;
		case palPlusMin:
			eprintf("divergences around zero blue-red (plusmin)");
			break;
		case palSunset:
			eprintf("sunset");
			break;
		case palGrayscale:
			eprintf("grayscale");
			break;
		default:
			eprintf("???");
			break;
		} /* end switch (colorPalette) */
		eprintf("\n");
#endif /* !NDEBUG */
		break;

	case ' ':
		pause_animation = !pause_animation;
		eprintf("animation %s\n", pause_animation ? "stopped" : "running");
		// drukujemy FPS, bo podczas pauzy nie będziemy tego robić
		if (pause_animation) {
			reset_fps(); // pauzujemy FPS, więc zatrzymujemy ich wylicznie
			printTiming();
			eprintf(" t = %g sec, iter = %lu [paused]\n", t, iter);
		}
		break;


	case 'i':
	case 'I':
		window.use_idle = !window.use_idle;
		eprintf(" %susing idleFunc\n", window.use_idle ? "" : "not ");
		glutIdleFunc(window.use_idle ? idleFunc : NULL); // animacja w tle
		break;

	case '>': // zwolnienie symulacji (tylko jeśli !window.use_idle)
	case '.':
		sleep_time += sleep_inc;
		eprintf(" sleep time = %d\n", sleep_time);
		break;

	case '<': // przyspieszenie symulacji (tylko jeśli !window.use_idle)
	case ',':
		sleep_time = (sleep_time > 0) ? sleep_time - sleep_inc : 0;
		eprintf(" sleep time = %d\n", sleep_time);
		break;


	case '/':
	case '?':
		print_help();
		break;

	// wyjście z programu
	case 27: /* ESC */
	case 'q':
	case 'Q':
		printf("Shutting down...\n");
		eprintf(" transformations:\n"
				"  rotate_x = % 6.2f,\n"
				"  rotate_y = % 6.2f,\n"
				"  rotate_z = % 6.2f\n\n"
				"  scale_x = % 6.2g,\n"
				"  scale_y = % 6.2g,\n"
				"  scale_z = % 6.2g:\n"
				"   scale = %.6g\n\n"
				" translate_z = %f\n\n",
				rotate_x, rotate_y, rotate_z,
				scale_x, scale_y, scale_z, scale,
				translate_z);
		eprintf(" t = %g, iters = %lu\n",
				t, iter);
		eprintf(" mesh: %d x %d, and value:\n"
				"  from (% f, % f, % f)\n"
				"  to   (% f, % f, % f)\n",
				mesh.width, mesh.height,
				-mesh.dx*(mesh.width/2), -mesh.dy*(mesh.height/2), -2.0,
				 mesh.dx*(mesh.width/2),  mesh.dy*(mesh.height/2),  2.0);

		freeGLBuffers();
		if (gl_has_query_timers)
			freeQueries();
		freeTimers();

		exit(EXIT_SUCCESS);
		break;

	default:
		break;
	}
}


// obsługa myszy
int mouse_old_x, mouse_old_y;
int mouse_buttons = 0;
// callback przycisków myszy dla bieżącego okna w GLUT
void mouseFunc(int button, int state, int x, int y)
{
	if (state == GLUT_DOWN) {
		mouse_buttons |= 1<<button;
	} else if (state == GLUT_UP) {
		mouse_buttons = 0;
	}

	eprintf("- mouse button %d in state %d {buttons=0x%02x} "
			"at (%d,%d), was (%d,%d)\n",
			button, state, mouse_buttons,
			x, y, mouse_old_x, mouse_old_y);

	mouse_old_x = x;
	mouse_old_y = y;

	glutPostRedisplay();
}

// callback ruchu wskaźnika myszy gdy przyciśnięty klawisz
void motionFunc(int x, int y)
{
	float dx, dy;
	dx = x - mouse_old_x;
	dy = y - mouse_old_y;

	if (mouse_buttons & 1) {
		rotate_x += dy * 0.2;
		rotate_y += dx * 0.2;
	} else if (mouse_buttons & 4) {
		translate_z += dy * 0.01;
	}

	mouse_old_x = x;
	mouse_old_y = y;

	glutPostRedisplay();
}

// callback zmiany rozmiaru (kształtu) bieżącego okna dla GLUT,
// używany w naszym programie do zapewnienia stałych wymiarów
void reshapeFunc(int _width, int _height)
/*@ globals: window @*/
{
	// nie wykonuj nic, jeśli rozmiar się nie zmienił
	if (window.width == _width && window.height == _height)
		return;

	if (_width < 64 || _height < 64) {
		// jeśli rozmiar jest zbyt mały, ignoruj go, zmień rozmiar na oryginalny
		// uwaga: wywołuje ponownie callback zmiany kształtu
		glutReshapeWindow(window.width, window.height);
		eprintf("- too small, resized back to %dx%d from %dx%d\n",
				window.width, window.height, _width, _height);
	} else {
		// dopasuj parametry okna widoku (viewport) w OpenGL,
		// czyli przeliczania na współrzędne ekranowe
		// ze znormalizowanych współrzędnych urządzenia
		eprintf("- resizing from %dx%d to %dx%d\n",
				window.width, window.height, _width, _height);

		window.width  = _width;
		window.height = _height;
		glViewport(0, 0, window.width, window.height);

		// sprawdzenie czy OpenGL zezwolił na zmianę rozmiaru widoku
		GLint viewport_params[4];
		glGetIntegerv(GL_VIEWPORT, viewport_params);
		if (window.width  != viewport_params[2] ||
			window.height != viewport_params[3]) {

			eprintf("  clamped to %dx%d by OpenGL\n",
					viewport_params[2], viewport_params[3]);
			window.width  = viewport_params[2];
			window.height = viewport_params[3];

			// może być potrzebna zmiana współczynnika kształtu w kamerze.
			// TODO: niepotrzebne przy rzucie ortograficznym
			setPerspectiveProjection(window.width, window.height);

			// uwaga: wywołuje ponownie callback zmiany kształtu,
			// w razie gdyby OpenGL nie pozwolił na zadaną zmianę
			glutReshapeWindow(window.width, window.height);
		}
	}
}


/* ---------------------------------------------------------------------- */
/* Inicjalizacja CUDA i GLUT */

#ifndef NO_CUDA
// inicjalizacja CUDA (jeśli potrzebne)
void initCUDA(void)
{
	eprintf("[%s]\n", __FUNCTION__);
	eprintf("Initializing CUDA device %d for OpenGL...\n", 0);
	// jawnie użyj urządzenia 0 do obliczeń CUDA i grafiki OpenGL;
	// listę urządzeń CUDA powiązanych z bieżącym kontekstem OpenGL
	// można uzyskać za pomocą cudaGLGetDevices()
	// UWAGA: niepotrzebne od CUDA 5.0
	cudaGLSetGLDevice(0);
	eprintf("CUDA initialized (done)\n");
}
#endif /* !NO_CUDA */

// skonfigurowanie rzutu perspektywicznego
void setPerspectiveProjection(int width, int height)
{
	// ustalenie macierzy widoku (dla 'fixed pipeline')
	// UWAGA: bez restartu - nie chcemy zmienić macierzy widoku
	glMatrixMode(GL_MODELVIEW);	// kombinowana modelu i widoku
	// TODO: bez przesunięcia przy rzutowaniu ortograficznym;
	//       to przesunięcie to jest oddalenie kamery od wykresu
	glTranslatef(0.0f, 0.0f, translate_z);

	// projekcja - rzut perspektywiczny
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity(); // startujemy od zera, tzn. od macierzy jednostkowej
	// TODO: dodać możliwość przełączenia do rzutowania ortograficznego
	//       zamiast perspektywy, tzn. gluOrtho2D lub glOrtho
	// TODO: dodać możliwość "powiększenia", a dokładniej zmiany kąta
	//       widzenia: 60.0 -> 60.0*fov_zoom, zastanowić się nad użyciem
	//       gluLookAt().
	gluPerspective(60.0, (GLfloat)width/(GLfloat)height, 0.10, 10.0);
}

// inicjalizacja grafiki OpenGL
void initGL(int width, int height)
{
	eprintf("[%s]\n", __FUNCTION__);
	// domyślna inicjalizacja
	glClearColor(0.0, 0.0, 0.0, 1.0);
	glDisable(GL_DEPTH_TEST);

	if (drawMode == drPlotSurface) {
		glEnable(GL_DEPTH_TEST);
		eprintf(" enabled depth test for surface plot [%d = drPlotSurface]\n",
				drawMode);
	}

	// rozmiary okna to rozmiary widoku OpenGL
	glViewport(0, 0, width, height);

	// set view matrix
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glScalef(scale_x, scale_y, scale_z);

	setPerspectiveProjection(width, height);
}

// inicjalizacja interfejsu graficznego w GLUT
void initGLUT(int *argc, char *argv[])
{
	GLenum err;


	eprintf("Initializing GLUT...\n");
	/*
	 * Inicjalizacja biblioteki GLUT, interpretując specyficzne dla
	 * GLUT i systemu okienkowego parametry wykonania, ekstrahując
	 * i usuwając te parametry (dla X Window System np. -gldebug
	 * oraz -display <DISPLAY>).
	 */
	glutInit(argc, argv);

	/*
	 * Ustawienie początkowego trybu wyświetlania, rozmiarów okna oraz
	 * ewentualnie jego położenia.  Początkowy tryb wyświetlania to
	 * model kolorów RGBA (GLUT_RGBA) z podwójnym buforowaniem
	 * (GLUT_DOUBLE), oraz buforem głębii (GLUT_DEPTH), bez komponentu
	 * alfa w buforze kolorów (GLUT_ALPHA).
	 */
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_DEPTH | GLUT_RGBA);
	glutInitWindowSize(window.width, window.height);
	//glutInitWindowPosition(0, 0);

	// utworzenie okna głównego (top-level) o zadanej nazwie (tytule)
	snprintf(window.title, MAX_WINDOW_TITLE_LENGTH, "%s", argv[0]);
	glutCreateWindow(window.title);
	eprintf(" window size %d x %d\n",
	        glutGet(GLUT_WINDOW_WIDTH),
	        glutGet(GLUT_WINDOW_HEIGHT));

	/*
	 * Pliki nagłówkowe bibliotek OpenGL domyślnie udostępniają tylko
	 * podstawowe funkcje OpenGL, na przykład tylko funkcje standardu
	 * OpenGL 1.1.  Funkcje spoza tego zakresu, na przykład
	 * glGenBuffers, glBindBuffer, glReadBuffer itp. ze standardu
	 * OpenGL 1.5, potrzebne do używania PBO (Pixel Buffer Object)
	 * wymagają 'załadowania', tzn. pobrania ich adresów.
	 *
	 * Najprostszym rozwiązaniem jest użycie biblioteki do zarządzania
	 * rozszerzeniami OpenGL jak GLEW lub GLee.
	 */
	// inicjalizacja biblioteki GLEW i sprawdzenie jej poprawności
	eprintf(" Initializing GLEW...\n");
	// użycie metody sprawdzania dostępności z nowego OpenGL
	//glewExperimental = GL_TRUE;
	err = glewInit();
	if (err != GLEW_OK) {
		/* Problem: glewInit nie powiodło się, pojawił się znaczący błąd */
		fprintf(stderr, "GLEW Error: %s\n", glewGetErrorString(err));
		exit(EXIT_FAILURE);
	}
	// wyświetlenie pewnych informacji o środowisku i bibliotekach,
	// podobnie do glewinfo czy glxinfo (dla GLX z X Window System)
	eprintf(" Status: Using GLEW %s\n", glewGetString(GLEW_VERSION));
	eprintf(" OpenGL vendor string: %s\n", glGetString(GL_VENDOR));
	eprintf(" OpenGL renderer string: %s\n", glGetString(GL_RENDERER));
	eprintf(" OpenGL version string: %s\n", glGetString(GL_VERSION));
	eprintf(" GLU version string: %s\n", gluGetString(GLU_VERSION));
	eprintf(" GLU extensions: %s\n", gluGetString(GLU_EXTENSIONS));
	// sprawdzenie rozszerzeń i wersji, czy VBO dostępne (GLEW >= 1.3.0)
	if (!GL_VERSION_1_5) {
		fprintf(stderr, "ERROR: OpenGL 1.5 (needed for glBindBuffer) not supported\n");
		fflush(stderr);
		//exit(EXIT_FAILURE);
	}
	if (!glewIsSupported("GL_VERSION_2_0")) {
		fprintf(stderr, "ERROR: Support for necessary OpenGL extensions missing\n");
		fflush(stderr);
		//exit(EXIT_FAILURE);
	}
	if (glewIsSupported("GL_VERSION_3_3")) {
		// https://www.opengl.org/wiki/Query_Object
		gl_has_query_timers = 1;
		initQueries();
	} else {
		gl_has_query_timers = 0;
		eprintf(" no support for query timers in OpenGL\n");
	}

	// wyczyszczenie okna
	clear();

	// zarejestrowanie funkcji zwrotnych (callback) zdarzeń GLUT
	glutIdleFunc(idleFunc);         // animacja
	glutDisplayFunc(displayFunc);   // wyświetlanie
	glutKeyboardFunc(keyboardFunc); // obsługa klawiszy
	glutReshapeFunc(reshapeFunc);   // zmiana rozmiaru okna
	glutMouseFunc(mouseFunc);       // przyciski myszy
	glutMotionFunc(motionFunc);     // przesuwanie (z wciśniętym przyciskiem)

	initGL(window.width, window.height);

	eprintf("GLUT initialized (done)\n");
}


/* ###################################################################### */
/* ====================================================================== */
/* ---------------------------------------------------------------------- */
/* Program główny */

int main(int argc, char *argv[])
{
	/*
	 * Obsługa [własnych] parametrów wywołania programu.
	 *
	 * UWAGA: przy większej ilości parametrów warto skorzystać z
	 * odpowiedniej biblioteki, np. funkcji getopt i getopt_long ze
	 * standardowej biblioteki C (libc), lub argp tamże, lub parseopt
	 * z git i perf, lub biblioteki popt lub argtable, lub tclap.
	 */
	if (argc > 1 &&
	    (!strcmp(argv[1], "-?") ||
	     !strcmp(argv[1], "-h") ||
	     !strcmp(argv[1], "--help"))) {
		printf("Składnia:  %1$s (-?|-h|--help)\n"
		       "           %1$s [opcje GLUT]\n", argv[0]);
		printf("\n");
		print_help();

		exit(EXIT_SUCCESS);
	}

	/*
	 * Główna część programu
	 */
	eprintf("[%s] - Starting...\n", argv[0]);

	// inicjalizacja zmiennych globalnych, tam gdzie nie można było
	// tego zrobić przy deklaracji ze względu na 'initializer element
	// is not constant'
	xmin = -0.5*mesh.dx*mesh.width; ymin = -0.5*mesh.dy*mesh.height; zmin = -2.0;
	xmax =  0.5*mesh.dx*mesh.width; ymax =  0.5*mesh.dy*mesh.height; zmax =  2.0;

	scale = fmin3f(1./xmax, 1./ymax,1./zmax); // zakładamy symetryczność
	scale_x = scale_y = scale; scale_z = scale*val_scale;

	// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
#if 1
	eprintf("Temporary mesh size reduction\n");
	//eprintf("Temporary mesh size increase\n");
	eprintf(" before:\n");
 	eprintf("  x = [%-.2f, %-.2f], y = [%-.2f, %-.2f], z = [%-.2f, %-.2f], scale = %g\n",
			xmin, xmax, ymin, ymax, zmin, zmax, scale);
	eprintf("  grid = %d x %d, mesh = [ dx = %f, dy = %f ]\n",
			mesh.width, mesh.height, mesh.dx, mesh.dy);

	mesh.width  = 64;
	mesh.height = 64;
	//mesh.width  = 1024;
	//mesh.height = 1024;
	mesh.dx *= 256.0/mesh.width;
	mesh.dy *= 256.0/mesh.height;

	xmin = -0.5*mesh.dx*mesh.width; ymin = -0.5*mesh.dy*mesh.height; zmin = -2.0;
	xmax =  0.5*mesh.dx*mesh.width; ymax =  0.5*mesh.dy*mesh.height; zmax =  2.0;

	scale = fmin3f(1./xmax, 1./ymax,1./zmax); // zakładamy symetryczność
	scale_x = scale_y = scale; scale_z = scale*val_scale;
	eprintf(" after:\n");
#endif
 	eprintf("  x = [%-.2f, %-.2f], y = [%-.2f, %-.2f], z = [%-.2f, %-.2f]\n"
			"  scale = (%g, %g, %g)\n",
			xmin, xmax, ymin, ymax, zmin, zmax, scale_x, scale_y, scale_z);
	eprintf("  grid = %d x %d, mesh = [ dx = %f, dy = %f ]\n",
			mesh.width, mesh.height, mesh.dx, mesh.dy);

	// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX


	// inicjalizacja GLUT, CUDA oraz buforów OpenGL
	initGLUT(&argc, argv);
#if !defined(NO_CUDA) && CUDART_VERSION < 5000
	// począwszy od wersji CUDA 5.0 niepotrzebna jest jawna inicjalizacja CUDA
	// wiążąca urządzenie CUDA z kontekstem OpenGL w celu uzyskania maksymalnej
	// wydajności interoperacyjności między CUDA i OpenGL
	initCUDA();
#endif
	initGLBuffers();
	initTimers();
	// zapewnij zwolnienie VBO przy wyjściu z programu
	// TODO: specjalna funkcja atexit, zwalniająca wszystko
	atexit(freeGLBuffers);

	// pętla przetwarzania zdarzeń GLUT, nie wraca
	glutMainLoop();

#ifndef NO_CUDA
	// zrestartowanie urządzenia CUDA, wymusza synchronizację
	cudaDeviceReset();
#endif

	eprintf("[%s] - Done\n", argv[0]);
	return EXIT_SUCCESS;
}

/*
 * Local Variables:
 * mode: c
 * encoding: utf-8-unix
 * compile-command: "gcc -Wall -std=c99 -x c -DNO_CUDA \
 *   -o cpu_plot3d_anim gpu_plot3d_anim.cu \
 *   -lGL -lGLU -lglut -lGLEW -lm"
 * tab-width: 4
 * c-basic-offset: 4
 * /eval: (whitespace-mode 1)/
 * eval: (which-function-mode 1)
 * End:
 */

/* end of file gpu_plot3d_anim.cu */
