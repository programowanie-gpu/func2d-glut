CC     = gcc
RM     = rm -rf

CFLAGS = -Wall -std=c99 -g
LDLIBS = -lGL -lGLU -lglut -lGLEW -lm

FUNC2D_PROGS = func2d_cpu func2d_cpu_nopbo func2d_cpu_map func2d_cpu_mapr func2d
CPU_PROGS    = cpu_bitmap_static cpu_bitmap_anim cpu_plot3d_anim \
               func2d_cpu func2d_cpu_nopbo func2d_cpu_map func2d_cpu_mapr
GPU_PROGS    = gpu_bitmap_static gpu_bitmap_anim gpu_plot3d_anim \
               func2d pbo_demo
ALL_PROGS    = $(GPU_PROGS) $(CPU_PROGS)

$(CPU_PROGS) : CPPFLAGS += -DNO_CUDA
$(CPU_PROGS) : CFLAGS   += -x c
func2d_cpu_nopbo : CPPFLAGS += -DNO_PBO
func2d_cpu_map   : CPPFLAGS += -DMAP_PBO
func2d_cpu_mapr  : CPPFLAGS += -DMAP_PBO=MAP_BUFFER_RANGE
# Xi  - X11 Input extension library
# Xmu - X11 miscellaneous utility library
$(FUNC2D_PROGS) : LDLIBS += -lX11 #-lXi -lXmu

# All Target
all: $(ALL_PROGS)
# GPU and CPU Targets
cpu: $(CPU_PROGS)
gpu: $(GPU_PROGS)

# Tool invocations
func2d: src/func2d.cu
	@echo 'Building target: $@'
	@echo 'Invoking: NVCC Linker'
	nvcc -link -arch compute_20 -o $@ $< $(LDLIBS)
	@echo 'Finished building target: $@'
	@echo ' '

func2d_cpu func2d_cpu_nopbo func2d_cpu_map func2d_cpu_mapr: src/func2d.cu
	@echo 'Building target: $@'
	$(CC) $(CFLAGS) $(CPPFLAGS) -o $@ $< $(LDLIBS)
	@echo 'Finished building target: $@'
	@echo ' '

gpu_bitmap_static: src/gpu_bitmap_static.cu
	nvcc -link -arch compute_20 -o $@ $< $(LDLIBS)

cpu_bitmap_static: src/gpu_bitmap_static.cu
	$(CC) $(CFLAGS) $(CPPFLAGS) -o $@ $< $(LDLIBS)

gpu_bitmap_anim: src/gpu_bitmap_anim.cu
	nvcc -link -arch compute_20 -o $@ $< $(LDLIBS)

cpu_bitmap_anim: src/gpu_bitmap_anim.cu
	$(CC) $(CFLAGS) $(CPPFLAGS) -o $@ $< $(LDLIBS)

pbo_demo: src/pbo_demo.c
	$(CC) $(CFLAGS) $(CPPFLAGS) $(LDFLAGS) -o $@ $< $(LDLIBS)

cpu_plot3d_anim: src/gpu_plot3d_anim.cu
	$(CC) $(CFLAGS) $(CPPFLAGS) -o $@ $< $(LDLIBS)

gpu_plot3d_anim: src/gpu_plot3d_anim.cu
	nvcc -link -arch compute_20 -o $@ $< $(LDLIBS)

# Other Targets
clean:
	-$(RM) *.o
	-@echo ' '

cleanall: clean
	-$(RM) $(ALL_PROGS)

.PHONY: all clean cleanall
